# PhuongNN27_Assignments

Hello there, this is my archive of Fresher .NET 2023. There are main following features:

1. [Folder SQL](#SQL)
2. [Folder C# Basic](#C#-Basic)
3. [Folder OOP](#OOP)
4. [Folder Mini Project](#Mini-Project)

## SQL
SQL folder includes CodeLearn assignments. There are 3 folders:

### SQL Essentials
1. ERD and Schemas
- [Assignment 1](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_101?ref_type=heads)
- [Assignment Opt 2](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_Opt02?ref_type=heads)
- [Assignment Opt 3](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_Opt03?ref_type=heads)
- [Assignment Opt 4](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_Opt4?ref_type=heads)
2. Basic SQL
- DDL
- DML
- Group By, Having, Join, Subquery
- [Assignment 2](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment2?ref_type=heads)
- [Assignment 3](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment3?ref_type=heads)

### SQL Advanced
1. Info
- Join
- Subquery
- CTE
- Trigger
- Store procedure
- Function
2. [Assignments](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Advanced?ref_type=heads)

### Test
This includes 2 assignments for test [Test](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/Test?ref_type=heads)

## C# Basic
C# Basic folder includes following features:

### Exercise 1
1. Info
- DateTime
- Queue, Stack
- String
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise1?ref_type=heads)

### Exercise 2
1. Info
- Delegate
- Generic
- Stack, Queue
- Collection (List, Dictionary)
- JsonConvert (Practice)
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise2?ref_type=heads)

### Exercise 3
1. Info
- Extension
- HttpClient
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise3?ref_type=heads)

### Exercise 4
1. Info
- Paging
- LinQ
- Deferred Execution
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise4?ref_type=heads)

### Test
1. Info: Student Management (Console Application)
- Add a student
- Show student by page
- Remove a student by Id
- Search a student by Id or by name
2. Link

## OOP

### Phone Management
- This folder includes Mobile Phone, Store, Customer, and Purchase
- [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/OOP/PhoneManagement?ref_type=heads)

### Hotel Management
- This folder includes Mobile Booking, Customer, Hotel, Invoice, and Room
- [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/OOP/HotelManagement?ref_type=heads)

### Library Management
- This folder includes Mobile Book, Customer, Library, and Loan
- [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/OOP/LibraryManagement?ref_type=heads)

### Movie Management

## Mini Project
This is the mini project for C# that manage a library. There are 5 entities:
- Customer
- Book
- Loan
- Review
- Unit testing: in processing ...
- [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/MiniProject?ref_type=heads)