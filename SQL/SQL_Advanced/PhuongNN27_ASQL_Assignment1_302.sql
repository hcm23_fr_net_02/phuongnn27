﻿/*Ex 01*/
CREATE DATABASE Ass302

USE Ass302

CREATE TABLE San_Pham (
	Ma_SP	INT	IDENTITY(1,1)	PRIMARY KEY,
	Ten_SP	NVARCHAR(100)		NOT NULL,
	Don_Gia	MONEY				NOT NULL
)

CREATE TABLE Khach_Hang (
	Ma_KH		INT IDENTITY(1,1)	PRIMARY KEY,
	Ten_KH		NVARCHAR(100)		NOT NULL,
	Phone_No	NVARCHAR(13)		NULL,
	Ghi_Chu		NVARCHAR(250)		NULL
)

CREATE TABLE Don_Hang (
	Ma_DH		INT IDENTITY(1,1)	PRIMARY KEY,
	Ngay_DH		DATETIME			DEFAULT GETDATE(),
	Ma_SP		INT					FOREIGN KEY REFERENCES San_Pham(Ma_SP),
	So_Luong	INT					NOT NULL,
	Ma_KH		INT					FOREIGN KEY REFERENCES Khach_Hang(Ma_KH)
)

INSERT INTO dbo.Khach_Hang (Ten_KH)
VALUES (N'Phong'),
(N'Binh'),
(N'Thinh')

INSERT INTO dbo.San_Pham(Ten_SP, Don_Gia)
VALUES (N'Kem', 150000),
(N'Bánh Quy', 20000),
(N'Nước soda', 200000)

INSERT INTO dbo.Don_Hang (Ma_KH, Ma_SP, Ngay_DH, So_Luong)
VALUES (1, 3, CAST (GETDATE() AS DATETIME), 1),
(2, 1, CAST (GETDATE() AS DATETIME), 2),
(3, 2, CAST (GETDATE() AS DATETIME), 1)

/*
https://www.sqlshack.com/sql-partition-by-clause-overview/
*/
CREATE VIEW DanhSachDonHang AS
(
	SELECT K.Ten_KH, D.Ngay_DH, S.Ten_SP, D.So_Luong, SUM(D.So_Luong * S.Don_Gia) OVER (PARTITION BY D.Ma_DH) AS [Thành Tiền]
	FROM dbo.Don_Hang D INNER JOIN dbo.Khach_Hang K ON D.Ma_KH = K.Ma_KH
		INNER JOIN dbo.San_Pham S ON D.Ma_SP = S.Ma_SP
)

/*Ex 02*/

-- Q1
DROP DATABASE Ass302Ex2

CREATE DATABASE Ass302Ex2

Use Ass302Ex2

CREATE TABLE Department (
	Department_Number	INT IDENTITY(1,1)	PRIMARY KEY,
	Department_Name		NVARCHAR(100)		NOT NULL
)

CREATE TABLE Employee (
	Employee_Number		INT IDENTITY(1,1)	PRIMARY KEY,
	Employee_Name		NVARCHAR(50)		NOT NULL,
	Department_Number	INT					FOREIGN KEY REFERENCES Department(Department_Number)
)

CREATE TABLE Skill (
	Skill_Code			INT IDENTITY(1,1)	PRIMARY KEY,
	Skill_Name			NVARCHAR(100)		NOT NULL
)

CREATE TABLE Employee_Skill (
	Employee_Number		INT		FOREIGN KEY REFERENCES Employee(Employee_Number),
	Skill_Code			INT		FOREIGN KEY REFERENCES Skill(Skill_Code)
)

SELECT * FROM dbo.Skill

INSERT INTO dbo.Skill (Skill_Name)
VALUES ('Java'),
('C'),
('C#'),
(N'Kế toán'),
(N'Tuyển dụng')

INSERT INTO dbo.Department (Department_Name)
VALUES (N'Kỹ thuật'),
(N'Kế toán'),
(N'HR')

insert into dbo.Employee (Employee_Name, Department_Number)
VALUES (N'Phong', 1),
(N'Bình', 1),
(N'Minh', 1),
(N'Lan', 2),
(N'Hương', 3),
(N'Ngọc', 3)

INSERT INTO dbo.Employee_Skill(Employee_Number, Skill_Code)
values (1, 1),
(1, 3),
(2, 2),
(2, 3),
(3, 3),
(4, 4),
(5, 5),
(6, 5)

-- Q2
-- Subquery

SELECT E.Employee_Name
FROM dbo.Employee E
WHERE E.Employee_Number IN (
								SELECT ES.Employee_Number
								FROM dbo.Employee_Skill ES
								WHERE ES.Skill_Code =	(
															SELECT S.Skill_Code
															FROM dbo.Skill S
															WHERE S.Skill_Name LIKE 'Java'
														)
							)

-- inner join

SELECT E.Employee_Name
FROM dbo.Employee E INNER JOIN dbo.Employee_Skill ES ON E.Employee_Number = ES.Employee_Number
	INNER JOIN dbo.Skill S ON ES.Skill_Code = S.Skill_Code
WHERE S.Skill_Name = 'Java'

-- Q3

SELECT D.Department_Number, STRING_AGG(E.Employee_Name, ', ') AS emps
FROM dbo.Department D INNER JOIN dbo.Employee E ON D.Department_Number = E.Department_Number
GROUP BY D.Department_Number
HAVING COUNT(E.Employee_Number) >= 3

-- Q4

SELECT E.Department_Number, E.Employee_Name, COUNT(ES.Employee_Number) AS [Number of skills]
FROM dbo.Employee E LEFT JOIN dbo.Employee_Skill ES ON E.Department_Number = ES.Employee_Number
GROUP BY E.Department_Number, E.Employee_Name
ORDER BY E.Employee_Name


-- Q5

CREATE VIEW [Working Employees] AS
(
	SELECT E.Employee_Name, S.Skill_Name, D.Department_Name
	FROM (
			((dbo.Employee E INNER JOIN dbo.Employee_Skill ES ON E.Employee_Number = ES.Employee_Number) 
			INNER JOIN dbo.Skill S ON ES.Skill_Code = S.Skill_Code)
			INNER JOIN dbo.Department D ON E.Department_Number = D.Department_Number
		)
)

SELECT * FROM [Working Employees]