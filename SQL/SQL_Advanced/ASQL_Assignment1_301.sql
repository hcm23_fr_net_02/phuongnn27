﻿CREATE DATABASE EMS

USE EMS

/* Create tables*/

CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO

/*
1. Add at least 8 records into each created tables.
*/
INSERT INTO dbo.Skill (SkillName, Note)
VALUES (N'C', N'Kỹ năng C'),
(N'C++', N'Kỹ năng C++'),
(N'.NET', N'Kỹ năng .NET'),
(N'Java', N'Kỹ năng Java'),
(N'Kế toán', N'Kỹ năng kế toán'),
(N'Lọc hồ sơ', N'Kỹ năng lọc hồ sơ'),
(N'Tuyển dụng', N'Kỹ năng tuyển dụng'),
(N'Quản lý', N'Kỹ năng quản lý')

INSERT INTO dbo.Department ([DeptName])
VALUES (N'Kỹ thuật'),
(N'FA'),
(N'HR'),
(N'Thi công'),
(N'Bảo mật'),
(N'Nội vụ'),
(N'Kinh tế'),
(N'Kế toán')


INSERT INTO dbo.Employee ([EmpNo], [EmpName], [BirthDay], [DeptNo], [StartDate], [Salary], [Level], Email, [MgrNo], [Status])
VALUES (1, N'Phong', CAST(N'1998-01-10T00:00:00.000' AS DateTime), 1, CAST(N'2022-01-10T00:00:00.000' AS DateTime), 20000000, 2, N'phong@fpt.com', 2, 0),
(2, N'Thịnh', CAST(N'1989-02-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-03-10T00:00:00.000' AS DateTime), 35000000, 3, N'thinh@fpt.com', 0, 1),
(3, N'Lâm', CAST(N'1996-07-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-06-20T00:00:00.000' AS DateTime), 18000000, 2, N'lam@fpt.com', 2, 1),
(4, N'Bình', CAST(N'2001-11-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-07-15T00:00:00.000' AS DateTime), 10000000, 1, N'binh@fpt.com', 2, 1),
(5, N'Ngọc', CAST(N'1997-06-23T00:00:00.000' AS DateTime), 3, CAST(N'2022-11-10T00:00:00.000' AS DateTime), 16000000, 2, N'ngoc@fpt.com', 7, 0),
(6, N'Lan', CAST(N'1999-08-10T00:00:00.000' AS DateTime), 3, CAST(N'2023-01-10T00:00:00.000' AS DateTime), 8000000, 1, N'lan@fpt.com', 7, 1),
(7, N'Hương', CAST(N'1980-12-17T00:00:00.000' AS DateTime), 3, CAST(N'2022-01-10T00:00:00.000' AS DateTime), 24000000, 3, N'huong@fpt.com', 0, 1),
(8, N'Hoa', CAST(N'1998-03-11T00:00:00.000' AS DateTime), 8, CAST(N'2023-04-10T00:00:00.000' AS DateTime), 10000000, 2, N'hoa@fpt.com', 0, 1)


INSERT INTO dbo.Emp_Skill (SkillNo, EmpNo, RegDate, SkillLevel)
VALUES (2,1, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(3,2, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 3),
(1,3, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(4,4, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 1),
(6,5, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(6,6, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 1),
(6,7, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 3),
(8,8, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(4,2, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2)

/*
2. Specify the name, email and department name of the employees that have been working at least six months.
*/

SELECT E.[EmpName], E.Email, D.DeptName
FROM dbo.Employee E INNER JOIN dbo.Department D
ON E.[DeptNo] = D.[DeptNo]
WHERE DATEDIFF(MONTH, E.[StartDate], CAST(GETDATE() AS DateTime)) >= 6


/*
3. Specify the names of the employees whore have either ‘C++’ or ‘.NET’ skills.
*/

SELECT E.[EmpName]
FROM ((dbo.Employee E INNER JOIN dbo.Emp_Skill ES ON E.EmpNo = ES.EmpNo) 
		INNER JOIN dbo.Skill S ON ES.SkillNo = S.SkillNo)
WHERE S.SkillName = '.NET' OR S.SkillName = 'C++'

/*
4. List all employee names, manager names, manager emails of those employees.
*/
SELECT DISTINCT E.[EmpName], M.EmpName, M.Email 
FROM dbo.Employee E INNER JOIN dbo.Employee M ON E.[MgrNo] = M.[EmpNo]

/*
5. Specify the departments which have >=2 employees, print out the list of departments’ employees right after each department.
https://stackoverflow.com/questions/15477743/listagg-in-sqlserver
*/
SELECT D.DeptNo, STRING_AGG(E.EmpName, '') AS emps
FROM dbo.Department D INNER JOIN dbo.Employee E on E.DeptNo = D.DeptNo
GROUP BY D.DeptNo
HAVING COUNT(E.EmpNo) >= 2

/*
6. List all name, email and number of skills of the employees and sort ascending order by employee’s name.
use message or function or store procedure
*/
SELECT E.Email, E.EmpName, (SELECT COUNT(ES.EmpNo)
						FROM dbo.Emp_Skill ES
						WHERE ES.EmpNo = E.EmpNo) AS [Number of skills]
FROM dbo.Employee E INNER JOIN dbo.Emp_Skill ES ON E.EmpNo = ES.EmpNo
ORDER BY E.EmpName



select * from dbo.Employee
select * from dbo.Emp_Skill
/*
7. Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills.
*/

SELECT E.[EmpName], E.Email, E.[BirthDay]
FROM dbo.Employee E
where E.EmpNo = (
					SELECT E.EmpNo
					FROM dbo.Employee E INNER JOIN dbo.Emp_Skill ES ON E.EmpNo = ES.EmpNo
					WHERE E.Status = 1
					GROUP BY E.EmpNo
					HAVING COUNT(ES.SkillNo) > 1
				)

/*
8. Create a view to list all employees are working (include: name of employee and skill name, department name).
*/

CREATE VIEW [Working Employees] AS
SELECT E.[EmpName], S.SkillName, D.DeptName
FROM (
		((dbo.Employee E INNER JOIN dbo.Emp_Skill ES ON E.EmpNo = ES.EmpNo) 
		INNER JOIN dbo.Skill S ON ES.SkillNo = S.SkillNo)
		INNER JOIN dbo.Department D ON E.DeptNo = D.DeptNo
	)
WHERE E.Status = 1

SELECT * FROM [Working Employees]

