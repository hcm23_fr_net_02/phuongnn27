USE AdventureWorks2008R2

-- EX1
-- Q1
SELECT P.[Name]
FROM Production.Product P
WHERE P.ProductSubcategoryID IN	(
							SELECT PS.ProductSubcategoryID
							FROM Production.ProductSubcategory PS
							WHERE PS.Name LIKE 'Saddles'
						)

-- Q2

SELECT P.[Name]
FROM Production.Product P
WHERE P.ProductSubcategoryID IN	(
							SELECT PS.ProductSubcategoryID
							FROM Production.ProductSubcategory PS
							WHERE PS.Name LIKE 'Bo%'
						)

-- Q3
SELECT P.[Name]
FROM Production.Product P
WHERE P.ListPrice IN (
							SELECT MIN(PT.ListPrice)
							FROM Production.Product PT
							WHERE PT.ProductSubcategoryID = 3
						)

-- Q4
SELECT CR.Name
FROM Person.CountryRegion CR
WHERE CR.CountryRegionCode IN	(
									SELECT SP.CountryRegionCode
									FROM Person.StateProvince SP
									GROUP BY SP.CountryRegionCode
									HAVING COUNT(SP.CountryRegionCode) < 10
								)

SELECT CR.Name
FROM Person.CountryRegion CR INNER JOIN Person.StateProvince SP 
	ON CR.CountryRegionCode = SP.CountryRegionCode
GROUP BY CR.Name
HAVING COUNT(SP.CountryRegionCode) < 10

-- Q5

DECLARE @AverageSubTotal Money
SET @AverageSubTotal = (SELECT AVG(SOH.SubTotal) FROM Sales.SalesOrderHeader SOH)

SELECT SOH.SalesPersonID, (AVG(SOH.SubTotal) - @AverageSubTotal) AS SalesDiff
FROM Sales.SalesOrderHeader SOH
WHERE SOH.SalesPersonID IS NOT NULL
GROUP BY SOH.SalesPersonID

-- Q6

-- Step 1
SELECT AVG(P.ListPrice)
FROM Production.Product P
WHERE P.ProductSubcategoryID NOT BETWEEN 1 AND 3

-- Step 2
SELECT P.[Name], (P.ListPrice - (
									SELECT AVG(P.ListPrice)
									FROM Production.Product P
									WHERE P.ProductSubcategoryID NOT BETWEEN 1 AND 3
								)) AS Diff
FROM Production.Product P
WHERE P.ProductSubcategoryID NOT BETWEEN 1 AND 3

-- Step 3

SELECT *
FROM	(
			SELECT P.[Name], (P.ListPrice - (
									SELECT AVG(P.ListPrice)
									FROM Production.Product P
									WHERE P.ProductSubcategoryID NOT BETWEEN 1 AND 3
								)) AS Diff
			FROM Production.Product P
			WHERE P.ProductSubcategoryID NOT BETWEEN 1 AND 3
		) AS X
WHERE X.Diff BETWEEN 400 AND 500

-- Q7
-- Part 1
SELECT P.FirstName + ' ' + P.LastName

FROM Sales.SalesPerson SP

JOIN HumanResources.Employee E

    ON E.BusinessEntityID  = SP.BusinessEntityID

JOIN Person.Person AS P 

    ON E.BusinessEntityID = P.BusinessEntityID

WHERE Bonus > 500

-- Part 2
SELECT P.FirstName + ' ' + P.LastName
FROM Person.Person AS P
WHERE EXISTS	(
					SELECT PT.BusinessEntityID
					FROM Sales.SalesPerson SP JOIN HumanResources.Employee E
					ON E.BusinessEntityID  = SP.BusinessEntityID 
					JOIN Person.Person AS PT 
					ON E.BusinessEntityID = PT.BusinessEntityID
					WHERE Bonus > 500
				)

-- Q8 (not)

-- EXISTS
SELECT ST.SalesPersonID
FROM Sales.Store ST
WHERE EXISTS (
					SELECT S.BusinessEntityID FROM Sales.SalesPerson S
					WHERE S.BusinessEntityID = ST.BusinessEntityID
			 )	

-- JOIN
SELECT ST.SalesPersonID
FROM Sales.SalesPerson S RIGHT JOIN Sales.Store ST ON S.BusinessEntityID = ST.BusinessEntityID
GROUP BY ST.SalesPersonID


-- Q9
-- Part 1

SELECT P.ProductSubcategoryID, COUNT(P.ProductID)
FROM Production.Product P
GROUP BY P.ProductSubcategoryID


-- Part 2

; WITH TempSet (ProdSubID, CountedProds) AS
(
SELECT P.ProductSubcategoryID, COUNT(P.ProductID)
FROM Production.Product P
GROUP BY P.ProductSubcategoryID
)

SELECT * FROM TempSet

; WITH TempSettestquery (ProductCategoryID, SubCat, SumProds) AS
(
SELECT P.ProductSubcategoryID, COUNT(P.ProductSubcategoryID), SUM(P.ProductID)
FROM Production.Product P INNER JOIN Production.ProductSubcategory PS 
	ON P.ProductSubcategoryID = PS.ProductSubcategoryID
GROUP BY P.ProductSubcategoryID
)

SELECT * FROM TempSettestquery

-----------------------------------------------------------------------------------

-- Ex2
-- Q1

SELECT CR.Name, SP.Name
FROM Person.CountryRegion CR INNER JOIN Person.StateProvince SP
	ON CR.CountryRegionCode = SP.CountryRegionCode

-- Q2
SELECT CR.Name, SP.Name
FROM Person.CountryRegion CR INNER JOIN Person.StateProvince SP
	ON CR.CountryRegionCode = SP.CountryRegionCode
WHERE CR.Name LIKE 'Germany' OR CR.Name LIKE 'Canada'

-- Q3
SELECT SOH.SalesOrderID, SOH.OrderDate , SOH.SalesPersonID, SP.BusinessEntityID, SP.Bonus, SP.SalesYTD
FROM Sales.SalesOrderHeader SOH INNER JOIN Sales.SalesPerson SP
	ON SOH.SalesPersonID = SP.BusinessEntityID

-- Q4
SELECT SOH.SalesOrderID, SOH.OrderDate, E.Jobtitle, SOH.SalesPersonID, SP.BusinessEntityID, SP.Bonus, SP.SalesYTD
FROM Sales.SalesOrderHeader SOH INNER JOIN Sales.SalesPerson SP
	ON SOH.SalesPersonID = SP.BusinessEntityID
	INNER JOIN HumanResources.Employee E
	ON SOH.SalesPersonID = E.BusinessEntityID

-- Q5
SELECT SOH.SalesOrderID, SOH.OrderDate, P.FirstName, P.LastName, SOH.SalesPersonID, SP.BusinessEntityID, SP.Bonus
FROM Sales.SalesOrderHeader SOH INNER JOIN Sales.SalesPerson SP
	ON SOH.SalesPersonID = SP.BusinessEntityID
	INNER JOIN HumanResources.Employee E
	ON SOH.SalesPersonID = E.BusinessEntityID
	INNER JOIN Person.Person P
	ON SOH.SalesPersonID = P.BusinessEntityID

-- Q6
SELECT SOH.SalesOrderID, SOH.OrderDate, P.FirstName, P.LastName, SOH.SalesPersonID, SP.BusinessEntityID, SP.Bonus
FROM Sales.SalesOrderHeader SOH INNER JOIN Sales.SalesPerson SP
	ON SOH.SalesPersonID = SP.BusinessEntityID
	INNER JOIN Person.Person P
	ON SOH.SalesPersonID = P.BusinessEntityID

-- Q7
SELECT SOH.SalesOrderID, SOH.OrderDate, P.FirstName, P.LastName
FROM Sales.SalesOrderHeader SOH INNER JOIN Person.Person P
	ON SOH.SalesPersonID = P.BusinessEntityID

-- Q8
SELECT SOH.SalesOrderID, SOH.OrderDate, CONCAT(P.FirstName, ' ', P.LastName) AS SalesPerson, SOD.ProductID, SOD.OrderQty 
FROM Sales.SalesOrderHeader SOH INNER JOIN Person.Person P
	ON SOH.SalesPersonID = P.BusinessEntityID
	INNER JOIN Sales.SalesOrderDetail SOD
	ON SOH.SalesOrderID = SOD.SalesOrderID

-- Q9
SELECT SOH.SalesOrderID, SOH.OrderDate, CONCAT(P.FirstName, ' ', P.LastName) AS SalesPerson, PR.Name, SOD.OrderQty 
FROM Sales.SalesOrderHeader SOH INNER JOIN Person.Person P
	ON SOH.SalesPersonID = P.BusinessEntityID
	INNER JOIN Sales.SalesOrderDetail SOD
	ON SOH.SalesOrderID = SOD.SalesOrderID
	INNER JOIN Production.Product PR
	ON SOD.ProductID = PR.ProductID

-- Q10
SELECT SOH.SalesOrderID, SOH.OrderDate, CONCAT(P.FirstName, ' ', P.LastName) AS SalesPerson, PR.Name, SOD.OrderQty 
FROM Sales.SalesOrderHeader SOH INNER JOIN Person.Person P
	ON SOH.SalesPersonID = P.BusinessEntityID
	INNER JOIN Sales.SalesOrderDetail SOD
	ON SOH.SalesOrderID = SOD.SalesOrderID
	INNER JOIN Production.Product PR
	ON SOD.ProductID = PR.ProductID
WHERE SOH.TotalDue > 100000 AND YEAR(SOH.OrderDate) = 2004

-- Q11
SELECT CR.Name, SP.Name
FROM Person.CountryRegion CR LEFT JOIN Person.StateProvince SP
	ON CR.CountryRegionCode = SP.CountryRegionCode

-- Q12
SELECT C.CustomerID, SOH.SalesOrderID
FROM Sales.Customer C LEFT JOIN Sales.SalesOrderHeader SOH
	ON C.CustomerID = SOH.CustomerID

-- Q13
SELECT P.Name, PM.Name
FROM Production.Product P FULL JOIN Production.ProductModel PM
	ON P.ProductModelID = PM.ProductModelID