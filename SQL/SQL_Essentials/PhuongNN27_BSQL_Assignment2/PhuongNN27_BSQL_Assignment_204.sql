﻿DROP DATABASE MovieDb

CREATE DATABASE MovieDb

USE MovieDb

-- Q1

CREATE TABLE Movie (
	Id				INT IDENTITY(1,1)	PRIMARY KEY,
	[Name]			NVARCHAR(100)		NOT NULL,
	Duration		INT					NOT NULL,
	Genre			INT					NOT NULL,
	Director		NVARCHAR(100)		NOT NULL,
	AmountOfMoney	MONEY				NOT NULL,
	Comments		NVARCHAR(2500)		NULL
)

CREATE TABLE Actor (
	Id					INT IDENTITY(1,1)	PRIMARY KEY,
	[Name]				NVARCHAR(100)		NOT NULL,
	Age					INT					NOT NULL,
	AvarageSalaryMovie	MONEY				NOT NULL,
	Nationality			NVARCHAR(100)		NOT NULL
)

CREATE TABLE ActedIn (
	MovieId			INT					FOREIGN KEY REFERENCES Movie(Id),
	ActorId			INT					FOREIGN KEY REFERENCES Actor(Id)
)

ALTER TABLE Movie
ADD CONSTRAINT check_Movie_Genre CHECK(Genre BETWEEN 1 AND 8)

ALTER TABLE Movie
ADD CONSTRAINT check_Movie_Duration CHECK(Duration <= 60)

-- Q2

-- 1
ALTER TABLE Movie
ADD ImageLink VARCHAR(4000)

ALTER TABLE Movie
ADD CONSTRAINT check_Image_Value CHECK(ImageLink IS NOT NULL)

ALTER TABLE Movie
ADD CONSTRAINT check_Duplicated_Image UNIQUE(ImageLink)

-- 2
INSERT INTO dbo.Movie([Name], Duration, Genre, Director, AmountOfMoney, ImageLink)
VALUES (N'Tomb Raider', 60, 1, N'Simon West', 50000, N'https://m.media-amazon.com/images/M/MV5BNzMzODVjMWUtYmIxZS00NDlkLTlmNTktNjI5NTdhZjUzYzY1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_FMjpg_UX1000_.jpg'),
(N'Tôi Thấy Hoa Vàng Trên Cỏ Xanh', 60, 3, N'Victor Vũ', 24000, N'https://upload.wikimedia.org/wikipedia/vi/f/f8/Toithayhoavangtrencoxanh.jpg'),
(N'Kính Vạn Hoa', 30, 3, N'Nguyễn Minh Chung', 25000, N'https://images2.thanhnien.vn/Uploaded/thanhlongn/2022_11_18/kinh-van-hoa-9287.jpeg'),
(N'Resident Evil', 60, 6, N'Paul W.S. Anderson', 60000, N'https://m.media-amazon.com/images/M/MV5BZmI1ZGRhNDYtOGVjZC00MmUyLThlNTktMTQyZGE3MzE1ZTdlXkEyXkFqcGdeQXVyNDE5MTU2MDE@._V1_.jpg'),
(N'Green Miles', 60, 5, N'Frank Darabont', 30000, N'https://flxt.tmsimg.com/assets/p24429_p_v12_bf.jpg')

INSERT INTO dbo.Actor ([Name], AvarageSalaryMovie, Nationality, Age)
VALUES (N'Ahihi', 25000000, N'American', 48),
(N'Thịnh Vinh', 15000000, N'Việt Nam', 22),
(N'Quý Ròm', 2000000, N'Việt Nam', 34),
(N'Milla Jovovich', 35000000, N'American', 47),
(N'Tom Hanks', 60000000, N'American', 67)

UPDATE dbo.Actor
SET [Name] = N'Angelina Jolie'
WHERE Id = 1

INSERT INTO dbo.ActedIn (MovieId, ActorId)
VALUES (1,1),
(2,2),
(3,3),
(4,4),
(5,5)

-- Q1

SELECT *
FROM dbo.Actor A
WHERE A.Age > 50

-- Q2

SELECT A.[Name], AVG(A.AvarageSalaryMovie) AS [Average Salary]
FROM dbo.Actor A
GROUP BY A.[Name]
ORDER BY AVG(A.AvarageSalaryMovie)

-- Q3

SELECT A.[Name], STRING_AGG(M.[Name], ',') AS Movies
FROM dbo.Actor A INNER JOIN dbo.ActedIn AI
ON A.Id = AI.ActorId
INNER JOIN dbo.Movie M
ON M.Id = AI.MovieId
GROUP BY A.[Name]

-- Q4

SELECT M.[Name], COUNT(AI.MovieId) AS [Number of Actors]
FROM dbo.ActedIn AI INNER JOIN dbo.Movie M
ON M.Id = AI.MovieId
GROUP BY M.[Name]
HAVING COUNT(AI.MovieId) > 3