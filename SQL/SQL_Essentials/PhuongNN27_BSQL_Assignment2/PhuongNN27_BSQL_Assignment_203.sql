USE AdventureWorks2008R2

-- Ex1

-- Q1
SELECT P.ProductID, P.[Name], P.Color, P.ListPrice
FROM Production.Product P

-- Q2
SELECT P.ProductID, P.[Name], P.Color, P.ListPrice
FROM Production.Product P
WHERE P.ListPrice > 0

-- Q3

SELECT P.ProductID, P.[Name], P.Color, P.ListPrice
FROM Production.Product P
WHERE P.Color IS NULL

-- Q4

SELECT P.ProductID, P.[Name], P.Color, P.ListPrice
FROM Production.Product P
WHERE P.Color IS NOT NULL

-- Q5
SELECT P.ProductID, P.[Name], P.Color, P.ListPrice
FROM Production.Product P
WHERE P.Color IS NOT NULL AND P.ListPrice > 0

-- Q6
SELECT P.ProductID, P.[Name], P.Color, P.ListPrice
FROM Production.Product P
WHERE P.Color IS NOT NULL

-- Q7
SELECT CONCAT(P.[Name], ': ', P.Color) AS [Name And Color]
FROM Production.Product P
WHERE P.Color IS NOT NULL

-- Q8
SELECT CONCAT('NAME: ',P.[Name], '  --  COLOR: ', P.Color) AS [Name And Color]
FROM Production.Product P
WHERE P.Color IS NOT NULL

-- Q9
SELECT P.ProductID, P.[Name]
FROM Production.Product P
WHERE P.ProductID BETWEEN 400 AND 500

-- Q10
SELECT P.ProductID, P.[Name], P.Color
FROM Production.Product P
WHERE P.Color LIKE 'Black' OR P.Color LIKE 'White'

-- Q11
SELECT P.[Name], P.ListPrice
FROM Production.Product P
WHERE P.[Name] LIKE 'S%'

-- Q12
SELECT P.[Name], P.ListPrice
FROM Production.Product P
WHERE P.[Name] LIKE 'S%' OR P.[Name] LIKE 'A%'
ORDER BY P.[Name]

-- Q13
SELECT P.[Name], P.ListPrice
FROM Production.Product P
WHERE P.[Name] LIKE 'SPO%' AND P.[Name] NOT LIKE 'SPOK%'
ORDER BY P.[Name]

-- Q14
SELECT DISTINCT P.ProductSubcategoryID, P.Color
FROM Production.Product P
WHERE P.ProductSubcategoryID IS NOT NULL

-- Q15
SELECT DISTINCT P.ProductCategoryID, P.Color
FROM Production.Product P

-- Q16
SELECT ProductSubCategoryID
      , LEFT([Name],35) AS [Name]
      , Color, ListPrice
FROM Production.Product
WHERE Color IN ('Red','Black')
      OR ListPrice BETWEEN 1000 AND 2000
      AND ProductSubCategoryID = 1
ORDER BY ProductID

-- Q17

SELECT P.[Name],	CASE
						WHEN P.Color IS NULL THEN 'Unknown'
						ELSE P.Color
					END
, P.ListPrice
FROM Production.Product P

-- Ex2
-- Q1
SELECT COUNT(*)
FROM Production.Product P

-- Q2
SELECT COUNT(*)
FROM Production.Product P
WHERE P.ProductSubcategoryID IS NOT NULL

-- Q3
SELECT P.ProductSubcategoryID, COUNT(P.ProductID) AS CountedProducts
FROM Production.Product P 
GROUP BY P.ProductSubcategoryID

SELECT *
FROM SalesLT.ProductCategory PC

-- Q4
SELECT COUNT(*)
FROM Production.Product P 
WHERE P.ProductSubcategoryID IS NULL

-- Q5
SELECT PIV.ProductID, SUM(PIV.Quantity) AS TheSum
FROM Production.ProductInventory PIV
WHERE PIV.Quantity > 0
GROUP BY PIV.ProductID

-- Q6
SELECT PIV.ProductID, SUM(PIV.Quantity) AS TheSum
FROM Production.ProductInventory PIV
WHERE PIV.Quantity > 0 AND PIV.LocationID = 40
GROUP BY PIV.ProductID
HAVING SUM(PIV.Quantity) < 100

-- Q7
SELECT PIV.Shelf, PIV.ProductID, SUM(PIV.Quantity) AS TheSum
FROM Production.ProductInventory PIV
WHERE PIV.Quantity > 0 AND PIV.LocationID = 40
GROUP BY PIV.ProductID, PIV.Shelf
HAVING SUM(PIV.Quantity) < 100

-- Q8
SELECT AVG(PIV.Quantity) AS TheAvg
FROM Production.ProductInventory PIV
WHERE PIV.LocationID = 10

-- Q9
SELECT PIV.ProductID, PIV.Shelf, AVG(PIV.Quantity) AS TheAvg
FROM Production.ProductInventory PIV
WHERE PIV.LocationID = 10
GROUP BY PIV.ProductID, PIV.Shelf

-- Q10
SELECT P.Color, P.Class, COUNT(P.ProductID) AS TheCount, AVG(P.ListPrice) AS AvgPrice
FROM Production.Product P
GROUP BY P.Color, P.Class

-- Q11
SELECT P.ProductSubcategoryID
      , COUNT(P.Name) as Counted
	  
FROM Production.Product P
GROUP BY ROLLUP (ProductSubcategoryID)

select *
from Production.Product P
where p.ProductSubcategoryID IS NULL