﻿DROP DATABASE FTM

CREATE DATABASE FTM

USE FTM


-- Q1
CREATE TABLE Trainee (
	TraineeID			INT IDENTITY(1,1)	PRIMARY KEY,
	Full_Name			NVARCHAR(100)		NOT NULL,
	Birth_Date			DATETIME			NOT NULL,
	Gender				VARCHAR(6)			NOT NULL,
	ET_IQ				INT					NOT NULL,
	ET_Gmath			INT					NOT NULL,
	ET_English			INT					NOT NULL,
	Training_Class		VARCHAR(15)			NOT NULL,
	Evaluation_Notes	NVARCHAR(250)
)

ALTER TABLE dbo.Trainee
ADD CONSTRAINT check_Gender	CHECK(Gender = 'male' OR Gender = 'female')

ALTER TABLE dbo.Trainee
ADD CONSTRAINT check_IQ	CHECK(ET_IQ BETWEEN 0 AND 20)

ALTER TABLE dbo.Trainee
ADD CONSTRAINT check_Gmath CHECK(ET_Gmath BETWEEN 0 AND 20)

ALTER TABLE dbo.Trainee
ADD CONSTRAINT check_English CHECK(ET_English BETWEEN 0 AND 50)

-- Q2

ALTER TABLE dbo.Trainee
ADD Fsoft_Account VARCHAR(100)

ALTER TABLE dbo.Trainee
ADD CONSTRAINT check_Account CHECK (Fsoft_Account IS NOT NULL)

ALTER TABLE dbo.Trainee
ADD CONSTRAINT check_Unique_Account UNIQUE(Fsoft_Account)

-- Q1

INSERT INTO dbo.Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Fsoft_Account)
VALUES (N'Phong', CAST (N'1998-01-10T00:00:00.000' AS DATETIME), 'male', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'phongnguyen'),
(N'Thịnh', CAST (N'1989-02-10T00:00:00.000' AS DATETIME), 'male', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'thinhnguyen'),
(N'Lâm', CAST (N'1996-07-10T00:00:00.000' AS DATETIME), 'male', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'lamnguyen'),
(N'Bình', CAST (N'2001-11-10T00:00:00.000' AS DATETIME), 'male', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'binhnguyen'),
(N'Ngọc', CAST (N'1997-06-23T00:00:00.000' AS DATETIME), 'female', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'ngocnguyen'),
(N'Lan', CAST (N'1999-08-10T00:00:00.000' AS DATETIME), 'female', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'lannguyen'),
(N'Hương', CAST (N'1980-12-17T00:00:00.000' AS DATETIME), 'female', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'huongnguyen'),
(N'Hoa', CAST (N'1998-03-11T00:00:00.000' AS DATETIME), 'female', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'hoanguyen'),
(N'Linh', CAST (N'1978-03-15T00:00:00.000' AS DATETIME), 'female', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'linhnguyen'),
(N'Huệ', CAST (N'2000-04-11T00:00:00.000' AS DATETIME), 'female', FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(20-0+1)+0), FLOOR(RAND()*(50-0+1)+0), 'Fresher', 'huenguyen')

-- Q3

CREATE VIEW [ET Passed] AS
(
	SELECT * 
	FROM dbo.Trainee T
	WHERE	(T.ET_IQ + ET_Gmath) >= 20 AND ET_IQ >= 8 AND
			ET_Gmath >= 8 AND ET_English >= 18
)


-- Q4
SELECT T.TraineeID, T.Full_Name, MONTH (T.Birth_Date) AS [Month]
FROM dbo.Trainee T
WHERE	(T.ET_IQ + ET_Gmath) >= 20 AND ET_IQ >= 8 AND
			ET_Gmath >= 8 AND ET_English >= 18
ORDER BY DATEPART(MONTH, T.Birth_Date)

-- Q5
SELECT T.TraineeID, T.Full_Name, MONTH (T.Birth_Date) AS [Month]
FROM dbo.Trainee T
WHERE LEN(T.Full_Name) =	(
								SELECT MAX(LEN(Full_Name))
								FROM dbo.Trainee
							)