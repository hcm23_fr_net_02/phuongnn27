﻿DROP DATABASE FSOFT

CREATE DATABASE FSOFT

USE FSOFT

-- Q1

CREATE TABLE Department (
	DeptNo		INT IDENTITY(1,1)	PRIMARY KEY,
	DeptName	NVARCHAR(100)		NOT NULL,
	Note		NVARCHAR(250)		NULL
)

CREATE TABLE Skill (
	SkillNo		INT					PRIMARY KEY,
	SkillName	NVARCHAR(100)		NOT NULL,
	Note		NVARCHAR(250)		NULL
)

CREATE TABLE Employee (
	EmpNo		INT					PRIMARY KEY,
	EmpName		NVARCHAR(100)		NOT NULL,
	BirthDay	DATETIME			NOT NULL,
	DeptNo		INT					FOREIGN KEY REFERENCES Department(DeptNo),
	MgrNo		INT					NOT NULL,
	StartDate	DATETIME			NOT NULL,
	Salary		MONEY				NOT NULL,
	[Level]		INT					NOT NULL,
	[Status]	INT					NOT NULL,
	Note		NVARCHAR(250)		NULL
)

CREATE TABLE Emp_Skill (
	SkillNo			INT					FOREIGN KEY REFERENCES Skill(SkillNo),
	EmpNo			INT					FOREIGN KEY REFERENCES Employee(EmpNo),
	SkillLevel		INT					NOT NULL,
	RegDate			DATETIME			NOT NULL,
	[Description]	NVARCHAR(250)		NULL,			
	PRIMARY KEY (SkillNo, EmpNo)
)

-- Level
ALTER TABLE dbo.Employee
ADD CONSTRAINT check_Level CHECK ([Level] >= 1 AND [Level] <= 7)

-- Status
ALTER TABLE dbo.Employee
ADD CONSTRAINT check_Status CHECK ([Status] >= 0 AND [Status] <= 2)

-- SkillLevel
ALTER TABLE dbo.Emp_Skill
ADD CONSTRAINT check_SkillLevel CHECK (SkillLevel >= 1 AND SkillLevel <= 3)

-- Q2

-- Email
ALTER TABLE dbo.Employee
ADD Email NCHAR(30)

ALTER TABLE dbo.Employee
ADD CONSTRAINT check_Email CHECK (Email IS NOT NULL)

ALTER TABLE dbo.Employee
ADD CONSTRAINT check_Unique_Email UNIQUE(Email)

-- MgrNo and Status fields.

ALTER TABLE dbo.Employee
ADD CONSTRAINT set_MrgNo DEFAULT 0 FOR MgrNo


ALTER TABLE dbo.Employee
ADD CONSTRAINT set_Status DEFAULT 0 FOR [Status]

-- Q3
-- DeptNo
ALTER TABLE dbo.Employee
ADD CONSTRAINT [FK_1] FOREIGN KEY DeptNo
REFERENCES Department (DeptNo)

ALTER TABLE dbo.Emp_Skill
DROP COLUMN [Description]

-- Q4

INSERT INTO dbo.Skill (SkillNo, SkillName, Note)
VALUES (1, N'C', N'Kỹ năng C'),
(2, N'C++', N'Kỹ năng C++'),
(3, N'.NET', N'Kỹ năng .NET'),
(4, N'Java', N'Kỹ năng Java'),
(5, N'Kế toán', N'Kỹ năng kế toán'),
(6, N'Lọc hồ sơ', N'Kỹ năng lọc hồ sơ'),
(7, N'Tuyển dụng', N'Kỹ năng tuyển dụng'),
(8, N'Quản lý', N'Kỹ năng quản lý')

INSERT INTO dbo.Department ([DeptName])
VALUES (N'Kỹ thuật'),
(N'FA'),
(N'HR'),
(N'Thi công'),
(N'Bảo mật'),
(N'Nội vụ'),
(N'Kinh tế'),
(N'Kế toán')

INSERT INTO dbo.Employee ([EmpNo], [EmpName], [BirthDay], [DeptNo], [StartDate], [Salary], [Level], Email, [MgrNo], [Status])
VALUES (1, N'Phong', CAST(N'1998-01-10T00:00:00.000' AS DateTime), 1, CAST(N'2022-01-10T00:00:00.000' AS DateTime), 20000000, 2, N'phong@fpt.com', 2, 0),
(2, N'Thịnh', CAST(N'1989-02-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-03-10T00:00:00.000' AS DateTime), 35000000, 3, N'thinh@fpt.com', 0, 1),
(3, N'Lâm', CAST(N'1996-07-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-06-20T00:00:00.000' AS DateTime), 18000000, 2, N'lam@fpt.com', 2, 1),
(4, N'Bình', CAST(N'2001-11-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-07-15T00:00:00.000' AS DateTime), 10000000, 1, N'binh@fpt.com', 2, 1),
(5, N'Ngọc', CAST(N'1997-06-23T00:00:00.000' AS DateTime), 3, CAST(N'2022-11-10T00:00:00.000' AS DateTime), 16000000, 2, N'ngoc@fpt.com', 7, 0),
(6, N'Lan', CAST(N'1999-08-10T00:00:00.000' AS DateTime), 3, CAST(N'2023-01-10T00:00:00.000' AS DateTime), 8000000, 1, N'lan@fpt.com', 7, 1),
(7, N'Hương', CAST(N'1980-12-17T00:00:00.000' AS DateTime), 3, CAST(N'2022-01-10T00:00:00.000' AS DateTime), 24000000, 3, N'huong@fpt.com', 0, 1),
(8, N'Hoa', CAST(N'1998-03-11T00:00:00.000' AS DateTime), 8, CAST(N'2023-04-10T00:00:00.000' AS DateTime), 10000000, 2, N'hoa@fpt.com', 0, 1)

INSERT INTO dbo.Emp_Skill (SkillNo, EmpNo, RegDate, SkillLevel)
VALUES (2,1, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(3,2, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 3),
(1,3, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(4,4, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 1),
(6,5, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(6,6, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 1),
(6,7, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 3),
(8,8, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(4,2, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2)

-- View

CREATE VIEW EMPLOYEE_TRACKING AS
(
	SELECT E.EmpNo, E.EmpName AS [Emp_Name], E.[Level]
	FROM dbo.Employee E
	WHERE E.[Level] BETWEEN 3 AND 5 
)

SELECT * FROM EMPLOYEE_TRACKING