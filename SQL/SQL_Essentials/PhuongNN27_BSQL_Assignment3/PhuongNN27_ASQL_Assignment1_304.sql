﻿DROP DATABASE DMS

CREATE DATABASE DMS

USE DMS

USE [DMS]
GO
/****** Object:  Table [dbo].[EMPMAJOR]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPMAJOR](
	[emp_no] [char](6) NOT NULL,
	[major] [char](3) NOT NULL,
	[major_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[major] ASC
)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMP]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMP](
	[emp_no] [char](6) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[dept_no] [char](3) NOT NULL,
	[job] [varchar](50) NULL,
	[salary] [money] NOT NULL,
	[bonus] [money] NULL,
	[ed_level] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC
)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DEPT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DEPT](
	[dept_no] [char](3) NOT NULL,
	[dept_name] [varchar](50) NOT NULL,
	[mgn_no] [char](6) NULL,
	[admr_dept] [char](3) NOT NULL,
	[location] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[dept_no] ASC
),
UNIQUE NONCLUSTERED 
(
	[dept_name] ASC
)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMPPROJACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPPROJACT](
	[emp_no] [char](6) NOT NULL,
	[proj_no] [char](6) NOT NULL,
	[act_no] [int] NOT NULL,
 CONSTRAINT [PK_EPA] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[proj_no] ASC,
	[act_no] ASC
)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACT](
	[act_no] [int] NOT NULL,
	[act_des] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[act_no] ASC
)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Dept]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[DEPT]  WITH CHECK ADD  CONSTRAINT [FK_Dept] FOREIGN KEY([mgn_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[DEPT] CHECK CONSTRAINT [FK_Dept]
GO
/****** Object:  ForeignKey [FK__EMP__dept_no__3E52440B]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMP]  WITH CHECK ADD FOREIGN KEY([dept_no])
REFERENCES [dbo].[DEPT] ([dept_no])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_Major]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPMAJOR]  WITH CHECK ADD  CONSTRAINT [FK_Major] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPMAJOR] CHECK CONSTRAINT [FK_Major]
GO
/****** Object:  ForeignKey [FK_EPA1]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA1] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA1]
GO
/****** Object:  ForeignKey [FK_EPA2]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA2] FOREIGN KEY([act_no])
REFERENCES [dbo].[ACT] ([act_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA2]
GO

-- Q1

INSERT INTO dbo.ACT (act_no, act_des)
VALUES (90,N'HCM'),
(110,N'Can Tho'),
(3,N'Vung Tau'),
(4,N'Quy Nhon'),
(5,N'Ha Noi'),
(6,N'Đa Nang'),
(7,N'Ha Tinh'),
(8,N'Hoa Lac')

INSERT INTO dbo.DEPT(dept_no, dept_name, mgn_no, admr_dept, [location])
VALUES ('1', 'A', NULL, '11', 'HCM'),
('2', 'B', NULL, '21', 'Can Tho'),
('3', 'C', NULL, '31', 'HCM'),
('4', 'D', NULL, '41', 'Ha Noi'),
('5', 'E', NULL, '51', 'HCM'),
('6', 'F', NULL, '61', 'Hoa Lac'),
('7', 'G', NULL, '71', 'Quy Nhon'),
('8', 'H', NULL, '81', 'Da Nang')

INSERT INTO dbo.EMP(emp_no, last_name, first_name, dept_no, job, salary, bonus, ed_level)
VALUES ('1', 'Nguyen', 'Phong', '1', 'Dev', 15000000, 1000000, 1),
('2', 'Nguyen', 'Binh', '1', 'Tech Dev', 30000000, 2000000, 3),
('3', 'Tran', 'Phu', '1', 'Junior Dev', 24000000, 1500000, 2),
('4', 'Nguyen', 'Hue', '5', 'Accountant', 10000000, 500000, 1),
('5', 'Nguyen', 'Ngoc', '5', 'Lead Accountant', 24000000, 1500000, 3),
('6', 'Nguyen', 'Lan', '2', 'Accountant',10000000, 500000, 1),
('7', 'Nguyen', 'Hoa', '2', 'Lead Accountant', 23000000, 1500000, 3),
('8', 'Nguyen', 'Duong', '8', 'Lead HR', 1500000, 500000, 1)


INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('1', '1', 90)
INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('2', '2', 90)
INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('3', '3', 90)
INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('4', '4', 5)
INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('5', '5', 5)
INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('6', '6', 110)
INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('7', '7', 110)
INSERT INTO dbo.EMPPROJACT (proj_no, emp_no, act_no)
VALUES ('8', '8', 8)

INSERT INTO dbo.EMPMAJOR (major, major_name, emp_no)
VALUES ('1', 'CSI', 1),
('2', 'IT', 2),
('3', 'Block Chain', 3),
('4', 'Biz', 4),
('5', 'Biz', 5),
('6', 'MAT', 6),
('7', 'Biz', 7),
('8', 'HR', 8)

UPDATE dbo.DEPT
SET mgn_no = '2'
WHERE dept_no = 1

UPDATE dbo.DEPT
SET mgn_no = '5'
WHERE dept_no = 5

UPDATE dbo.DEPT
SET mgn_no = '7'
WHERE dept_no = 2

UPDATE dbo.DEPT
SET mgn_no = '8'
WHERE dept_no = 8

-- Q2
SELECT E.emp_no, E.first_name, EP.proj_no
FROM dbo.EMP E INNER JOIN dbo.EMPPROJACT EP ON E.emp_no = EP.emp_no


-- Q3
SELECT E.emp_no, E.first_name, EM.major_name
FROM dbo.EMP E INNER JOIN dbo.EMPMAJOR EM ON E.emp_no = EM.emp_no
WHERE EM.major_name IN ('CSI', 'MAT')

-- Q4
SELECT E.emp_no, E.first_name, EP.act_no
FROM dbo.EMP E INNER JOIN dbo.EMPPROJACT EP ON E.emp_no = EP.emp_no
WHERE EP.act_no BETWEEN 90 AND 110

-- Q5

SELECT E.dept_no, E.last_name, E.first_name, E.salary, E.dept_no,	(
																		SELECT AVG(ET.salary)
																		FROM dbo.EMP ET
																		WHERE ET.dept_no = E.dept_no
																	) AS DEPT_AVG_SAL
FROM dbo.EMP E

-- Q6
; WITH EdCTE AS 
(
	SELECT emp_no, first_name, dept_no, ed_level
	FROM dbo.EMP
	WHERE ed_level > (
						SELECT AVG(ET.ed_level)
						FROM dbo.EMP ET
					)
)
SELECT *
FROM EdCTE

-- Q7

CREATE FUNCTION CalculatePayroll(
	@DepNumber CHAR(3)
) 
RETURNS MONEY
AS
BEGIN
		RETURN ( 
					SELECT SUM(E.salary + E.bonus)
					FROM dbo.EMP E
					WHERE E.dept_no = @DepNumber
				)
END
GO


SELECT D.dept_no, D.dept_name, dbo.CalculatePayroll(D.dept_no) AS PayRoll
FROM dbo.DEPT D
WHERE dbo.CalculatePayroll(D.dept_no) = (
						SELECT MAX(dbo.CalculatePayroll(DT.dept_no))
						FROM dbo.DEPT DT
					)

-- Q8
SELECT TOP 5 E.emp_no, E.first_name, E.dept_no, E.salary
FROM dbo.EMP E
ORDER BY E.salary