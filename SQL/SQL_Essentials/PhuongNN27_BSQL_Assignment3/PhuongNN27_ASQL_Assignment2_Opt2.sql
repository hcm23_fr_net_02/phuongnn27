﻿IF DB_ID('EMS') IS NOT NULL
	DROP DATABASE EMS
ELSE
	CREATE DATABASE EMS

USE EMS

/*
1. Write SQL statements for following activities & 
print out respectively the screenshots to show test data 
(the table data that you create to test each query) & query results:
*/

CREATE TABLE [dbo].[Employee](
        [EmpNo] [int] NOT NULL,
        [EmpName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [BirthDay] [datetime] NOT NULL,
        [DeptNo] [int] NOT NULL,
        [MgrNo] [int] NOT NULL,
        [StartDate] [datetime] NOT NULL,
        [Salary] [money] NOT NULL,
        [Status] [int] NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL,
        [Level] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE Employee
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Level] CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO

ALTER TABLE [dbo].[Employee]  ADD  CONSTRAINT [chk_Status] CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))
GO

ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30)
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] ADD CONSTRAINT chk_Email1 UNIQUE(Email)
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo
GO

ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status
GO

CREATE TABLE [dbo].[Skill](
        [SkillNo] [int] IDENTITY(1,1) NOT NULL,
        [SkillName] [nchar](30) COLLATE
         SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)
GO

CREATE TABLE [dbo].[Department](
        [DeptNo] [int] IDENTITY(1,1) NOT NULL,
        [DeptName] [nchar](30) COLLATE
        SQL_Latin1_General_CP1_CI_AS NOT NULL,
        [Note] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)
GO

CREATE TABLE [dbo].[Emp_Skill](
        [SkillNo] [int] NOT NULL,
        [EmpNo] [int] NOT NULL,
        [SkillLevel] [int] NOT NULL,
        [RegDate] [datetime] NOT NULL,
        [Description] [nchar](100) COLLATE
        SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

/*
2. Add at least 8 records into each created tables.
*/

INSERT INTO dbo.Skill (SkillName, Note)
VALUES (N'C', N'Kỹ năng C'),
(N'C++', N'Kỹ năng C++'),
(N'.NET', N'Kỹ năng .NET'),
(N'Java', N'Kỹ năng Java'),
(N'Kế toán', N'Kỹ năng kế toán'),
(N'Lọc hồ sơ', N'Kỹ năng lọc hồ sơ'),
(N'Tuyển dụng', N'Kỹ năng tuyển dụng'),
(N'Quản lý', N'Kỹ năng quản lý')

INSERT INTO dbo.Department ([DeptName])
VALUES (N'Kỹ thuật'),
(N'FA'),
(N'HR'),
(N'Thi công'),
(N'Bảo mật'),
(N'Nội vụ'),
(N'Kinh tế'),
(N'Kế toán')

INSERT INTO dbo.Employee ([EmpNo], [EmpName], [BirthDay], [DeptNo], [StartDate], [Salary], [Level], Email, [MgrNo], [Status])
VALUES (1, N'Phong', CAST(N'1998-01-10T00:00:00.000' AS DateTime), 1, CAST(N'2022-01-10T00:00:00.000' AS DateTime), 20000000, 2, N'phong@fpt.com', 2, 0),
(2, N'Thịnh', CAST(N'1989-02-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-03-10T00:00:00.000' AS DateTime), 35000000, 3, N'thinh@fpt.com', 0, 1),
(3, N'Lâm', CAST(N'1996-07-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-06-20T00:00:00.000' AS DateTime), 18000000, 2, N'lam@fpt.com', 2, 1),
(4, N'Bình', CAST(N'2001-11-10T00:00:00.000' AS DateTime), 1, CAST(N'2023-07-15T00:00:00.000' AS DateTime), 10000000, 1, N'binh@fpt.com', 2, 1),
(5, N'Ngọc', CAST(N'1997-06-23T00:00:00.000' AS DateTime), 3, CAST(N'2022-11-10T00:00:00.000' AS DateTime), 16000000, 2, N'ngoc@fpt.com', 7, 0),
(6, N'Lan', CAST(N'1999-08-10T00:00:00.000' AS DateTime), 3, CAST(N'2023-01-10T00:00:00.000' AS DateTime), 8000000, 1, N'lan@fpt.com', 7, 1),
(7, N'Hương', CAST(N'1980-12-17T00:00:00.000' AS DateTime), 3, CAST(N'2022-01-10T00:00:00.000' AS DateTime), 24000000, 3, N'huong@fpt.com', 0, 1),
(8, N'Hoa', CAST(N'1998-03-11T00:00:00.000' AS DateTime), 8, CAST(N'2023-04-10T00:00:00.000' AS DateTime), 10000000, 2, N'hoa@fpt.com', 0, 1)

INSERT INTO dbo.Emp_Skill (SkillNo, EmpNo, RegDate, SkillLevel)
VALUES (2,1, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(3,2, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 3),
(1,3, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(4,4, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 1),
(6,5, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(6,6, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 1),
(6,7, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 3),
(8,8, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2),
(4,2, CAST(N'1998-03-11T00:00:00.000' AS DateTime), 2)

/*
Write a stored procedure (without parameter) 
to update employee level to 2 of the employees that has employee level = 1 
and has been working at least three year ago (from starting date). 
Print out the number updated records.
*/

CREATE PROC IncreaseEmpLevel
AS
	IF (EXISTS (
					SELECT E.EmpNo
					FROM dbo.Employee E
					WHERE E.[Level] = 1 AND YEAR(E.StartDate) >= 3
				))
		DECLARE @UpdateRows INT
		
		SELECT @UpdateRows = COUNT(E.EmpNo)
		FROM dbo.Employee E
		WHERE E.[Level] = 1 AND YEAR(E.StartDate) >= 3
		
		UPDATE dbo.Employee
		SET [Level] = 2
		WHERE EmpNo IN (
							SELECT E.EmpNo
							FROM dbo.Employee E
							WHERE E.[Level] = 1 AND YEAR(E.StartDate) >= 3
						)
		IF @@ROWCOUNT = 0  
		PRINT 'Warning: No rows were updated';
		ELSE
		PRINT @UpdateRows + ' is/are updated'
GO

EXEC IncreaseEmpLevel

SELECT *
FROM dbo.Employee E
where e.EmpNo between 4 and 6

/*
UPDATE dbo.Employee
SET [Level] = 1
WHERE EmpNo = 4

UPDATE dbo.Employee
SET [Level] = 1
WHERE EmpNo = 6
*/

/*
4. Write a stored procedure (with EmpNo parameter) 
to print out employee’s name, employee’s email address 
and department’s name of employee that has been out.
*/
CREATE PROC ShowEmpInfo
	@EmpNo INT
AS
	SET NOCOUNT ON;

	DECLARE @DepartNo INT

	SET @DepartNo = (
						SELECT D.DeptNo
						FROM dbo.Employee E INNER JOIN dbo.Department D
							ON E.DeptNo = D.DeptNo
						WHERE E.EmpNo = @EmpNo
					)

	IF (@DepartNo IS NOT NULL)
		SELECT E.EmpName, E.Email, (
										SELECT STRING_AGG(D.DeptName, ', ')
										FROM dbo.Department D
										WHERE D.DeptNo != @DepartNo
									) AS [Other departments]
		FROM dbo.Employee E
		WHERE E.EmpNo = @EmpNo
GO

EXEC ShowEmpInfo 1

/*
5. Write a user function named Emp_Tracking (with EmpNo parameter) 
that return the salary of the employee has been working.
*/

CREATE FUNCTION Emp_Tracking (@EmpNo INT)
RETURNS MONEY
	BEGIN
		RETURN (
					SELECT E.Salary
					FROM dbo.Employee E
					WHERE E.EmpNo = @EmpNo
				)
	END

SELECT * FROM dbo.Emp_Tracking(1)

/*
6. Write the trigger(s) to prevent the case that 
the end user to input invalid employees information (level = 1 and salary >10.000.000).
*/

CREATE TRIGGER CheckNewEmployee
ON dbo.Employee
FOR INSERT, UPDATE
AS
	BEGIN
		DECLARE @EmpLevel	INT
		DECLARE @EmpSalary	MONEY

		SELECT @EmpLevel = I.Level, @EmpSalary = I.Salary
		FROM inserted I

		IF (@EmpLevel = 1 AND @EmpSalary > 10000000)
			PRINT 'Cannot insert/update due to level and salary'
			ROLLBACK TRANSACTION
	END

-- Test
INSERT INTO dbo.Employee ([EmpNo], [EmpName], [BirthDay], [DeptNo], [StartDate], [Salary], [Level], Email, [MgrNo], [Status])
VALUES (20, N'Phong', CAST(N'1998-01-10T00:00:00.000' AS DateTime), 1, CAST(N'2022-01-10T00:00:00.000' AS DateTime), 10000000, 1, N'phong1@fpt.com', 2, 0)