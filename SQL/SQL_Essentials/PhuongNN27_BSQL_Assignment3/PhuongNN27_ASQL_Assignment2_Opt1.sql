﻿DROP DATABASE ASQL2OPT1

CREATE DATABASE ASQL2OPT1

USE ASQL2OPT1

/*
a. Create the tables (with the most appropriate field/column constraints & types) 
and add at least 3 records into each created table.
*/

CREATE TABLE Employee (
	EmployeeID				INT	IDENTITY(1,1)	PRIMARY KEY,
	EmployeeLastName		NVARCHAR(50)		NOT NULL,
	EmployeeFirstName		NVARCHAR(50)		NOT NULL,
	EmployeeHireDate		DATETIME			NOT NULL,
	EmployeeStatus			BIT					DEFAULT(1),
	SupervisorID			INT					FOREIGN KEY REFERENCES Employee(EmployeeID),
	SocialSecurityNumber	VARCHAR(13)			NOT NULL
)

CREATE TABLE Projects (
	ProjectID				INT IDENTITY(1,1)	PRIMARY KEY,
	ProjectManagerID		INT					FOREIGN KEY REFERENCES Employee(EmployeeID),					
	ProjectName				NVARCHAR(100)		NOT NULL,
	ProjectStartDate		DATETIME			NOT NULL,
	ProjectDescription		NVARCHAR(250)		NULL,
	ProjectDetail			NVARCHAR(4000)		NULL,
	ProjectCompletedOn		DATETIME			NULL
)

CREATE TABLE Project_Modules (
	ModuleID					INT IDENTITY(1,1)	PRIMARY KEY,
	ProjectID					INT					FOREIGN KEY REFERENCES Projects(ProjectID),
	EmployeeID					INT					FOREIGN KEY REFERENCES Employee(EmployeeID),
	ProjectModulesDate			DATETIME			NULL,
	ProjectModulesCompletedOn	DATETIME			NULL,
	ProjectModulesDescription	NVARCHAR(1000)		NULL
)

CREATE TABLE Work_Done (
	WorkDoneID					INT IDENTITY(1,1)	PRIMARY KEY,
	ModuleID					INT					FOREIGN KEY REFERENCES Project_Modules(ModuleID),
	WorkDoneDate				DATETIME			NULL,
	WorkDoneDescription			NVARCHAR(1000)		NULL,
	WorkDoneStatus				BIT					DEFAULT(0)
)

INSERT INTO dbo.Employee(EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SupervisorID, SocialSecurityNumber)
VALUES (N'Nguyễn', N'Bình', CAST (N'2022-01-02T00:00:00.000' AS DATETIME), 1, NULL, '1'),
(N'Trần', N'Phú', CAST (N'2019-12-10T00:00:00.000' AS DATETIME), 1, NULL, '2'),
(N'Nguyễn', N'An', CAST (N'2023-01-10T00:00:00.000' AS DATETIME), 1, NULL, '3'),
(N'Nguyễn', N'Dương', CAST (N'2019-05-23T00:00:00.000' AS DATETIME), 1, 1, '4'),
(N'Trần', N'Linh', CAST (N'2022-01-10T00:00:00.000' AS DATETIME), 1, 1, '5'),
(N'Nguyễn', N'Tuấn', CAST (N'2023-07-17T00:00:00.000' AS DATETIME), 1, 2, '6'),
(N'Lê', N'Huệ', CAST (N'2022-12-10T00:00:00.000' AS DATETIME), 1, 2, '7'),
(N'Trịnh', N'Khang', CAST (N'2022-08-07T00:00:00.000' AS DATETIME), 1, 2, '8')

INSERT INTO dbo.Projects (ProjectManagerID, ProjectName, ProjectStartDate)
VALUES (3, N'Project C', CAST (N'2022-09-10T00:00:00.000' AS DATETIME)),
(2, N'Project B', CAST (N'2023-09-10T00:00:00.000' AS DATETIME)),
(1, N'Project A', CAST (N'2023-01-10T00:00:00.000' AS DATETIME))

INSERT INTO dbo.Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn)
VALUES (1, 8, CAST (N'2022-09-10T00:00:00.000' AS DATETIME), CAST (N'2022-09-10T23:00:00.000' AS DATETIME)),
(1, 5, CAST (N'2022-09-11T00:00:00.000' AS DATETIME), CAST (N'2022-09-11T23:00:00.000' AS DATETIME)),
(2, 4, CAST (N'2023-09-10T00:00:00.000' AS DATETIME), CAST (N'2023-09-10T23:00:00.000' AS DATETIME)),
(2, 5, CAST (N'2023-09-11T00:00:00.000' AS DATETIME), CAST (N'2023-09-11T23:00:00.000' AS DATETIME))

INSERT INTO dbo.Work_Done(ModuleID, WorkDoneDate, WorkDoneStatus)
VALUES (1,CAST (N'2022-09-10T00:00:00.000' AS DATETIME), 1),
(2,CAST (N'2022-09-11T00:00:00.000' AS DATETIME), 1),
(3,CAST (N'2023-09-10T00:00:00.000' AS DATETIME), 1),
(4,CAST (N'2023-09-11T00:00:00.000' AS DATETIME), 1)

/*
b. Write a stored procedure (with parameter) 
to print out the modules that a specific employee has been working on.
*/

CREATE PROC EmployeeProjects
AS
	SET NOCOUNT ON;
	SELECT	E.EmployeeID, 
			CONCAT(E.EmployeeLastName, ' ', E.EmployeeFirstName) AS FullName, 
			P.ProjectID,
			P.ProjectName,
			P.ProjectStartDate,
			P.ProjectCompletedOn
	FROM dbo.Employee E INNER JOIN dbo.Projects P
		ON E.EmployeeID = P.ProjectManagerID
	
	SELECT E.EmployeeID, 
			CONCAT(E.EmployeeLastName, ' ', E.EmployeeFirstName) AS FullName, 
			PM.ModuleID,
			PM.ProjectModulesDate,
			PM.ProjectModulesCompletedOn
	FROM dbo.Employee E INNER JOIN dbo.Project_Modules PM
		ON E.EmployeeID = PM.EmployeeID
GO

EXEC EmployeeProjects

/*
c. Write a user function (with parameter) 
that return the Works information that a specific employee 
has been involving though those were not assigned to him/her.
*/

CREATE FUNCTION UnassignedProjectModule (@ID INT)
RETURNS TABLE
AS
	RETURN
			SELECT	E.EmployeeID, 
					CONCAT(E.EmployeeLastName, ' ', E.EmployeeFirstName) AS FullName, 
					PM.ModuleID,
					PM.ProjectModulesDate,
					PM.ProjectModulesCompletedOn
			FROM	dbo.Employee E FULL OUTER JOIN dbo.Project_Modules PM
					ON E.EmployeeID = PM.EmployeeID
			WHERE	E.EmployeeID = @ID

SELECT * FROM dbo.UnassignedProjectModule(1)

CREATE FUNCTION UnassignedProject (@ID INT)
RETURNS TABLE
AS
	RETURN
			SELECT	E.EmployeeID, 
					CONCAT(E.EmployeeLastName, ' ', E.EmployeeFirstName) AS FullName, 
					P.ProjectID,
					P.ProjectName,
					P.ProjectStartDate,
					P.ProjectCompletedOn
			FROM	dbo.Employee E FULL OUTER JOIN dbo.Projects P
					ON E.EmployeeID = P.ProjectManagerID
			WHERE	E.EmployeeID = @ID

SELECT * FROM dbo.UnassignedProject(1)


/*
d. Write the trigger(s) to prevent the case 
that the end user to input invalid Projects and Project Modules information
(Project_Modules.ProjectModulesDate < Projects.ProjectStartDate, 
Project_Modules.ProjectModulesCompletedOn > Projects.ProjectCompletedOn).
*/

CREATE TRIGGER CheckProjectModulesInput
ON dbo.Project_Modules
FOR INSERT,UPDATE
AS
	BEGIN
			DECLARE @ProjectID	INT
			DECLARE @ProjectModulesDate DATETIME
			DECLARE @ProjectModulesCompletedOn DATETIME

			SELECT	@ProjectID = I.ProjectID, 
					@ProjectModulesDate = I.ProjectModulesDate,
					@ProjectModulesCompletedOn = I.ProjectModulesCompletedOn
			FROM inserted I

			IF (EXISTS (
							SELECT P.ProjectID
							FROM dbo.Projects P
							WHERE P.ProjectID = @ProjectID
						))
				

				IF (@ProjectModulesCompletedOn IS NOT NULL)
					IF (@ProjectModulesCompletedOn > (
														SELECT P.ProjectCompletedOn
														FROM dbo.Projects P
														WHERE P.ProjectID = @ProjectID
													))
						PRINT 'Module date is earlier than project start date'
						ROLLBACK TRANSACTION

				IF (@ProjectModulesDate IS NOT NULL)
					IF (@ProjectModulesDate < (
													SELECT P.ProjectStartDate
													FROM dbo.Projects P
													WHERE P.ProjectID = @ProjectID
												))
						PRINT 'Module Complete date is later than project complete date'
						ROLLBACK TRANSACTION

	END

-- Test
INSERT INTO dbo.Project_Modules (ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesCompletedOn)
VALUES (1, 8, CAST (N'2022-09-09T00:00:00.000' AS DATETIME), CAST (N'2022-09-10T23:00:00.000' AS DATETIME))

SELECT * FROM dbo.Project_Modules