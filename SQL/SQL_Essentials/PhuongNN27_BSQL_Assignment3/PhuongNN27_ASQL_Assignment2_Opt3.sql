USE AdventureWorks2008R2

-- Problem 1
/*
Writes an UDF returns 
ProductID, Name, ProductNumber, Color and ListPrice for products 
that have 'CA' in the ProductNumber and Color is 'black'
*/
CREATE FUNCTION PrintCAAndBlackProducts ()
RETURNS TABLE
AS
	RETURN (
				SELECT	P.ProductID, 
						P.Name, 
						P.ProductNumber, 
						P.Color, 
						P.ListPrice
				FROM	Production.Product P
				WHERE	P.ProductNumber LIKE 'CA%'
						AND P.Color LIKE 'Black'
			)

SELECT * FROM PrintCAAndBlackProducts()

/*
Writes an inline table UDF that accept an integer parameter named BusinessEntityID and 
returns the associated address of a business entity.
*/

CREATE FUNCTION GetAddressFromBusinessEntityID (@BusinessEntityID INT)
RETURNS TABLE
AS
	RETURN (
				SELECT BEA.AddressID, BEA.AddressTypeID, A.AddressLine1, A.AddressLine2
				FROM Person.BusinessEntity BE INNER JOIN Person.BusinessEntityAddress BEA
					ON BE.BusinessEntityID = BEA.BusinessEntityID
					INNER JOIN Person.Address A
					ON BEA.AddressID = A.AddressID
				WHERE BE.BusinessEntityID = @BusinessEntityID
			)

-- Test
SELECT * FROM GetAddressFromBusinessEntityID(1)

-- Problem 2

/*
In the AdventureWorks2008 database or up, writes a stored proc meet below requirements:
- Accept 2 parameters: @ModifiedDate (datetime) and @UpperFlag (bit)
- Returns FirstName from Person.Person table if ModifedDate is greater than @ModifiedDate, 
FirstName will be uppercase if @UpperFlag is true.
*/

CREATE PROC GetFirstName 
	@ModifiedDate DATETIME, 
	@UpperFlag BIT
AS
	IF (EXISTS (
					SELECT COUNT(*)
					FROM Person.Person P
					WHERE DATEDIFF(	DAY, 
									CAST (@ModifiedDate AS datetime), 
									CAST (P.ModifiedDate AS datetime)) > 0
		))
		IF (@UpperFlag = 1)
			SELECT UPPER(P.FirstName)
			FROM Person.Person P
			WHERE DATEDIFF(	DAY, 
							CAST (@ModifiedDate AS datetime), 
							CAST (P.ModifiedDate AS datetime)) > 0
		ELSE
			SELECT P.FirstName
			FROM Person.Person P
			WHERE DATEDIFF(	DAY, 
							CAST (@ModifiedDate AS datetime), 
							CAST (P.ModifiedDate AS datetime)) > 0
GO

-- Test
EXEC GetFirstName 'Dec 31 2008 12:00AM', 0

-- Problem 3
CREATE TABLE Production.ProductInventoryAudit (

       ProductID           INT NOT NULL,
       LocationID          SMALLINT NOT NULL,
       Shelf               NVARCHAR(10) NOT NULL,
       Bin                 TINYINT NOT NULL,
       Quantity            SMALLINT NOT NULL,
       rowguid             UNIQUEIDENTIFIER NOT NULL,
       ModifiedDate        DATETIME NOT NULL,
       InsertOrDelete      CHAR(1) NOT NULL
)

/*
Writes a trigger to ensure that 
each time user insert or delete records from Production.ProductInventory 
then these records will be captured in Production.ProductInventoryAudit table.
*/

CREATE TRIGGER Production.CopyDataFromProductInventoryToAudit
ON Production.ProductInventory
FOR INSERT, DELETE
AS
	BEGIN
		IF (EXISTS (
						SELECT *
						FROM inserted
		))
			INSERT INTO Production.ProductInventoryAudit
			(ProductID, LocationID, Shelf, Bin, Quantity, ModifiedDate, InsertOrDelete)
			SELECT I.ProductID, I.LocationID, I.Shelf, I.Bin, I.Quantity, I.ModifiedDate, 'i'
			FROM inserted I
			
			IF @@ROWCOUNT = 0
				PRINT 'Cannot update data into audit'
		ELSE
			INSERT INTO Production.ProductInventoryAudit
			(ProductID, LocationID, Shelf, Bin, Quantity, ModifiedDate, InsertOrDelete)
			SELECT D.ProductID, D.LocationID, D.Shelf, D.Bin, D.Quantity, D.ModifiedDate, 'd'
			FROM deleted D
			
			IF @@ROWCOUNT = 0
				PRINT 'Cannot update data into audit'
	END

INSERT INTO Production.ProductInventory (ProductID, LocationID, Shelf, Bin, Quantity, rowguid, ModifiedDate)
VALUES(1, 7,'A',1, 408,'47A24246-6C43-48EB-968F-025738A8A412', '2023-09-08 00:00:00.000')