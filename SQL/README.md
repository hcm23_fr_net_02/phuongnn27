## SQL
SQL folder includes CodeLearn assignments. There are 3 folders:

### SQL Essentials
1. ERD and Schemas
- [Assignment 1](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_101?ref_type=heads)
- [Assignment Opt 2](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_Opt02?ref_type=heads)
- [Assignment Opt 3](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_Opt03?ref_type=heads)
- [Assignment Opt 4](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment1_Opt4?ref_type=heads)
2. Basic SQL
- DDL
- DML
- Group By, Having, Join, Subquery
- [Assignment 2](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment2?ref_type=heads)
- [Assignment 3](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Essentials/PhuongNN27_BSQL_Assignment3?ref_type=heads)

### SQL Advanced
1. Info
- Join
- Subquery
- CTE
- Trigger
- Store procedure
- Function
2. [Assignments](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/SQL_Advanced?ref_type=heads)

### Test
This includes 2 assignments for test [Test](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/SQL/Test?ref_type=heads)