public class Customer
{
	public int Id { get; set; }
	public string FullName { get; set; }
	public string Email { get; set; }
	public string Address { get; set; }
	public List<Order> Orders { get; set; }
} 

public class Product
{
	public int Id { get; set; }
	public string ProductName { get; set; }
	public decimal Price { get; set; }
	public int StockQuantity { get; set; }
	public List<OrderDetail> OrderDetails { get; set; }
}

public class Order
{
	public int Id { get; set; }
	public int CustomerId { get; set; }
	public DateTime OrderDate { get;set }
	public decimal TotalPrice { get; set; }
	public Customer customer { get; set; }
	public List<OrderDetail> OrderDetails { get; set; }
}

public class OrderDetail
{
	public int Id { get; set; }
	public int OrderId { get; set; }
	public int ProductId { get; set; }
	public int Quantity { get; set; }
	public Order order { get; set; }
	public Product product { get; set; }
}