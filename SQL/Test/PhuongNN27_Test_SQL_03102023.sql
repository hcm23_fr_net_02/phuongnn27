﻿CREATE DATABASE PhuongNN27Ass02

USE PhuongNN27Ass02


-- 1. Tạo database và mối quan hệ đúng(30 điểm)
CREATE TABLE Customers (
	ID				INT IDENTITY(1,1)	PRIMARY KEY,
	FullName		NVARCHAR(100),
	Email			NVARCHAR(100),
	[Address]		NVARCHAR(100)
)

CREATE TABLE Products (
	ID				INT IDENTITY(1,1)	PRIMARY KEY,
	ProductName		NVARCHAR(100),
	Price			DECIMAL(10,2),
	StockQuantity	INT
)

CREATE TABLE Orders (
	ID				INT IDENTITY(1,1)	PRIMARY KEY,
	CustomerID		INT					FOREIGN KEY REFERENCES Customers(ID),
	OrderDate		DateTime			DEFAULT(GETDATE()),
	TotalPrice		DECIMAL(10,2)		DEFAULT(0)
)

CREATE TABLE OrderDetails (
	ID				INT IDENTITY(1,1)	PRIMARY KEY,
	OrderID			INT					FOREIGN KEY REFERENCES Orders(ID),
	ProductID		INT					FOREIGN KEY REFERENCES Products(ID),
	Quantity		INT					DEFAULT(0)
)

ALTER TABLE dbo.Products
ADD CONSTRAINT check_price CHECK(Price > 0)

ALTER TABLE dbo.Products
ADD CONSTRAINT check_price CHECK(StockQuantity >= 0)

/*
2. Thêm ít nhất 10 bản ghi vào mỗi bảng (Customers, Products, Orders, OrderDetails) để tạo dữ liệu thử nghiệm cho cơ sở dữ liệu. (10 điểm) Chú ý: Bạn có thể sử dụng dữ liệu giả mạo từ https://www.mockaroo.com/ để tạo và điều chỉnh dữ liệu trong cơ sở dữ liệu của bạn.
*/

insert into Customers (FullName, Email, Address) values ('Cooper', 'chayes0@taobao.com', 'Hà Nội');
insert into Customers (FullName, Email, Address) values ('Tanner', 'tmulvihill1@boston.com', 'PO Box 43098');
insert into Customers (FullName, Email, Address) values ('Nahum', 'ndooly2@skype.com', 'Hà Nội');
insert into Customers (FullName, Email, Address) values ('Rhodie', 'rolivey3@g.co', 'Hà Nội');
insert into Customers (FullName, Email, Address) values ('Thomas', 'tmedmore4@flavors.me', 'Hồ Chí Minh');
insert into Customers (FullName, Email, Address) values ('Mick', 'mpotten5@dot.gov', 'Suite 64');
insert into Customers (FullName, Email, Address) values ('Sibeal', 'sreyson6@illinois.edu', 'Hồ Chí Minh');
insert into Customers (FullName, Email, Address) values ('Pepe', 'pather7@cocolog-nifty.com', 'Suite 78');
insert into Customers (FullName, Email, Address) values ('Jada', 'jdoog8@netlog.com', 'Hồ Chí Minh');
insert into Customers (FullName, Email, Address) values ('Vida', 'vcroxley9@prnewswire.com', 'Hồ Chí Minh');
insert into Customers (FullName, Email, Address) values ('Phuong', 'vcroxley9@prnewswire.com', 'Hồ Chí Minh');


insert into Products (ProductName, Price, StockQuantity) values ('Beans - Turtle, Black, Dry', '71367.30', 85);
insert into Products (ProductName, Price, StockQuantity) values ('Wood Chips - Regular', '15564.11', 93);
insert into Products (ProductName, Price, StockQuantity) values ('Mikes Hard Lemonade', '98258.42', 168);
insert into Products (ProductName, Price, StockQuantity) values ('Soup - Campbells Mushroom', '8102.85', 8);
insert into Products (ProductName, Price, StockQuantity) values ('Banana - Green', '2900.77', 14);
insert into Products (ProductName, Price, StockQuantity) values ('Bread - Assorted Rolls', '58931.39', 73);
insert into Products (ProductName, Price, StockQuantity) values ('Pork - Back, Short Cut, Boneless', '31755.87', 149);
insert into Products (ProductName, Price, StockQuantity) values ('Saskatoon Berries - Frozen', '24262.40', 11);
insert into Products (ProductName, Price, StockQuantity) values ('Tea - Earl Grey', '54140.29', 81);
insert into Products (ProductName, Price, StockQuantity) values ('Squid - Breaded', '93871.73', 147);

insert into Orders (CustomerID, OrderDate, TotalPrice) values (1, '2022-12-14', '53097.79');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (2, '2023-09-26', '84580.26');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (3, '2023-08-27', '8467.88');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (4, '2023-05-13', '99702.40');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (5, '2023-03-16', '54611.52');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (6, '2023-04-28', '1411.07');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (7, '2023-06-25', '131416.11');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (8, '2023-09-23', '153759.65');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (9, '2023-02-03', '7242.49');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (10, '2022-10-21', '189860.22');
insert into Orders (CustomerID, OrderDate, TotalPrice) values (10, '2022-10-21', '600860.22');

insert into OrderDetails (OrderID, ProductID, Quantity) values (1, 1, 1);
insert into OrderDetails (OrderID, ProductID, Quantity) values (2, 2, 2);
insert into OrderDetails (OrderID, ProductID, Quantity) values (3, 3, 3);
insert into OrderDetails (OrderID, ProductID, Quantity) values (4, 4, 4);
insert into OrderDetails (OrderID, ProductID, Quantity) values (5, 5, 5);
insert into OrderDetails (OrderID, ProductID, Quantity) values (6, 6, 6);
insert into OrderDetails (OrderID, ProductID, Quantity) values (7, 7, 7);
insert into OrderDetails (OrderID, ProductID, Quantity) values (8, 8, 8);
insert into OrderDetails (OrderID, ProductID, Quantity) values (9, 9, 9);
insert into OrderDetails (OrderID, ProductID, Quantity) values (10, 10, 10);
insert into OrderDetails (OrderID, ProductID, Quantity) values (11, 10, 10);


/*3. Truy vấn SQL để lấy danh sách tên đầy đủ và email của tất cả khách hàng có địa chỉ chứa thông tin là ở thành phố hoặc tỉnh bắt đầu bằng chữ "H" (ví dụ: Hà Nội, Hồ Chí Minh, Hà Tĩnh...) và sắp xếp theo tên đầy đủ của khách hàng tăng dần (10 điểm)*/
SELECT		C.FullName, C.Email
FROM		dbo.Customers C
WHERE		C.[Address] LIKE 'H%'
ORDER BY	C.FullName ASC

/*4. Truy vấn SQL để lấy danh sách tên sản phẩm và giá của tất cả sản phẩm có giá lớn hơn 100 và số lượng trong kho ít hơn 50, sắp xếp theo giá sản phẩm giảm dần (10 điểm).*/
SELECT		P.ProductName, P.Price
FROM		dbo.Products P
WHERE		P.Price > 100 AND P.StockQuantity < 50
ORDER BY	P.Price DESC
/*5. Truy vấn SQL để lấy danh sách tên đầy đủ và email của tất cả khách hàng chưa đặt đơn hàng nào, sắp xếp theo tên đầy đủ của khách hàng (tăng dần) .(10 điểm)*/
SELECT	C.FullName, C.Email
FROM	dbo.Customers C
WHERE	C.ID NOT IN		(
							SELECT		CT.ID
							FROM		dbo.Orders O INNER JOIN dbo.Customers CT
										ON CT.ID = O.CustomerID
							GROUP BY	CT.ID
						)
/*6. Truy vấn SQL để lấy tên sản phẩm (ProductName), giá sản phẩm (Price), số lượng (Quantity) Thông tin về các sản phẩm đã được mua trong các đơn hàng có giá trị tổng (TotalPrice) lớn hơn 500,000. (10 điểm)*/
SELECT	P.ProductName, P.Price, P.StockQuantity
FROM	dbo.Products P INNER JOIN dbo.OrderDetails OD
		ON P.ID = OD.ProductID
		INNER JOIN dbo.Orders O
		ON OD.OrderID = O.ID
WHERE	O.TotalPrice > 500000

/*7. Lấy danh sách tên khách hàng và tháng. Mà trong tháng đó khách hàng này đó đã chi tiêu hơn 500,000 đồng cho tổng các đơn hàng của họ.(20 điểm)*/
SELECT		C.FullName, MONTH(O.OrderDate) AS [Month of orders]
FROM		dbo.Customers C INNER JOIN dbo.Orders O
			ON C.ID = O.CustomerID
GROUP BY	C.FullName, MONTH(O.OrderDate)
HAVING		SUM(O.TotalPrice) > 500000