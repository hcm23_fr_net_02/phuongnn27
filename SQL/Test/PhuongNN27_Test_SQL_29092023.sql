﻿/*
PhuongNN27
Cở sở dữ liệu quản lý bánh hàng

 

KhachHang(MaKH, HoTen, DiaChi, SoDT, NgSinh, Doanhso, NgDk)
NhanVien(MaNV, HoTen, SoDT, NgVaoLam)
SanPham(MaSP, TenSP, DVT, NuocSX, Gia)
HoaDon(SoHD, NgHD, MaKH, MaNV, TriGia)
CTHD(SoHD, MaSP, SL)

 

Tạo bảng và thêm vào mỗi bảng 3 dòng dữ liệu

 

Câu 1: In ra danh sách các sản phẩm(MaSP, TenSP) do "Viet Nam" sản xuất hoặc các sản phẩm bán trong ngày 23/052023
Câu 2: In ra danh sách các sản phẩm (MaSP, TenSP) không bán được
Câu 3: Tìm số hóa đon đã mua tất cả các sản phẩm do Viet Nam sản xuất
*/


CREATE DATABASE CakeManagement

USE CakeManagement

-- Tạo bảng

CREATE TABLE KhachHang (
	MaKH		INT IDENTITY(1,1)	PRIMARY KEY, 
	HoTen		NVARCHAR(100)		NOT NULL, 
	DiaChi		NVARCHAR(1000)		NULL, 
	SoDT		VARCHAR(25)			NULL, 
	NgSinh		DATETIME			NULL, 
	Doanhso		INT					DEFAULT (0), 
	NgDk		DATETIME			DEFAULT (GETDATE())
)

CREATE TABLE NhanVien (
	MaNV		INT IDENTITY(1,1)	PRIMARY KEY, 
	HoTen		NVARCHAR(100)		NOT NULL, 
	SoDT		VARCHAR(25)			NULL, 
	NgVaoLam	DATETIME			NOT NULL
)

CREATE TABLE SanPham (
	MaSP		INT IDENTITY(1,1)	PRIMARY KEY, 
	TenSP		NVARCHAR(100)		NOT NULL, 
	DVT			NVARCHAR(25)		NOT NULL, 
	NuocSX		NVARCHAR(100)		NOT NULL, 
	Gia			MONEY				NOT NULL
)

CREATE TABLE HoaDon (
	SoHD		INT IDENTITY(1,1)	PRIMARY KEY, 
	NgHD		DATETIME			DEFAULT(GETDATE()), 
	MaKH		INT					FOREIGN KEY REFERENCES KhachHang(MaKH), 
	MaNV		INT					FOREIGN KEY REFERENCES NhanVien(MaNV),
	TriGia		MONEY				NOT NULL
)

CREATE TABLE CTHD (
	SoHD		INT					FOREIGN KEY REFERENCES HoaDon(SoHD),
	MaSP		INT					FOREIGN KEY REFERENCES SanPham(MaSP), 
	SL			INT					NOT NULL
)

-- Đưa dữ liệu

INSERT INTO dbo.NhanVien (HoTen, NgVaoLam)
VALUES (N'Nguyễn Bình', CAST('2022-01-10' AS datetime)),
(N'Trương Hạnh', CAST('2022-09-10' AS datetime)),
(N'Trần Phú', CAST('2022-04-10' AS datetime))

INSERT INTO dbo.KhachHang (HoTen, NgDk)
VALUES (N'Nguyễn Hoa', CAST('2022-01-10' AS datetime)),
(N'Trương Ký', CAST('2022-09-10' AS datetime)),
(N'Trần Toản', CAST('2022-04-10' AS datetime))

INSERT INTO dbo.SanPham (TenSP, DVT, NuocSX, Gia)
VALUES (N'Kem', N'Đồng', N'Việt Nam', 10000),
(N'Nước ngọt Pepsi', N'Đồng', N'Việt Nam', 15000),
(N'Bánh quy', N'Đồng', N'Việt Nam', 20000),
(N'Kitkat', N'Đồng', N'Mỹ', 20000)

INSERT INTO dbo.HoaDon (MaKH, MaNV, TriGia, NgHD)
VALUES (1, 1, 10000, CAST(N'2023-05-23' AS datetime)),
(1, 2, 25000, CAST(GETDATE() AS datetime)),
(2, 2, 10000, CAST(GETDATE() AS datetime))

INSERT INTO dbo.HoaDon (MaKH, MaNV, TriGia, NgHD)
VALUES (2, 2, 40000, CAST(GETDATE() AS datetime))

INSERT INTO dbo.CTHD(SoHD, MaSP, SL)
VALUES (1, 1, 1),
(2, 1, 1),
(2, 2, 1),
(3, 1, 1)

INSERT INTO dbo.CTHD(SoHD, MaSP, SL)
VALUES (4, 3, 1),
(4, 4, 1)

SELECT * FROM dbo.HoaDon

-- Câu 1
SELECT	SP.MaSP, SP.TenSP
FROM	dbo.HoaDon HD INNER JOIN dbo.CTHD CT
		ON HD.SoHD = CT.SoHD
		INNER JOIN dbo.SanPham SP
		ON CT.MaSP = SP.MaSP
WHERE HD.NgHD LIKE CAST(N'2023-05-23' AS datetime)

-- Câu 2
SELECT	SP.MaSP, SP.TenSP
FROM	dbo.SanPham SP
WHERE	SP.MaSP NOT IN	(
							SELECT	SP.MaSP
							FROM	dbo.CTHD CT INNER JOIN dbo.SanPham SP
									ON CT.MaSP = SP.MaSP
							GROUP BY SP.MaSP
						)

-- Câu 3

-- Đơn hàng có thể chứa SP không phải từ Việt Nam
SELECT	HD.SoHD, HD.NgHD, HD.TriGia
FROM	dbo.HoaDon HD INNER JOIN dbo.CTHD CT
		ON HD.SoHD = CT.SoHD
		INNER JOIN dbo.SanPham SP
		ON CT.MaSP = SP.MaSP
WHERE	SP.NuocSX LIKE N'Việt Nam'

-- Đơn hàng chỉ chứa hàng Việt Nam (chất lượng cao :) )
SELECT		HD.SoHD, HD.NgHD, HD.TriGia
FROM		dbo.HoaDon HD INNER JOIN dbo.CTHD CT
			ON HD.SoHD = CT.SoHD
			INNER JOIN dbo.SanPham SP
			ON CT.MaSP = SP.MaSP
GROUP BY	HD.SoHD, HD.NgHD, HD.TriGia
HAVING SUM(	CASE 
			WHEN SP.NuocSX LIKE N'Việt Nam' 
				THEN 1 
			ELSE 0 
			END) = COUNT(*)
