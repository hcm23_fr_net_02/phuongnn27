﻿using LibraryManagement.Presentations.Implements;
using LibraryManagement.Presentations.Interfaces;
using LibraryManagement.Repositories.Implements;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Implements;
using LibraryManagement.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryManagement.AppStart
{
    public static class DependencyInjectionResolver
    {
        public static IServiceCollection ConfigureDependencyInjection(this IServiceCollection services)
        {
            //Unit of work
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //Generic
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

            //Book
            services.AddScoped<IBookRepository, BookRepository>();

            //Loan
            services.AddScoped<ILoanRepository, LoanRepository>();

            //Customer
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            
            //Customer
            services.AddScoped<IReviewRepository, ReviewRepository>();

            //Service
            services.AddScoped<ILibraryPresentation, LibraryPresentation>();
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<ILoanService, LoanService>();

            //Presentation
            services.AddScoped<IBookPresentation, BookPresentation>();
            services.AddScoped<IReviewPresentation, ReviewPresentation>();
            services.AddScoped<ILoanPresentation, LoanPresentation>();
            services.AddScoped<ICustomerPresentation, CustomerPresentation>();

            return services;
        }
    }
}
