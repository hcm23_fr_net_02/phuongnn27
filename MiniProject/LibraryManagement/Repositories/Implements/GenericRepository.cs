using LibraryManagement.Commons;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Utils;

namespace LibraryManagement.Repositories.Implements
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly List<T> Entities;

        public GenericRepository() => Entities = new List<T>();

        public void AddRange(IEnumerable<T> entities) => Entities.AddRange(entities);

        public void Create(T entity) => Entities.Add(entity);

        public void Delete(Predicate<T> predicate)
        {
            var item = Entities.Find(predicate);
            if (item != null)
                Delete(item);
        }

        public void Delete(T entity) => Entities.Remove(entity);

        public List<T> Get() => Entities;

        public List<T> GetEntities(Func<T, bool> predicate) => Entities.Where(predicate).ToList();

        public T Get(Predicate<T> predicate) => Entities.Find(predicate)!;

        public bool IsEmpty() => !Entities.Any();

        public void Update(T entity, Func<T, bool> predicate)
        {
            var item = Entities.SingleOrDefault(predicate);
            if (item != null)
            {
                var index = Entities.IndexOf(item);
                Entities[index] = entity;
            }
        }

        public void GetData(string path, string errorMessage, bool RemovedOldData = false)
        {
            var temp = JSonUtils<T>.ReadJsonIntoData(path);

            if (temp == null || !temp.Any())
            {
                throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_READ_DATA_FROM_JSON} {LibraryConstants.BOOK.ToLower()}");
            }

            if (RemovedOldData)
            {
                Entities.Clear();
            }
            Entities.AddRange(temp);
        }
    }
}
