using LibraryManagement.Repositories.Interfaces;

namespace LibraryManagement.Repositories.Implements
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Fields
        private readonly IBookRepository _bookRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly ILoanRepository _loanRepository;
        private readonly IReviewRepository _reviewRepository;
        #endregion

        #region Constructor
        public UnitOfWork(IBookRepository bookRepository,
            ICustomerRepository customerRepository,
            ILoanRepository loanRepository,
            IReviewRepository reviewRepository)
        {
            _bookRepository = bookRepository;
            _customerRepository = customerRepository;
            _loanRepository = loanRepository;
            _reviewRepository = reviewRepository;
        }
        #endregion

        #region Properties

        public IBookRepository BookRepository => _bookRepository;

        public ICustomerRepository CustomerRepository => _customerRepository;

        public ILoanRepository LoanRepository => _loanRepository;

        public IReviewRepository ReviewRepository => _reviewRepository;

        #endregion
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
