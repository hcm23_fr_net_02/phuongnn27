﻿using LibraryManagement.Entities;
using LibraryManagement.Repositories.Interfaces;

namespace LibraryManagement.Repositories.Implements
{
    public class LoanRepository : GenericRepository<Loan>, ILoanRepository
    {
    }
}
