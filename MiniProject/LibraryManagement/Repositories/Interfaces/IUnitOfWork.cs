namespace LibraryManagement.Repositories.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        #region Repository
        IBookRepository BookRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        ILoanRepository LoanRepository { get; }
        IReviewRepository ReviewRepository { get; }

        #endregion
    }
}
