﻿using LibraryManagement.Entities;

namespace LibraryManagement.Repositories.Interfaces
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
    }
}
