﻿namespace LibraryManagement.Repositories.Interfaces
{
    public interface IGenericRepository<TEntities>
    {
        /// <summary>
        /// Get all data
        /// </summary>
        /// <returns></returns>
        List<TEntities> Get();

        /// <summary>
        /// Get data with filter
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        List<TEntities> GetEntities(Func<TEntities, bool> predicate);

        /// <summary>
        /// Get an entity by filter
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        TEntities Get(Predicate<TEntities> predicate);

        /// <summary>
        /// Add an entity
        /// </summary>
        /// <param name="Entity"></param>
        void Create(TEntities Entity);

        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="Entities"></param>
        /// <param name="predicate"></param>
        void Update(TEntities entity, Func<TEntities, bool> predicate);

        /// <summary>
        /// Delete an Entities by filter
        /// </summary>
        /// <param name="predicate"></param>
        void Delete(Predicate<TEntities> predicate);

        /// <summary>
        /// Delete an entity by parameterized Entities
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntities entity);

        /// <summary>
        /// Add range of entities
        /// </summary>
        /// <param name="entities"></param>
        void AddRange(IEnumerable<TEntities> entities);

        /// <summary>
        /// Check list of Entities is empty. If it is empty, it returns true. Otherwise, it returns false;
        /// </summary>
        /// <returns></returns>
        bool IsEmpty();

        /// <summary>
        /// Get data for entity
        /// </summary>
        /// <param name="path"></param>
        void GetData(string path, string errorMessage, bool RemovedOldData = false);
    }
}
