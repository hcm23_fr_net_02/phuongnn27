﻿using LibraryManagement.Entities;

namespace LibraryManagement.Repositories.Interfaces
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
    }
}
