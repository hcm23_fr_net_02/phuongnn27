﻿using LibraryManagement.Entities;

namespace LibraryManagement.Repositories.Interfaces
{
    public interface ILoanRepository : IGenericRepository<Loan>
    {
    }
}
