﻿using LibraryManagement.Entities;

namespace LibraryManagement.Repositories.Interfaces
{
    public interface IBookRepository : IGenericRepository<Book>
    {
    }
}
