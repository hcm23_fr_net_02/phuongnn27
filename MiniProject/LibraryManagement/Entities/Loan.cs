﻿using LibraryManagement.Commons;
using LibraryManagement.Exceptions;
using static LibraryManagement.Commons.LibraryEnum;

namespace LibraryManagement.Entities
{
    public class Loan
    {
        #region Fields
        private int _id;
        private int _customerId;
        private DateTime _borrowedDate;
        private DateTime _dueDate;
        private DateTime? _returnedDate;
        private int _status;
        private Book? _book;
        private Customer? _customer;
        #endregion

        public Loan()
        {

        }

        public Loan(int id, int customerId, DateTime borrowedDate,
            DateTime dueDate, Book book,
            Customer customer,
            int status = (int)LoanStatus.Borrowing,
            DateTime? returnedDate = null)
        {
            _id = id;
            _customerId = customerId;
            _borrowedDate = borrowedDate;
            _returnedDate = returnedDate;
            _status = status;
            _dueDate = dueDate;
            _book = book;
            _customer = customer;
        }

        #region Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public int CustomerId
        {
            get => _customerId;
            set => _customerId = value;
        }

        public int Status
        {
            get => _status;
            set => _status = value;
        }

        public DateTime BorrowedDate
        {
            get => _borrowedDate;
            set => _borrowedDate = value;
        }

        public DateTime DueDate
        {
            get => _dueDate;
            set
            {
                if (DateTime.Compare(_borrowedDate, value) >= 0)
                {
                    throw new LibraryException($"{LibraryConstants.LOAN_DUE_DATE} {LibraryConstants.EARLIER_THAN_OR_EQUAL_TO_DATE} {LibraryConstants.LOAN_BORROWED_DATE}");
                }
                _dueDate = value;
            }
        }

        public DateTime? ReturnedDate
        {
            get => _returnedDate;
            set
            {
                if (value.HasValue && DateTime.Compare(_borrowedDate, (DateTime)value) > 0)
                {
                    throw new LibraryException($"{LibraryConstants.LOAN_RETURNED_DATE} {LibraryConstants.EARLIER_THAN_DATE} {LibraryConstants.LOAN_BORROWED_DATE}");
                }
                _returnedDate = value;
            }
        }

        public Book Book
        {
            get => _book!;
            set => _book = value;
        }
        #endregion

        #region 

        public string Show(bool IsCustomer = false)
        {
            string message = $"{_id} - " +
                (IsCustomer ? $"{LibraryConstants.CUSTOMER} {LibraryConstants.CUSTOMER_ID}{LibraryConstants.MESSAGE_INPUT}{_customerId} - " +
                $"{LibraryConstants.CUSTOMER} {LibraryConstants.CUSTOMER_NAME}{LibraryConstants.MESSAGE_INPUT}{_customer!.Name}" : string.Empty)
                + $"\n{LibraryConstants.LOAN_STATUS}{LibraryConstants.MESSAGE_INPUT}{StatusExtension<LoanStatus>.GetStatus(Status)}"
                + $"\n{LibraryConstants.LOAN_BORROWED_DATE} {_borrowedDate} - " +
                $"{LibraryConstants.LOAN_DUE_DATE} {_dueDate}\n" +
                (ReturnedDate.HasValue ? $"{LibraryConstants.LOAN_RETURNED_DATE} {_returnedDate}\n" : string.Empty);

            message += $"{LibraryConstants.MESSAGE_LINE_1}\n";
            message += $"{_book}";
            message += $"{LibraryConstants.MESSAGE_LINE_1}\n";

            return message;
        }

        public override string ToString() => Show(true);

        #endregion
    }
}
