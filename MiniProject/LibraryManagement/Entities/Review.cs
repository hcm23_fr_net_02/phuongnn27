﻿using LibraryManagement.Commons;

namespace LibraryManagement.Entities
{
    public class Review
    {
        #region Fields
        private int _id;
        private int? _customerId;
        private int _bookId;
        private string _title;
        private string _content;
        private int _rate;
        private DateTime _reviewDate;
        private Customer? _customer;
        private Book _book;
        #endregion

        public Review(int id, int bookId, string title,
            string content, int rate, DateTime reviewDate,
            Book book, int? customerId = null, Customer? customer = null)
        {
            _id = id;
            _bookId = bookId;
            _title = title;
            _content = content;
            _rate = rate;
            _reviewDate = reviewDate;
            _book = book;
            if (customer != null) 
            {
                _customer = customer;
            }

            if (customerId.HasValue)
            {
                _customerId = customerId;
            }
        }

        #region Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }
        
        public int BookId
        {
            get => _bookId;
            set => _bookId = value;
        }

        public int Rate
        {
            get => _rate;
            set => _rate = value;
        }

        public int? CustomerId
        {
            get => _customerId;
            set => _customerId = value;
        }

        public DateTime ReviewDate
        {
            get => _reviewDate;
            set => _reviewDate = value;
        }

        public string Title
        {
            get => _title;
            set => _title = value;
        }

        public string Content
        {
            get => _content;
            set => _content = value;
        }

        public Book Book
        {
            get => _book;
            set => _book = value;
        }

        public Customer? Customer
        {
            get => _customer;
            set => _customer = value;
        }
        #endregion

        #region Method
        public string Show()
        {
            string message = $"{_id} - " +
                (Customer != null ? $"{_customer?.Name}\n" : $"{LibraryConstants.REVIEW_UNKNOWN_CUSTOMER}\n");
            message += $"{LibraryConstants.MESSAGE_LINE_1}\n";
            message += $"{ReviewDate}\n" +
                $"{_rate}\n" +
                $"{_title}\n" +
                $"{_content}\n";
            message += $"{LibraryConstants.MESSAGE_LINE_1}\n";
            return message;
        }
        #endregion
    }
}
