﻿using LibraryManagement.Commons;

namespace LibraryManagement.Entities
{
    public class Customer
    {
        #region Fields
        private int? _id;
        private string? _name;
        private string? _address;
        private List<Loan>? _loans;
        #endregion

        public Customer()
        {

        }

        public Customer(int Id, string Name, string Address, List<Loan>? Loans = null)
        {
            _id = Id;
            _name = Name;
            _address = Address;

            if (Loans == null)
                _loans = new List<Loan>();
            else
                _loans = Loans;
        }

        #region Properties

        public int? Id
        {
            get => _id;
            set => _id = value;
        }

        public string Name
        {
            get => _name!;
            set => _name = value.Trim();
        }

        public string Address
        {
            get => _address!;
            set => _address = value.Trim();
        }

        public List<Loan> Loans
        {
            get => _loans!;
            set => _loans = value;
        }
        #endregion

        #region Methods
        public void AddLoan(Loan loan) => _loans!.Add(loan);

        public string Show(bool ShowAll = false)
        {
            string message = $"{_id} - {_name} - {_address}\n";
            if (ShowAll && _loans!.Any())
            {
                message += $"{LibraryConstants.MESSAGE_LINE_2}\n";
                _loans!.ForEach(loan => message += $"{loan.Show(true)}\n");
                message += $"{LibraryConstants.MESSAGE_LINE_2}\n";
            }
            return message;
        }

        public override string ToString() => Show();
        public Customer GetCustomerValue() => new Customer((int)_id!, _name!, _address!);
        #endregion
    }
}
