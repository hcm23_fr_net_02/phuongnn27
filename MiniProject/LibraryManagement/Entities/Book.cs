﻿using LibraryManagement.Commons;
using LibraryManagement.Exceptions;

namespace LibraryManagement.Entities
{
    public class Book
    {
        #region Fields
        private int _id;
        private string? _title;
        private string? _author;
        private int _publicationYear;
        private int _quantity;
        private string? _description;
        private List<string>? _genres;
        private List<Review>? _reviews;
        #endregion

        public Book()
        {

        }

        public Book(int id) => _id = id;

        public Book(int id, string title)
        {
            _id = id;
            _title = title;
        }

        public Book(int id, string title, string author,
            int publicationYear, int quantity, string description, List<string>? genres = null,
            List<Review>? reviews = null)
        {
            _id = id;
            _title = title;
            _author = author;
            _publicationYear = publicationYear;
            _quantity = quantity;
            _description = description;

            if (genres != null)
            {
                _genres = genres;
            }
            else
            {
                _genres = new List<string>();
            }

            if (reviews != null)
            {
                _reviews = reviews;
            }
            else
            {
                _reviews = new List<Review>();
            }
        }

        #region Properties
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        public string Title
        {
            get => _title!;
            set => _title = value;
        }

        public string Author
        {
            get => _author!;
            set => _author = value;
        }

        public int Quantity
        {
            get => _quantity;
            set
            {
                if (value < 0)
                    throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_SMALLER_INTEGER} 0");
                if (value > DateTime.Now.Year)
                    throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_LARGER_INTEGER} {DateTime.Now.Year}");
                _quantity = value;
            }
        }

        public int PublicationYear
        {
            get => _publicationYear;
            set
            {
                if (value < 0)
                    throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_SMALLER_INTEGER} 0");
                _publicationYear = value;
            }
        }
        public string Description
        {
            get => _description!;
            set => _description = value;
        }

        public List<string> Genres
        {
            get => _genres!;
            set => _genres = value;
        }

        public List<Review> Reviews
        {
            get => _reviews!;
            set => _reviews = value;
        }
        #endregion

        #region Method

        public void AddReview(Review review) => _reviews!.Add(review);

        public void AddGenre(string genre) => _genres!.Add(genre);

        public override string ToString()
        {
            string message = $"{_id} - {_title} - {_author} - {_quantity} - {_publicationYear}\n";

            if (_genres!.Any())
            {
                _genres!.ForEach(g => message += $"{g}, ");
                message += "\n";
            }

            message += (string.IsNullOrEmpty(_description) ? string.Empty : $"{_description}\n");
            return message;
        }

        public Book GetBookValue() => new Book(_id, _title!, _author!, _publicationYear, _quantity, _description!, _genres);
        #endregion
    }

}
