﻿using LibraryManagement.Entities;
namespace LibraryManagement.Services.Interfaces
{
    public interface IReviewService
    {
        Book CreateReview(Review review, Book book);
    }
}
