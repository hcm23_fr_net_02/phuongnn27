﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;

namespace LibraryManagement.Services.Interfaces
{
    public interface ICustomerService
    {
        bool AddCustomers(Customer customer);
        Customer UpdateCustomer(Customer customer);
        bool DeleteCustomer(Customer customer);
    }
}
