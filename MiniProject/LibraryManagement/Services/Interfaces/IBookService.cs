﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;

namespace LibraryManagement.Services.Interfaces
{
    public interface IBookService
    {
        public Book SearchBook();
        bool AddBook(Book book);
        Book UpdateBook(Book book);
        bool DeleteBook(Book book);
        List<Book> FilterBooks(Sorting sorting);
    }
}
