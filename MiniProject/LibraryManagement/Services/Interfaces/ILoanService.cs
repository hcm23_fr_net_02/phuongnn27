﻿using LibraryManagement.Entities;

namespace LibraryManagement.Services.Interfaces
{
    public interface ILoanService
    {
        #region Loan
        bool CreateLoan(Loan loan, Customer customer);
        List<Loan> CheckOutLoan(Loan loan, List<Loan>? loans = null);
        #endregion
    }
}
