﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using LibraryManagement.Utils;
using static LibraryManagement.Commons.LibraryEnum;

namespace LibraryManagement.Services.Implements
{
    public class LoanService : ILoanService
    {
        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public LoanService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        #endregion

        #region Methods
        public List<Loan> CheckOutLoan(Loan loan, List<Loan>? loans = null)
        {
            List<Loan>? result = null;
            if (_unitOfWork.LoanRepository.Get(l => l.Id.Equals(loan.Id)) == null)
            {
                throw new LibraryException($"{LibraryConstants.LOAN} {LibraryConstants.ERROR_NOT_FOUND}");
            }

            if (loan.Status.Equals((int)LoanStatus.CheckedOut))
            {
                throw new LibraryException($"{LibraryConstants.LOAN} - {loan.Id} {LibraryConstants.ERROR_LOAN_INVALID_CHECK_OUT_STATUS}");
            }

            var customer = _unitOfWork.CustomerRepository.Get(c => c.Id.Equals(loan.CustomerId));

            if (customer == null)
            {
                throw new LibraryException($"{LibraryConstants.CUSTOMER} {LibraryConstants.ERROR_NOT_FOUND}");
            }


            //Set values
            loan.Status = (int)LoanStatus.CheckedOut;
            loan.ReturnedDate = DateTime.Now;

            //Set customer's updated loan
            var _loan = customer.Loans.SingleOrDefault(l => l.Id.Equals(loan.Id));
            if (_loan == null)
            {
                throw new LibraryException($"{LibraryConstants.CUSTOMER} {LibraryConstants.LOAN.ToLower()} {LibraryConstants.ERROR_NOT_FOUND}");
            }
            customer.Loans[customer.Loans.IndexOf(_loan!)] = loan;

            //Update loan
            _unitOfWork.LoanRepository.Update(loan, l => l.Id.Equals(loan.Id));

            //Update book
            UpdateBookByLoan(loan, true);

            //Update customer
            _unitOfWork.CustomerRepository.Update(customer, c => c.Id.Equals(customer.Id));

            if (loans != null)
            {
                //Update list of loans
                var temp = loans!.SingleOrDefault(l => l.Id.Equals(loan.Id));
                loans![loans.IndexOf(temp!)] = loan;
                result = loans;
            }
            else
            {
                result = new List<Loan>() { loan! };
            }

            //Show message
            ServiceUtils.ShowMessage(new List<string>()
                {
                    $"{LibraryConstants.MESSAGE_LOAN_CHECK_OUT}\n",
                    $"{LibraryConstants.MESSAGE_LINE_1}\n",
                    loan.Show(),
                    $"\n {LibraryConstants.MESSAGE_LINE_1}",
                });

            return result!;
        }

        public bool CreateLoan(Loan loan, Customer customer)
        {
            var result = false;
            if (_unitOfWork.LoanRepository.Get(l => l.Id.Equals(loan.Id)) != null)
            {
                throw new LibraryException($"{LibraryConstants.LOAN} - {loan.Id} {LibraryConstants.ERROR_MESSAGE_IS_EXISTED}");
            }

            if (_unitOfWork.CustomerRepository.Get(c => c.Id.Equals(customer.Id)) == null)
            {
                throw new LibraryException($"{LibraryConstants.CUSTOMER} - {customer.Id} {LibraryConstants.ERROR_NOT_FOUND}");
            }

            //Customer
            customer.Loans.Add(loan);
            _unitOfWork.CustomerRepository.Update(customer, c => c.Id.Equals(customer.Id));

            //Loan
            _unitOfWork.LoanRepository.Create(loan);
            result = true;

            //book
            UpdateBookByLoan(loan);

            return result;
        }
        #endregion

        #region Utils
        private void UpdateBookByLoan(Loan loan, bool IsCheckOut = false)
        {
            //Update book
            var book = _unitOfWork.BookRepository.Get(b => b.Id.Equals(loan.Book.Id));
            if (book != null)
            {
                if (IsCheckOut)
                {
                    book.Quantity += 1;
                }
                else
                {
                    book.Quantity -= 1;
                }
                _unitOfWork.BookRepository.Update(book, b => b.Id.Equals(loan.Book.Id));
            }
        }
        #endregion
    }
}