﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using LibraryManagement.Utils;

namespace LibraryManagement.Services.Implements
{
    public class CustomerService : ICustomerService
    {
        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Method

        public bool AddCustomers(Customer customer)
        {
            if (_unitOfWork.CustomerRepository.Get(c => c.Id.Equals(customer.Id)) != null)
            {
                throw new LibraryException($"{LibraryConstants.CUSTOMER} - {customer.Id} {LibraryConstants.ERROR_MESSAGE_IS_EXISTED}");
            }
            //Customer
            _unitOfWork.CustomerRepository.Create(customer);

            ServiceUtils.ShowMessage(new List<string>()
            {
                LibraryConstants.MESSAGE_ADD,
                LibraryConstants.CUSTOMER.ToLower(),
                LibraryConstants.MESSAGE_SUCCESS
            });
            return true;
        }

        public bool DeleteCustomer(Customer customer)
        {
            var result = false;

            //Check if book is existed in review(s) or loan(s)
            var check = _unitOfWork.ReviewRepository.GetEntities(r => r.CustomerId!.Equals(customer.Id)).Any() ||
                _unitOfWork.LoanRepository.GetEntities(l => l.CustomerId.Equals(customer.Id)).Any();

            if (check)
            {
                throw new LibraryException($"{LibraryConstants.MESSAGE_DELETE} " +
                    $"{LibraryConstants.CUSTOMER.ToLower()} ({customer.Id} - {customer.Name}) " +
                    $"{LibraryConstants.MESSAGE_FAIL} {LibraryConstants.ERROR_RELATED_INFO}");
            }

            _unitOfWork.CustomerRepository.Delete(customer);

            ServiceUtils.ShowMessage($"{LibraryConstants.MESSAGE_DELETE} " +
                    $"{LibraryConstants.CUSTOMER} ({customer.Id} - {customer.Name}) " +
                    $"{LibraryConstants.MESSAGE_SUCCESS}");
            result = true;

            return result;
        }

        public Customer UpdateCustomer(Customer customer)
        {
            if (_unitOfWork.CustomerRepository.Get(c => c.Id.Equals(customer.Id)) == null)
            {
                throw new LibraryException($"{LibraryConstants.CUSTOMER} - {customer.Id} {LibraryConstants.ERROR_NOT_FOUND}");
            }
            //Update book
            _unitOfWork.CustomerRepository.Update(customer, c => c.Id.Equals(customer.Id));
            ServiceUtils.ShowMessage(new List<string>()
            {
                LibraryConstants.MESSAGE_UPDATE,
                LibraryConstants.CUSTOMER.ToLower(),
                LibraryConstants.MESSAGE_SUCCESS
            });
            return _unitOfWork.CustomerRepository.Get(c => c.Id.Equals(customer.Id));
        }
        #endregion
    }
}
