﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using LibraryManagement.Utils;

namespace LibraryManagement.Services.Implements
{
    public class ReviewService : IReviewService
    {
        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        #endregion

        public Book CreateReview(Review review, Book book)
        {
            //Check if review is existed
            if (_unitOfWork.ReviewRepository.Get(r => r.Id.Equals(review.Id)) != null)
            {
                throw new LibraryException($"{LibraryConstants.REVIEW} - {review.Id} {LibraryConstants.ERROR_MESSAGE_IS_EXISTED}");
            }

            //Check if book is existed
            if (_unitOfWork.BookRepository.Get(b => b.Id.Equals(book.Id)) == null ||
                _unitOfWork.BookRepository.Get(b => b.Id.Equals(review.BookId)) == null)
            {
                throw new LibraryException($"{LibraryConstants.BOOK} - {book.Id} {LibraryConstants.ERROR_NOT_FOUND}");
            }

            //If customerId has value, then check if customer is existed or not
            if (review.CustomerId.HasValue && _unitOfWork.CustomerRepository.Get(c => c.Id.Equals(review.CustomerId)) == null)
            {
                throw new LibraryException($"{LibraryConstants.CUSTOMER} - {review.CustomerId} {LibraryConstants.ERROR_NOT_FOUND}");
            }

            //Review
            _unitOfWork.ReviewRepository.Create(review);

            //Book
            book.AddReview(review);
            _unitOfWork.BookRepository.Update(book, b => b.Id.Equals(book.Id));

            ServiceUtils.ShowMessage(new List<string>()
            {
                LibraryConstants.MESSAGE_ADD,
                LibraryConstants.REVIEW,
                review.Id.ToString(),
                LibraryConstants.MESSAGE_INTO,
                LibraryConstants.BOOK,
                book.Id.ToString(),
                LibraryConstants.MESSAGE_SUCCESS
            });
            return book;
        }
    }
}
