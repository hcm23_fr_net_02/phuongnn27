﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using LibraryManagement.Utils;
using System.Linq.Expressions;
using static LibraryManagement.Commons.LibraryEnum;

namespace LibraryManagement.Services.Implements
{
    public class BookService : IBookService
    {
        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public BookService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Book
        public bool AddBook(Book book)
        {
            if (_unitOfWork.BookRepository.Get(b => b.Id.Equals(book.Id)) != null)
            {
                throw new LibraryException($"{LibraryConstants.BOOK} - {book.Id} {LibraryConstants.ERROR_MESSAGE_IS_EXISTED}");
            }
            //Book
            _unitOfWork.BookRepository.Create(book);

            ServiceUtils.ShowMessage(new List<string>()
            {
                LibraryConstants.MESSAGE_ADD,
                LibraryConstants.BOOK.ToLower(),
                LibraryConstants.MESSAGE_SUCCESS
            });
            return true;
        }

        public Book UpdateBook(Book book)
        {
            if (_unitOfWork.BookRepository.Get(b => b.Id.Equals(book.Id)) == null)
            {
                throw new LibraryException($"{LibraryConstants.BOOK} - {book.Id} {LibraryConstants.ERROR_NOT_FOUND}");
            }
            //Update book
            _unitOfWork.BookRepository.Update(book, b => b.Id.Equals(book.Id));
            ServiceUtils.ShowMessage(new List<string>()
            {
                LibraryConstants.MESSAGE_UPDATE,
                LibraryConstants.BOOK.ToLower(),
                LibraryConstants.MESSAGE_SUCCESS
            });

            return _unitOfWork.BookRepository.Get(b => b.Id.Equals(book.Id));
        }

        public bool DeleteBook(Book book)
        {
            var result = false;
            try
            {
                if (_unitOfWork.BookRepository.Get(b => b.Id.Equals(book.Id)) == null)
                {
                    throw new LibraryException($"{LibraryConstants.BOOK} - {book.Id} {LibraryConstants.ERROR_NOT_FOUND}");
                }

                //Check if book is existed in review(s) or loan(s)
                var check = _unitOfWork.ReviewRepository.GetEntities(r => r.Book.Id.Equals(book.Id)).Any() ||
                    _unitOfWork.LoanRepository.GetEntities(l => l.Book.Id.Equals(book.Id)).Any();

                if (check)
                {
                    throw new LibraryException($"{LibraryConstants.MESSAGE_DELETE} " +
                        $"{LibraryConstants.BOOK} ({book.Id} - {book.Title}) " +
                        $"{LibraryConstants.MESSAGE_FAIL} {LibraryConstants.ERROR_RELATED_INFO}");
                }

                _unitOfWork.BookRepository.Delete(book);

                ServiceUtils.ShowMessage($"{LibraryConstants.MESSAGE_DELETE} " +
                        $"{LibraryConstants.BOOK.ToLower()} ({book.Id} - {book.Title}) " +
                        $"{LibraryConstants.MESSAGE_SUCCESS}");
                result = true;
            }
            catch (LibraryException ex)
            {
                ServiceUtils.ShowMessage(ex.Message);
            }
            return result;
        }

        public List<Book> FilterBooks(Sorting sorting) => GetSortBook(sorting);

        public Book SearchBook()
        {
            var book = new Book();
            var flag = true;
            do
            {
                try
                {
                    //Get book id
                    var id = ServiceUtils.GetIntegerInput(LibraryConstants.MESSAGE_BOOK_ID, 1);

                    //Get book by id
                    book = _unitOfWork.BookRepository.Get(b => b.Id.Equals(id));

                    //If book is null
                    if (book == null)
                    {
                        throw new LibraryException($"{LibraryConstants.BOOK_ID} {id} {LibraryConstants.ERROR_NOT_FOUND}");
                    }

                    flag = false;
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                }
            } while (flag); //End of searching book

            return book;
        }
        #endregion

        #region Utils

        private List<Book> GetSortBook(Sorting sorting)
        {
            var books = _unitOfWork.BookRepository.Get().AsQueryable();
            Expression<Func<Book, string>> sortBy = sorting.PropertyName switch
            {
                nameof(Book.Title) => book => book.Title,
                nameof(Book.Author) => book => book.Author,
                nameof(Book.PublicationYear) => book => book.PublicationYear.ToString(),
                nameof(Book.Genres) => book => book.Genres.Count().ToString(),
                _ => throw new LibraryException(LibraryConstants.ERROR_MESSAGE_SORT_BOOK)
            };

            return sorting.Order.Equals(BookSortOrder.Ascending.ToEnumMemberAttrValue()) ?
                books.OrderBy(sortBy).ToList() : books.OrderByDescending(sortBy).ToList();
        }
        #endregion
    }
}
