﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Presentations.Interfaces;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Utils;

namespace LibraryManagement.Presentations.Implements
{
    public class LoanPresentation : ILoanPresentation
    {
        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public LoanPresentation(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        #endregion

        #region Methods
        public Loan GetLoanData(Book book, Customer customer)
        {
            var borrowedDate = DateTime.Now;
            ServiceUtils.ShowMessage($"{LibraryConstants.LOAN_BORROWED_DATE}{LibraryConstants.MESSAGE_INPUT}" +
                $"{borrowedDate.ToString(LibraryConstants.MESSAGE_DATE_FORMAT_VALUE)}");

            var dueDate = ServiceUtils.GetDateTimeInput($"{LibraryConstants.LOAN_DUE_DATE} {LibraryConstants.MESSAGE_DATE_FORMAT}{LibraryConstants.MESSAGE_INPUT}", borrowedDate);

            var id = _unitOfWork.LoanRepository.IsEmpty() ? 1 : _unitOfWork.LoanRepository.Get().Max(c => c.Id) + 1;

            if (book.Quantity != 1)
            {
                book.Quantity = 1;
            }

            return new Loan(id, (int)customer.Id!, borrowedDate, dueDate, book, customer.GetCustomerValue());
        }

        public List<Customer>? GetLoansFromSearchKeys()
        {
            List<Customer>? customers = null;
            int input = 0;

            do
            {
                input = ServiceUtils.GetIntegerInput(LibraryConstants.MESSAGE_SEARCH_LOAN_INFO, 0, 2);

                if (input != 0)
                {
                    customers = input == 1 ? GetLoansByCustomerName() : GetLoansByBookTitle();
                    input = 0;
                }

            } while (input != 0);

            return customers;
        }

        public Loan SearchLoanById(List<Loan> loans, int? status = null)
        {
            var flag = true;
            var loan = new Loan();

            while (flag)
            {
                try
                {
                    var id = ServiceUtils.GetIntegerInput($"{LibraryConstants.LOAN} {LibraryConstants.LOAN_ID.ToLower()}{LibraryConstants.MESSAGE_INPUT}", 1);

                    loan = loans.SingleOrDefault(l => l.Id.Equals(id));

                    if (loan == null)
                    {
                        throw new LibraryException($"{LibraryConstants.LOAN} {LibraryConstants.ERROR_NOT_FOUND}");
                    }

                    if (!loan.Status.Equals(status) && status.HasValue)
                    {
                        throw new LibraryException(LibraryConstants.ERROR_LOAN_INVALID_CHECK_OUT_STATUS);
                    }

                    flag = false;

                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                }
            }

            return loan!;
        }

        public void ShowLoan(int id)
        {
            var loan = _unitOfWork.LoanRepository.Get(l => l.Id.Equals(id));

            if (loan != null)
            {
                string message = $"{LibraryConstants.MESSAGE_LINE_2}\n";
                message += loan.ToString();
                message += LibraryConstants.MESSAGE_LINE_2;

                ServiceUtils.ShowMessage(message);
            }
        }
        #endregion

        #region Utils

        private List<Customer>? GetLoansByCustomerName()
        {
            string name = ServiceUtils.GetInputNotNull($"{LibraryConstants.CUSTOMER} {LibraryConstants.CUSTOMER_NAME.ToLower()}{LibraryConstants.MESSAGE_INPUT}").Trim().ToLower();

            var customers = _unitOfWork.CustomerRepository.GetEntities(c => c.Name.ToLower().Contains(name));

            return customers;
        }

        private List<Customer>? GetLoansByBookTitle()
        {
            string title = ServiceUtils.GetInputNotNull($"{LibraryConstants.BOOK} {LibraryConstants.BOOK_TITLE.ToLower()}{LibraryConstants.MESSAGE_INPUT}").Trim().ToLower();

            var customers = _unitOfWork.CustomerRepository.GetEntities(c =>
            c.Loans.Any(l =>
            l.Book.Title.ToLower().Contains(title)));

            return customers;
        }

        #endregion
    }
}
