﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Presentations.Interfaces;
using LibraryManagement.Repositories.Implements;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Utils;

namespace LibraryManagement.Presentations.Implements
{
    public class ReviewPresentation : IReviewPresentation
    {

        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public ReviewPresentation(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        #endregion

        #region Method
        public Review GetReviewData(Book book, Customer? customer)
        {
            int id = _unitOfWork.ReviewRepository.IsEmpty() ? 1 : _unitOfWork.ReviewRepository.Get().Count() + 1;
            string title = ServiceUtils.GetInputNotNull(LibraryConstants.REVIEW_TITLE + LibraryConstants.MESSAGE_INPUT);
            string content = ServiceUtils.GetInputNotNull(LibraryConstants.REVIEW_CONTENT + LibraryConstants.MESSAGE_INPUT);
            int rate = ServiceUtils.GetIntegerInput($"{LibraryConstants.REVIEW_RATE}{LibraryConstants.REVIEW_RATE_RANGE}{LibraryConstants.MESSAGE_INPUT}", 1, 5);

            return new Review(id, book.Id, title, content, rate, DateTime.Now, book, 
                customer == null ? null : customer.Id, customer);
        }

        public void ShowReviews(Book book)
        {
            string message = string.Empty;

            if (!book.Reviews.Any())
                throw new LibraryException($"{LibraryConstants.REVIEW} {LibraryConstants.ERROR_MESSAGE_IS_EMPTY}");

            book.Reviews.ForEach(r => message += $"{r.Show()}");

            ServiceUtils.ShowMessage(message);
        }
        #endregion
    }
}
