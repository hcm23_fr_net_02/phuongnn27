﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Presentations.Interfaces;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Utils;

namespace LibraryManagement.Presentations.Implements
{
    public class BookPresentation : IBookPresentation
    {
        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public BookPresentation(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        public Book GetNewBookData()
        {
            //Declare variables
            int id = _unitOfWork.BookRepository.IsEmpty() ? 1 : _unitOfWork.BookRepository.Get().Count() + 1;
            string title = ServiceUtils.GetInputNotNull(LibraryConstants.BOOK_TITLE + LibraryConstants.MESSAGE_INPUT);
            string author = ServiceUtils.GetInputNotNull(LibraryConstants.BOOK_AUTHOR + LibraryConstants.MESSAGE_INPUT);
            int publicationYear = ServiceUtils.GetIntegerInput(LibraryConstants.BOOK_PUBLICATION_YEAR + LibraryConstants.MESSAGE_INPUT);
            int quantity = ServiceUtils.GetIntegerInput(LibraryConstants.BOOK_QUANTITY + LibraryConstants.MESSAGE_INPUT, 1);
            string description = ServiceUtils.GetInputNotNull(LibraryConstants.BOOK_DESCRIPTION_OPTIONAL + LibraryConstants.MESSAGE_INPUT);
            var genres = GetGenres();

            var book = new Book(id, title, author, publicationYear, quantity, description, genres);
            return book;
        }

        public Book GetUpdateBookData(Book book)
        {
            //Title
            string title = ServiceUtils.GetUserInput($"{LibraryConstants.BOOK_TITLE} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}")!.Trim();
            book.Title = string.IsNullOrEmpty(title) ? book.Title : title;

            //Author
            string author = ServiceUtils.GetUserInput($"{LibraryConstants.BOOK_AUTHOR} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}")!.Trim();
            book.Author = string.IsNullOrEmpty(author) ? book.Author : author;

            //Publication Year
            int publicationYear = ServiceUtils.GetIntegerNullInput($"{LibraryConstants.BOOK_PUBLICATION_YEAR} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}", 1, DateTime.Now.Year);
            book.PublicationYear = publicationYear == -1 ? book.PublicationYear : publicationYear;

            //Quantity
            int quantity = ServiceUtils.GetIntegerNullInput($"{LibraryConstants.BOOK_QUANTITY} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}", -1);
            book.Quantity = quantity == -1 ? book.Quantity : quantity;

            //Descriptionm
            string description = ServiceUtils.GetUserInput($"{LibraryConstants.BOOK_DESCRIPTION} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}")!.Trim();
            book.Description = string.IsNullOrEmpty(description) ? book.Description : description;

            //Genre
            var genres = GetGenres();
            book.Genres = genres ?? book.Genres;

            return book;
        }

        public void ShowFilterBooks(List<Book> books)
        {
            string message = $"{LibraryConstants.MESSAGE_LINE_1}\n";
            books.ForEach(b => message += $"{b.ToString()}\n");
            message += LibraryConstants.MESSAGE_LINE_1;

            ServiceUtils.ShowMessage(message);
        }
        #endregion

        #region Utils
        private List<string>? GetGenres()
        {
            var genres = new List<String>();

            var flag = true;
            while (flag)
            {
                try
                {
                    string genre = ServiceUtils.GetUserInput($"{LibraryConstants.BOOK_GENRE} {LibraryConstants.MESSAGE_INFO_STOP}{LibraryConstants.MESSAGE_INPUT}")!.Trim();

                    if (string.IsNullOrEmpty(genre))
                    {
                        throw new LibraryException(LibraryConstants.ERROR_MESSAGE_EMPTY_VALUE);
                    }

                    if (genres.Contains(genre))
                    {
                        throw new LibraryException($"{LibraryConstants.ERROR_DUPLICATED} {LibraryConstants.BOOK_GENRE.ToLower()}");
                    }

                    if (genre.Equals("0"))
                    {
                        flag = false;
                    }
                    else
                    {
                        genres.Add(genre);
                    }
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                }
            }
            return genres.Any() ? genres : null;
        }
        #endregion
    }
}
