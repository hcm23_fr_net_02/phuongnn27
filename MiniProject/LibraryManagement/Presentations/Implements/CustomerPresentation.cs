﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Presentations.Interfaces;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Utils;

namespace LibraryManagement.Presentations.Implements
{
    public class CustomerPresentation : ICustomerPresentation
    {
        #region Field and constructor
        private readonly IUnitOfWork _unitOfWork;

        public CustomerPresentation(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        #endregion

        #region Methods
        public Customer GetNewCustomerData()
        {
            //Declare variables
            int id = _unitOfWork.CustomerRepository.IsEmpty() ? 1 : _unitOfWork.CustomerRepository.Get().Count() + 1;
            string name = ServiceUtils.GetInputNotNull(LibraryConstants.CUSTOMER_NAME + LibraryConstants.MESSAGE_INPUT);
            string address = ServiceUtils.GetInputNotNull(LibraryConstants.CUSTOMER_ADDRESS + LibraryConstants.MESSAGE_INPUT);

            return new Customer(id, name, address);
        }

        public Customer GetUpdateCustomerData(Customer customer)
        {
            ServiceUtils.ShowMessage(customer.Show());

            //Name
            string name = ServiceUtils.GetUserInput($"{LibraryConstants.CUSTOMER_NAME} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}")!.Trim();
            customer.Name = string.IsNullOrEmpty(name) ? customer.Name : name;

            //Address
            string address = ServiceUtils.GetUserInput($"{LibraryConstants.CUSTOMER_ADDRESS} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}")!.Trim();
            customer.Address = string.IsNullOrEmpty(address) ? customer.Address : address;
            return customer;
        }

        public Customer SearchCustomer()
        {
            var customer = new Customer();
            var flag = true;
            do
            {
                try
                {
                    //Get customer id
                    var id = ServiceUtils.GetIntegerInput(LibraryConstants.MESSAGE_CUSTOMER_ID, 1);

                    //Get book by id
                    customer = _unitOfWork.CustomerRepository.Get(c => c.Id.Equals(id));

                    //If book is null
                    if (customer == null)
                    {
                        throw new LibraryException($"{LibraryConstants.CUSTOMER_ID} {id} {LibraryConstants.ERROR_NOT_FOUND}");
                    }

                    flag = false;
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                }

            } while (flag); //End of searching book

            return customer;
        }

        public void ShowCustomerList(ApplicationModel application)
        {
            string choice = string.Empty;
            int page = LibraryConstants.PAGE_DEFAULT_PAGE;
            int size = LibraryConstants.PAGE_DEFAULT_SIZE;
            var model = ServiceUtils.Paging(_unitOfWork.CustomerRepository.Get(), page, size);

            do
            {
                try
                {
                    ServiceUtils.ClearImmediateScreen();

                    application.Data = model.ToString();

                    ServiceUtils.ShowMessage(application.ToString());

                    choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

                    switch (choice)
                    {
                        case LibraryConstants.APPLICATION_PAGE_SET_SIZE:
                            size = ServiceUtils.GetSize();
                            model = ServiceUtils.Paging(_unitOfWork.CustomerRepository.Get(), LibraryConstants.PAGE_DEFAULT_PAGE, size);
                            break;
                        case LibraryConstants.APPLICATION_PAGE_NEXT:
                            if (model.IsLastPage)
                                throw new LibraryException(LibraryConstants.ERROR_PAGE_LAST_PAGE);
                            page += 1;
                            model = ServiceUtils.Paging(_unitOfWork.CustomerRepository.Get(), page, size);
                            break;
                        case LibraryConstants.APPLICATION_PAGE_PREVIOUS:
                            if (model.IsFirstPage)
                                throw new LibraryException(LibraryConstants.ERROR_PAGE_FIRST_PAGE);
                            page -= 1;
                            model = ServiceUtils.Paging(_unitOfWork.CustomerRepository.Get(), page, size);
                            break;
                        default:
                            if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                            {
                                throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                            }
                            break;
                    }
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                    ServiceUtils.ClearScreen();
                }
            } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));

            ServiceUtils.ClearScreen();
        }
        #endregion
    }
}
