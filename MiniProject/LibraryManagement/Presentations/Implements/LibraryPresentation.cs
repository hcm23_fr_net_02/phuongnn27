﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Presentations.Interfaces;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using LibraryManagement.Utils;
using static LibraryManagement.Commons.LibraryEnum;

namespace LibraryManagement.Presentations.Implements
{
    public class LibraryPresentation : ILibraryPresentation
    {
        #region Field and constructor
        public bool IsFullData { get; private set; }
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBookService _bookService;
        private readonly IReviewService _reviewService;
        private readonly ILoanService _loanService;
        private readonly IBookPresentation _bookPresentation;
        private readonly IReviewPresentation _reviewPresentation;
        private readonly ILoanPresentation _loanPresentation;
        private readonly ICustomerPresentation _customerPresentation;
        private readonly ICustomerService _customerService;
        private List<ApplicationModel>? _applications;
        public ApplicationModel? Application => GetValidLibraryManagementOption();

        public LibraryPresentation(IUnitOfWork unitOfWork, IBookService bookService,
            ICustomerService customerService, IReviewService reviewService,
            ILoanService loanService, ILoanPresentation loanPresentation,
            IBookPresentation bookPresentation,
            IReviewPresentation reviewPresentation,
            ICustomerPresentation customerPresentation)
        {
            _unitOfWork = unitOfWork;
            _bookService = bookService;
            _reviewService = reviewService;
            _customerService = customerService;
            _loanService = loanService;
            _reviewPresentation = reviewPresentation;
            _bookPresentation = bookPresentation;
            _reviewPresentation = reviewPresentation;
            _loanPresentation = loanPresentation;
            _customerPresentation = customerPresentation;
            IsFullData = Seed();
            if (IsFullData)
            {
                _applications = LoadApplicationModel();
            }
        }
        #endregion

        #region Methods

        #region Book
        public void ShowBookManagement()
        {
            string choice = string.Empty;
            int page = LibraryConstants.PAGE_DEFAULT_PAGE;
            int size = LibraryConstants.PAGE_DEFAULT_SIZE;
            var model = ServiceUtils.Paging(_unitOfWork.BookRepository.Get(), page, size);
            var application = _applications![1];

            do
            {
                try
                {
                    ServiceUtils.ClearImmediateScreen();

                    application.Data = model.ToString();

                    ServiceUtils.ShowMessage(application.ToString());

                    choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

                    switch (choice)
                    {
                        //Set size
                        case LibraryConstants.APPLICATION_PAGE_SET_SIZE:
                            size = ServiceUtils.GetSize();
                            model = ServiceUtils.Paging(_unitOfWork.BookRepository.Get(), LibraryConstants.PAGE_DEFAULT_PAGE, size);
                            break;
                        //Next page
                        case LibraryConstants.APPLICATION_PAGE_NEXT:
                            if (model.IsLastPage)
                            {
                                throw new LibraryException(LibraryConstants.ERROR_PAGE_LAST_PAGE);
                            }
                            page += 1;
                            model = ServiceUtils.Paging(_unitOfWork.BookRepository.Get(), page, size);
                            break;
                        //Previous page
                        case LibraryConstants.APPLICATION_PAGE_PREVIOUS:
                            if (model.IsFirstPage)
                            {
                                throw new LibraryException(LibraryConstants.ERROR_PAGE_FIRST_PAGE);
                            }
                            page -= 1;
                            model = ServiceUtils.Paging(_unitOfWork.BookRepository.Get(), page, size);
                            break;
                        //Show book detail
                        case LibraryConstants.APPLICATION_OPTION_4:
                            ServiceUtils.ClearImmediateScreen();
                            ShowBookDetail();
                            page = LibraryConstants.PAGE_DEFAULT_PAGE;
                            model = ServiceUtils.Paging(_unitOfWork.BookRepository.Get(), page, size);
                            break;
                        default:
                            if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                            {
                                throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                            }
                            break;
                    }
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                    ServiceUtils.ClearScreen();
                }
            } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));

            ServiceUtils.ClearScreen();
        }

        private void ShowBookDetail()
        {
            var book = _bookService.SearchBook();
            var application = _applications![2];

            application.Data = book.ToString();
            string choice = string.Empty;

            do
            {
                try
                {
                    ServiceUtils.ShowMessage(application.ToString());

                    choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

                    switch (choice)
                    {
                        //Update book
                        case LibraryConstants.APPLICATION_OPTION_1:
                            book = _bookPresentation.GetUpdateBookData(book);
                            book = _bookService.UpdateBook(book);
                            application.Data = book.ToString();
                            break;
                        //Delete book
                        case LibraryConstants.APPLICATION_OPTION_2:
                            choice = _bookService.DeleteBook(book) ? LibraryConstants.APPLICATION_LAST_OPTION :
                                choice;
                            break;
                        //Add review
                        case LibraryConstants.APPLICATION_OPTION_3:
                            var customer = _customerPresentation.SearchCustomer();
                            var review = _reviewPresentation.GetReviewData(book, customer);
                            book = _reviewService.CreateReview(review, book);
                            application.Data = book.ToString();
                            break;
                        //Show reviews
                        case LibraryConstants.APPLICATION_OPTION_4:
                            _reviewPresentation.ShowReviews(book);
                            break;
                        //Create loan
                        case LibraryConstants.APPLICATION_OPTION_5:
                            customer = _customerPresentation.SearchCustomer();
                            var loan = _loanPresentation.GetLoanData(book.GetBookValue(), customer);
                            if (_loanService.CreateLoan(loan, customer))
                            {
                                _loanPresentation.ShowLoan(loan.Id);
                            }
                            book = _unitOfWork.BookRepository.Get(b => b.Id.Equals(book.Id));
                            application.Data = book.ToString();
                            break;
                        default:
                            if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                            {
                                throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                            }
                            break;
                    }
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                }

                ServiceUtils.ClearScreen();

            } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));

        }

        public void AddBook()
        {
            ServiceUtils.ClearImmediateScreen();
            var book = _bookPresentation.GetNewBookData();
            _bookService.AddBook(book);
            ServiceUtils.ClearScreen();
        }

        public void FilterBooks()
        {
            ServiceUtils.ClearImmediateScreen();

            var application = _applications![3];
            string choice = string.Empty;
            Sorting sorting;
            var propertyName = string.Empty;

            do
            {
                try
                {
                    ServiceUtils.ShowMessage(application.ToString());

                    choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

                    switch (choice)
                    {
                        case LibraryConstants.APPLICATION_OPTION_1:
                            propertyName = nameof(Book.Title);
                            break;
                        case LibraryConstants.APPLICATION_OPTION_2:
                            propertyName = nameof(Book.Author);
                            break;
                        case LibraryConstants.APPLICATION_OPTION_3:
                            propertyName = nameof(Book.PublicationYear);
                            break;
                        case LibraryConstants.APPLICATION_OPTION_4:
                            propertyName = nameof(Book.Genres);
                            break;
                        default:
                            if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                            {
                                throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                            }
                            propertyName = string.Empty;
                            break;
                    }

                    if (!string.IsNullOrEmpty(propertyName))
                    {
                        sorting = new Sorting(propertyName, GetOrderForSort());
                        var books = _bookService.FilterBooks(sorting);
                        _bookPresentation.ShowFilterBooks(books);
                    }
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                }

                ServiceUtils.ClearScreen();

            } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));
        }
        #endregion

        #region Loan
        public void ShowLoanManagement()
        {
            string choice = string.Empty;
            int page = LibraryConstants.PAGE_DEFAULT_PAGE;
            int size = LibraryConstants.PAGE_DEFAULT_SIZE;
            var model = ServiceUtils.Paging(_unitOfWork.LoanRepository.Get(), page, size);
            var application = _applications![4];

            do
            {
                try
                {
                    ServiceUtils.ClearImmediateScreen();

                    application.Data = model.ToString();

                    ServiceUtils.ShowMessage(application.ToString());

                    choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

                    switch (choice)
                    {
                        case LibraryConstants.APPLICATION_PAGE_SET_SIZE:
                            size = ServiceUtils.GetSize();
                            model = ServiceUtils.Paging(_unitOfWork.LoanRepository.Get(), LibraryConstants.PAGE_DEFAULT_PAGE, size);
                            break;
                        case LibraryConstants.APPLICATION_PAGE_NEXT:
                            if (model.IsLastPage)
                                throw new LibraryException(LibraryConstants.ERROR_PAGE_LAST_PAGE);
                            page += 1;
                            model = ServiceUtils.Paging(_unitOfWork.LoanRepository.Get(), page, size);
                            break;
                        case LibraryConstants.APPLICATION_PAGE_PREVIOUS:
                            if (model.IsFirstPage)
                                throw new LibraryException(LibraryConstants.ERROR_PAGE_FIRST_PAGE);
                            page -= 1;
                            model = ServiceUtils.Paging(_unitOfWork.LoanRepository.Get(), page, size);
                            break;
                        case LibraryConstants.APPLICATION_OPTION_4:
                            ServiceUtils.ClearImmediateScreen();
                            CheckOutLoanDetail();
                            page = LibraryConstants.PAGE_DEFAULT_PAGE;
                            model = ServiceUtils.Paging(_unitOfWork.LoanRepository.Get(), page, size);
                            break;
                        default:
                            if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                            {
                                throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                            }
                            break;
                    }
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                    ServiceUtils.ClearScreen();
                }

            } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));

            ServiceUtils.ClearScreen();
        }

        private void CheckOutLoanDetail()
        {
            try
            {
                var customers = _loanPresentation.GetLoansFromSearchKeys();

                if (customers == null || !customers.Any())
                {
                    throw new LibraryException($"{LibraryConstants.LOAN} {LibraryConstants.ERROR_NOT_FOUND}");
                }

                var loans = customers.SelectMany(c => c.Loans).ToList();

                GetLoanById(loans);
            }
            catch (LibraryException ex)
            {
                ServiceUtils.ShowMessage(ex.Message);
                ServiceUtils.ClearScreen();
            }
        }

        private void GetLoanById(List<Loan> loans)
        {
            var page = LibraryConstants.PAGE_DEFAULT_PAGE;
            var size = LibraryConstants.PAGE_DEFAULT_SIZE;
            var model = ServiceUtils.Paging(loans, page, size);
            string choice = string.Empty;
            var application = _applications![5];

            do
            {
                if (loans.Any())
                {
                    try
                    {
                        ServiceUtils.ClearImmediateScreen();

                        application.Data = model.ToString();

                        ServiceUtils.ShowMessage(application.ToString());

                        choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

                        switch (choice)
                        {
                            case LibraryConstants.APPLICATION_PAGE_SET_SIZE:
                                size = ServiceUtils.GetSize();
                                model = ServiceUtils.Paging(loans, LibraryConstants.PAGE_DEFAULT_PAGE, size);
                                break;
                            case LibraryConstants.APPLICATION_PAGE_NEXT:
                                if (model.IsLastPage)
                                    throw new LibraryException(LibraryConstants.ERROR_PAGE_LAST_PAGE);
                                page += 1;
                                model = ServiceUtils.Paging(loans, page, size);
                                break;
                            case LibraryConstants.APPLICATION_PAGE_PREVIOUS:
                                if (model.IsFirstPage)
                                    throw new LibraryException(LibraryConstants.ERROR_PAGE_FIRST_PAGE);
                                page -= 1;
                                model = ServiceUtils.Paging(loans, page, size);
                                break;
                            //Check out
                            case LibraryConstants.APPLICATION_OPTION_4:
                                var loan = _loanPresentation.SearchLoanById(loans, (int)LoanStatus.Borrowing);
                                loans = _loanService.CheckOutLoan(loan, loans);
                                page = LibraryConstants.PAGE_DEFAULT_PAGE;
                                model = ServiceUtils.Paging(loans, page, size);
                                ServiceUtils.ClearScreen();
                                break;
                            default:
                                if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                                {
                                    throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                                }
                                break;
                        }
                    }
                    catch (LibraryException ex)
                    {
                        ServiceUtils.ShowMessage(ex.Message);
                        ServiceUtils.ClearScreen();
                    }
                }
                else
                {
                    choice = LibraryConstants.APPLICATION_LAST_OPTION;
                }

            } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));
            ServiceUtils.ClearScreen();
        }
        #endregion

        #region Customer
        public void ShowCustomerManagement()
        {
            string choice = string.Empty;
            var application = _applications![6];

            do
            {
                ServiceUtils.ClearImmediateScreen();
                try
                {
                    ServiceUtils.ShowMessage(application.ToString());

                    choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

                    switch (choice)
                    {
                        //Add customer
                        case LibraryConstants.APPLICATION_OPTION_1:
                            ServiceUtils.ClearImmediateScreen();
                            var customer = _customerPresentation.GetNewCustomerData();
                            _customerService.AddCustomers(customer);
                            break;
                        //Show customers
                        case LibraryConstants.APPLICATION_OPTION_2:
                            ServiceUtils.ClearImmediateScreen();
                            _customerPresentation.ShowCustomerList(_applications.Last());
                            break;
                        //Update customer
                        case LibraryConstants.APPLICATION_OPTION_3:
                            ServiceUtils.ClearImmediateScreen();
                            customer = _customerPresentation.SearchCustomer();
                            customer = _customerPresentation.GetUpdateCustomerData(customer);
                            _customerService.UpdateCustomer(customer);
                            break;
                        //Delete customer
                        case LibraryConstants.APPLICATION_OPTION_4:
                            ServiceUtils.ClearImmediateScreen();
                            customer = _customerPresentation.SearchCustomer();
                            _customerService.DeleteCustomer(customer);
                            break;
                        default:
                            if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                            {
                                throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                            }
                            break;
                    }
                }
                catch (LibraryException ex)
                {
                    ServiceUtils.ShowMessage(ex.Message);
                }

                if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                {
                    ServiceUtils.ClearScreen();
                }

            } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));

            ServiceUtils.ClearScreen();
        }
        #endregion

        #endregion

        #region Utils
        private bool Seed()
        {
            var result = true;
            try
            {
                //book
                _unitOfWork.BookRepository.GetData(
                    $"{LibraryConstants.s_rootPath}{LibraryConstants.BOOK.ToLower()}{LibraryConstants.FILE_EXTENSION}",
                    $"{LibraryConstants.ERROR_MESSAGE_READ_DATA_FROM_JSON} {LibraryConstants.BOOK.ToLower()}");

                //loan
                _unitOfWork.LoanRepository.GetData(
                    $"{LibraryConstants.s_rootPath}{LibraryConstants.LOAN.ToLower()}{LibraryConstants.FILE_EXTENSION}",
                    $"{LibraryConstants.ERROR_MESSAGE_READ_DATA_FROM_JSON} {LibraryConstants.LOAN.ToLower()}");

                //customer
                _unitOfWork.CustomerRepository.GetData(
                    $"{LibraryConstants.s_rootPath}{LibraryConstants.CUSTOMER.ToLower()}{LibraryConstants.FILE_EXTENSION}",
                    $"{LibraryConstants.ERROR_MESSAGE_READ_DATA_FROM_JSON} {LibraryConstants.CUSTOMER.ToLower()}");

                //review
                _unitOfWork.ReviewRepository.GetData(
                    $"{LibraryConstants.s_rootPath}{LibraryConstants.REVIEW.ToLower()}{LibraryConstants.FILE_EXTENSION}",
                    $"{LibraryConstants.ERROR_MESSAGE_READ_DATA_FROM_JSON} {LibraryConstants.REVIEW.ToLower()}");
            }
            catch (LibraryException ex)
            {
                result = false;
                ServiceUtils.ShowMessage(ex.Message);
            }
            return result;
        }

        private List<ApplicationModel> LoadApplicationModel()
        => JSonUtils<ApplicationModel>.ReadJsonIntoData($"{LibraryConstants.s_rootPath}{LibraryConstants.FILE_APPLICATION.ToLower()}{LibraryConstants.FILE_EXTENSION}");

        private ApplicationModel GetValidLibraryManagementOption()
        {
            var application = _applications![0];
            var excludedCheckList = new List<string>() { LibraryConstants.APPLICATION_ADD_A_BOOK_KEY, LibraryConstants.APPLICATION_LAST_OPTION };
            foreach (var options in application.Options!)
            {
                if (!excludedCheckList.Contains(options.Key))
                {
                    options.Value.IsValid = options.Key.Equals(LibraryConstants.APPLICATION_CUSTOMER_MANAGEMENT_KEY) ?
                        !_unitOfWork.CustomerRepository.IsEmpty() : !_unitOfWork.BookRepository.IsEmpty();
                }
            }
            return application;
        }

        private string GetOrderForSort()
        {
            int result = ServiceUtils.GetIntegerInput(LibraryConstants.MESSAGE_BOOK_SORT_ORDER, 1, 2);
            return StatusExtension<BookSortOrder>.GetStatus(result);
        }
        #endregion
    }
}
