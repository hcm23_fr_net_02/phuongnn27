﻿using LibraryManagement.Entities;

namespace LibraryManagement.Presentations.Interfaces
{
    public interface IBookPresentation
    {
        /// <summary>
        /// Get data of a new book
        /// </summary>
        /// <returns></returns>
        Book GetNewBookData();
        /// <summary>
        /// Get data of an updated book
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        Book GetUpdateBookData(Book book);
        /// <summary>
        /// Filter book by book's fields (such as Title, Author, etc.)
        /// </summary>
        /// <param name="books"></param>
        void ShowFilterBooks(List<Book> books);
    }
}
