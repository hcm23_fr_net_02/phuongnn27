﻿using LibraryManagement.Commons;
using LibraryManagement.Entities;

namespace LibraryManagement.Presentations.Interfaces
{
    public interface ICustomerPresentation
    {
        Customer GetNewCustomerData();
        Customer GetUpdateCustomerData(Customer customer);
        Customer SearchCustomer();
        void ShowCustomerList(ApplicationModel application);
    }
}
