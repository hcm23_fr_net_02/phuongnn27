﻿using LibraryManagement.Commons;

namespace LibraryManagement.Presentations.Interfaces
{
    public interface ILibraryPresentation
    {
        bool IsFullData { get; }

        ApplicationModel? Application { get; }

        void ShowBookManagement();

        void AddBook();

        void FilterBooks();

        void ShowLoanManagement();

        void ShowCustomerManagement();

    }
}
