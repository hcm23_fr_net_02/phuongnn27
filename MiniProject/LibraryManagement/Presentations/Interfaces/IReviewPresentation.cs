﻿using LibraryManagement.Entities;

namespace LibraryManagement.Presentations.Interfaces
{
    public interface IReviewPresentation
    {
        Review GetReviewData(Book book, Customer? customer);
        void ShowReviews(Book book);
    }
}
