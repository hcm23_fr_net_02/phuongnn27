﻿using LibraryManagement.Entities;

namespace LibraryManagement.Presentations.Interfaces
{
    public interface ILoanPresentation
    {
        Loan GetLoanData(Book book, Customer customer);
        void ShowLoan(int id);
        List<Customer>? GetLoansFromSearchKeys();
        Loan SearchLoanById(List<Loan> loans, int? status = null);
    }
}
