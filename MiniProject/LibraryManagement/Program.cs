﻿using LibraryManagement.AppStart;
using LibraryManagement.Commons;
using LibraryManagement.Exceptions;
using LibraryManagement.Presentations.Interfaces;
using LibraryManagement.Utils;
using Microsoft.Extensions.DependencyInjection;

var service = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
var libraryPresentation = service.GetService<ILibraryPresentation>();
if (libraryPresentation!.IsFullData)
{
    string choice = string.Empty;

    do
    {
        var application = libraryPresentation.Application;

        try
        {
            ServiceUtils.ShowMessage(application!.ToString());
            choice = ServiceUtils.GetInputNotNull($"{LibraryConstants.MESSAGE_CHOICE}{LibraryConstants.MESSAGE_INPUT}").Trim().ToUpper();

            switch (choice)
            {
                case LibraryConstants.APPLICATION_OPTION_1:
                    libraryPresentation.ShowBookManagement();
                    break;
                case LibraryConstants.APPLICATION_OPTION_2:
                    libraryPresentation.AddBook();
                    break;
                case LibraryConstants.APPLICATION_OPTION_3:
                    libraryPresentation.FilterBooks();
                    break;
                case LibraryConstants.APPLICATION_OPTION_4:
                    libraryPresentation.ShowLoanManagement();
                    break;
                case LibraryConstants.APPLICATION_OPTION_5:
                    libraryPresentation.ShowCustomerManagement();
                    break;
                default:
                    if (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                    {
                        throw new LibraryException(LibraryConstants.ERROR_CHOICE_RANGE);
                    }
                    break;
            }
        }
        catch (LibraryException ex)
        {
            ServiceUtils.ShowMessage(ex.Message);
            ServiceUtils.ClearScreen();
        }

    } while (!choice.Equals(LibraryConstants.APPLICATION_LAST_OPTION));
    Console.WriteLine(LibraryConstants.MESSAGE_END);
}