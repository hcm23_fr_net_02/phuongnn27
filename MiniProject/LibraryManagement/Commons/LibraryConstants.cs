﻿namespace LibraryManagement.Commons
{
    public class LibraryConstants
    {
        #region File
        public const string FILE_EXTENSION = ".json";
        public const string FILE_APPLICATION = "application";
        public static string s_rootPath = Environment.CurrentDirectory.Split(@"bin\Debug\net6.0")[0] + @"Data\";
        #endregion

        #region Message
        public const string MESSAGE_ADD = "Add";
        public const string MESSAGE_UPDATE = "Update";
        public const string MESSAGE_DELETE = "Delete";
        public const string MESSAGE_SUCCESS = "successfully";
        public const string MESSAGE_FAIL = "failed";
        public const string MESSAGE_NO_EMPTY = "Cannot empty";
        public const string MESSAGE_INTO = "into";
        public const string MESSAGE_LINE_1 = "--------------------------";
        public const string MESSAGE_LINE_2 = "********************************************";
        public const string MESSAGE_END = "Good bye!";
        public const string MESSAGE_CLEAR = "Enter to clear console: ";
        public const string MESSAGE_DATE_FORMAT = "(yyyy-mm-dd)";
        public const string MESSAGE_DATE_FORMAT_VALUE = "yyyy-MM-dd";
        public static string MESSAGE_BOOK_ID = $"Enter {BOOK.ToLower()} {BOOK_ID}{MESSAGE_INPUT}";
        public static string MESSAGE_CUSTOMER_ID = $"Enter {CUSTOMER.ToLower()} {CUSTOMER_ID}{MESSAGE_INPUT}";
        public const string MESSAGE_INFO_STOP = "(Enter 0 to stop)";
        public const string MESSAGE_INFO_KEEP_VALUE = "(Enter to keep value)";
        public const string MESSAGE_INPUT = ": ";
        public const string MESSAGE_CHOICE = "Your choice";
        public const string MESSAGE_BOOK_SORT_ORDER = "Do you want to filter ascending (1) or descending (2)? ";
        public const string MESSAGE_LOAN_CHECK_OUT = "-------------------Check out result-------------------";
        #endregion

        #region Error
        public const string ERROR_DUPLICATED = "Duplicated";
        public const string ERROR_RELATED_INFO = "because it relates other info";
        public const string ERROR_NOT_FOUND = "not found";
        public const string ERROR_MESSAGE_IS_EMPTY = "is empty";
        public const string ERROR_MESSAGE_IS_EXISTED = "is existed";
        public const string ERROR_MESSAGE_LARGER_INTEGER = "Please enter number larger than";
        public const string ERROR_MESSAGE_SMALLER_INTEGER = "Please enter number smaller than";
        public const string ERROR_MESSAGE_EMPTY_VALUE = "Please type value";
        public const string EARLIER_THAN_OR_EQUAL_TO_DATE = "is earlier than or equal to";
        public const string EARLIER_THAN_DATE = "is earlier than";
        public const string ERROR_MESSAGE_READ_DATA_FROM_JSON = "Cannot read file into";
        public const string ERROR_MESSAGE_SORT_BOOK = "Book cannot sort";
        public const string ERROR_PAGE_NOT_IN_RANGE = "Page is not the range of total page";
        public const string ERROR_PAGE_LAST_PAGE = "Current page is last page";
        public const string ERROR_PAGE_FIRST_PAGE = "Current page is first page";
        public const string ERROR_CHOICE_RANGE = "Please choose as mentioned information";
        public const string ERROR_LOAN_INVALID_CHECK_OUT_STATUS = "Book is already returned";
        #endregion

        #region Book
        public const string BOOK = "Book";
        public const string BOOK_ID = "Id";
        public const string BOOK_TITLE = "Title";
        public const string BOOK_AUTHOR = "Author";
        public const string BOOK_PUBLICATION_YEAR = "Publication year";
        public const string BOOK_QUANTITY = "Quantity";
        public const string BOOK_DESCRIPTION = "Description";
        public const string BOOK_DESCRIPTION_OPTIONAL = "Description (Enter to skip)";
        public const string BOOK_GENRE = "Genre";
        #endregion

        #region Customer
        public const string CUSTOMER = "Customer";
        public const string CUSTOMER_ID = "Id";
        public const string CUSTOMER_NAME = "Name";
        public const string CUSTOMER_ADDRESS = "Address";
        #endregion

        #region Loan
        public const string LOAN = "Loan";
        public const string LOAN_ID = "Id";
        public const string LOAN_BORROWED_DATE = "Borrowed date";
        public const string LOAN_DUE_DATE = "Due date";
        public const string LOAN_RETURNED_DATE = "Returned date";
        public const string LOAN_STATUS = "Status";
        public const string MESSAGE_SEARCH_LOAN_INFO = "------------------ Search Loan -------------------" +
            "\n1. Search by customer's name" +
            "\n2. Search by book's title" +
            "\n0. Quit" +
            "\nYour choice: ";
        #endregion

        #region Review
        public const string REVIEW = "Review";
        public const string REVIEW_TITLE = "Title";
        public const string REVIEW_CONTENT = "Content";
        public const string REVIEW_RATE = "Rate";
        public const string REVIEW_RATE_RANGE = "(From 1 to 5)";
        public const string REVIEW_UNKNOWN_CUSTOMER = "Unknown";
        #endregion

        #region Application
        public const string APPLICATION_ADD_A_BOOK_KEY = "2";
        public const string APPLICATION_CUSTOMER_MANAGEMENT_KEY = "5";
        public const string APPLICATION_LAST_OPTION = "0";
        public const string APPLICATION_PAGE_SET_SIZE = "S";
        public const string APPLICATION_PAGE_NEXT = "N";
        public const string APPLICATION_PAGE_PREVIOUS = "P";
        public const string APPLICATION_OPTION_1 = "1";
        public const string APPLICATION_OPTION_2 = "2";
        public const string APPLICATION_OPTION_3 = "3";
        public const string APPLICATION_OPTION_4 = "4";
        public const string APPLICATION_OPTION_5 = "5";
        public const string APPLICATION_OPTION_6 = "6";
        #endregion

        #region Page
        public const string PAGE_TOTAL = "Total";
        public const string PAGE_SIZE = "Size";
        public static string s_pageSizeInfo = $"Size (default is {PAGE_DEFAULT_SIZE})";
        public const int PAGE_DEFAULT_SIZE = 3;
        public const int PAGE_DEFAULT_PAGE = 0;
        #endregion
    }
}
