﻿namespace LibraryManagement.Commons
{
    public class ApplicationModel
    {
        #region Fields
        private int _id;
        private string? _title;
        private string? _data;
        private Dictionary<string, OptionModel> _options;
        #endregion

        public ApplicationModel(int id, string title, string? data, Dictionary<string, OptionModel> options)
        {
            _id = id;
            _title = title;
            _data = data;
            _options = options;
        }

        #region Properties
        public int Id { get => _id; set => _id = value; }
        public string? Title { get => _title; set => _title = value; }
        public string? Data { get => _data; set => _data = value; }
        public Dictionary<string, OptionModel> Options { get => _options; set => _options = value; }
        #endregion

        #region Method
        public override string ToString()
        {
            string message = $"{_title}\n" + (!string.IsNullOrEmpty(_data) ? $"{_data}\n" : string.Empty);
            if (_options!.Any() && _options != null)
            {
                int index = 1;
                foreach (var option in _options)
                {
                    if (option.Key!.Equals(LibraryConstants.APPLICATION_LAST_OPTION))
                    {
                        message += $"{LibraryConstants.APPLICATION_LAST_OPTION}. {option.Value.Title}\n";
                    }
                    else
                    {
                        message += $"{index}. {option.Value.Title}\n";
                    }
                    index++;
                }
            }
            message += $"{LibraryConstants.MESSAGE_LINE_1}";
            return message;
        }
        #endregion
    }

    public class OptionModel
    {
        #region Fields
        private string? _title;
        private bool _isValid;
        #endregion

        public OptionModel(string title, bool isValid = true)
        {
            _title = title;
            _isValid = isValid;
        }

        #region Properties
        public string? Title { get => _title; set => _title = value; }
        public bool IsValid { get => _isValid; set => _isValid = value; }
        #endregion
    }
}
