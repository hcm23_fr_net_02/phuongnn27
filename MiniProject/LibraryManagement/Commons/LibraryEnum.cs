﻿using System.Runtime.Serialization;

namespace LibraryManagement.Commons
{
    public class LibraryEnum
    {
        public enum LoanStatus
        {
            [EnumMember(Value = "Borrowing")]
            Borrowing = 1,
            [EnumMember(Value = "Checked Out")]
            CheckedOut = 2,
        }
        public enum BookSortOrder
        {
            [EnumMember(Value = "asc")]
            Ascending = 1,
            [EnumMember(Value = "desc")]
            Descending = 2,
        }
    }
}
