﻿namespace LibraryManagement.Commons
{
    public static class StatusExtension<T> where T : Enum
    {
        private static string? GetStringStatus(int? index)
        {
            try
            {
                var list = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(t => ((int)Enum.Parse(typeof(T), t.ToString(), ignoreCase: true), t))
                .ToList();
                var result = list.SingleOrDefault(l => l.Item1.Equals((int)index!)).t.ToEnumMemberAttrValue();
                return result;
            }
            catch
            {
                return null;
            }
        }

        private static T GetEnumItemStatus(int? index)
        {
            try
            {
                var list = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(t => ((int)Enum.Parse(typeof(T), t.ToString(), ignoreCase: true), t))
                .ToList();
                return list.SingleOrDefault(l => l.Item1.Equals((int)index!)).t;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static string GetStatus(int? Status) => GetStringStatus(Status)!;
        public static T GetEnumStatus(int? Status) => GetEnumItemStatus(Status);
    }
}
