﻿namespace LibraryManagement.Commons
{
    public record Sorting(string PropertyName, string Order);
}
