﻿namespace LibraryManagement.Commons
{
    public class PagingModel<T>
    {
        public int Total { get; }
        public int Size { get; }
        public List<T> Entities { get; set; }
        public int Page { get; set; }
        public int TotalPage { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }

        public PagingModel(List<T> list, int page, int size, int totalPage, bool isFirstPage, bool isLastPage)
        {
            this.Entities = list;
            this.Page = page;
            this.Size = size;
            this.IsFirstPage = isFirstPage;
            this.IsLastPage = isLastPage;
            Total = Entities.Count();
            TotalPage = totalPage;
        }

        public override string ToString()
        {
            var tempPage = Page + 1;
            string message = "";

            message += $"Page: {tempPage}/{TotalPage} | {LibraryConstants.PAGE_TOTAL}{LibraryConstants.MESSAGE_INPUT} {Total}\n";

            Entities.ForEach(item =>
            {
                message += $"{LibraryConstants.MESSAGE_LINE_2}\n" +
                $"{item?.ToString()}\n" +
                $"{LibraryConstants.MESSAGE_LINE_2}";
            });

            return message;
        }

    }
}
