﻿using LibraryManagement.Commons;
using LibraryManagement.Exceptions;

namespace LibraryManagement.Utils
{
    public static class ServiceUtils
    {
        public static void ShowMessage(List<string> list)
        {
            string message = string.Empty;

            list.ForEach(m => message += $"{m} ");
            Console.WriteLine(message.Trim());
        }

        public static void ShowMessage(string message) => Console.WriteLine(message);

        public static string? GetUserInput(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        public static string GetInputNotNull(string message)
        {
            var flag = true;
            string input = string.Empty;
            do
            {
                input = GetUserInput(message)!;
                if (string.IsNullOrEmpty(input))
                {
                    ShowMessage(new List<string>() { LibraryConstants.MESSAGE_NO_EMPTY });
                }

                flag = false;

            } while (flag);

            return input!.Trim();
        }

        /// <summary>
        /// Get integer number from user input (<paramref name="message"/>, <paramref name="min"/>, <paramref name="max"/>)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="min">min number (nullable). If min is null, then default will be 0</param>
        /// <param name="max">max number (nullable). If max is null, then default will be max of integer</param>
        /// <returns></returns>
        public static int GetIntegerInput(string message, int? min = null, int? max = null)
        {
            var flag = true;
            int input = 0;

            if (!min.HasValue)
                min = 0;

            if (!max.HasValue)
                max = int.MaxValue;

            do
            {
                try
                {
                    input = Convert.ToInt32(GetUserInput(message));
                    if (input < min)
                    {
                        throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_LARGER_INTEGER} {min}");
                    }

                    if (max < input)
                    {
                        throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_SMALLER_INTEGER} {max}");
                    }

                    flag = false;
                }
                catch (FormatException ex)
                {
                    ShowMessage(new List<string>() { ex.Message });
                }
                catch (LibraryException ex)
                {
                    ShowMessage(ex.Message);
                }

            } while (flag);

            return input;
        }

        /// <summary>
        /// Get integer number from user input (<paramref name="message"/>, <paramref name="min"/>, <paramref name="max"/>)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="min">min number (nullable). If min is null, then default will be 0</param>
        /// <param name="max">max number (nullable). If max is null, then default will be max of integer</param>
        /// <returns></returns>
        public static int GetIntegerNullInput(string message, int? min = null, int? max = null)
        {
            var flag = true;
            int input = 0;

            if (!min.HasValue)
                min = 0;

            if (!max.HasValue)
                max = int.MaxValue;

            do
            {
                try
                {
                    var check = GetUserInput(message)!.Trim();

                    if (string.IsNullOrEmpty(check))
                    {
                        input = -1;
                        flag = false;
                    }
                    else
                    {
                        input = Convert.ToInt32(check);

                        if (input < min)
                        {
                            throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_LARGER_INTEGER} {min}");
                        }

                        if (max < input)
                        {
                            throw new LibraryException($"{LibraryConstants.ERROR_MESSAGE_SMALLER_INTEGER} {max}");
                        }

                        flag = false;
                    }
                }
                catch (FormatException ex)
                {
                    ShowMessage(new List<string>() { ex.Message });
                }
                catch (LibraryException ex)
                {
                    ShowMessage(new List<string>() { ex.Message });
                }

            } while (flag);

            return input;
        }

        public static DateTime GetDateTimeInput(string message, DateTime borrowedDate)
        {
            var flag = true;
            DateTime input = DateTime.Now;

            do
            {
                try
                {
                    input = Convert.ToDateTime(GetUserInput(message)!.Trim());

                    if (DateTime.Compare(borrowedDate.Date, input) >= 0)
                    {
                        throw new LibraryException($"{LibraryConstants.LOAN_DUE_DATE} {LibraryConstants.EARLIER_THAN_OR_EQUAL_TO_DATE} {LibraryConstants.LOAN_BORROWED_DATE}");
                    }

                    flag = false;
                }
                catch (FormatException ex)
                {
                    ShowMessage(ex.Message);
                }
                catch (LibraryException ex)
                {
                    ShowMessage(ex.Message);
                }

            } while (flag);

            return input;
        }

        public static PagingModel<T> Paging<T>(List<T> list, int page, int size = LibraryConstants.PAGE_DEFAULT_SIZE)
        {
            PagingModel<T> pagingModel;
            double TotalPage = Math.Ceiling(list.Count * 1.0 / size);
            if (page >= 0 && page <= TotalPage)
            {
                var tempList = list.Skip(page * size).Take(size).ToList();
                var lastPage = page + 1;
                pagingModel = new PagingModel<T>(tempList, page, size, Convert.ToInt32(TotalPage), page == 0, lastPage == TotalPage);
            }
            else
                throw new LibraryException(LibraryConstants.ERROR_PAGE_NOT_IN_RANGE);
            return pagingModel;
        }

        public static int GetSize()
        {
            var result = GetIntegerNullInput($"{LibraryConstants.s_pageSizeInfo} {LibraryConstants.MESSAGE_INFO_KEEP_VALUE}{LibraryConstants.MESSAGE_INPUT}", 1);
            return result == 0 ? LibraryConstants.PAGE_DEFAULT_SIZE : result;
        }

        public static void ClearScreen()
        {
            Console.Write(LibraryConstants.MESSAGE_CLEAR);
            Console.ReadKey();
            Console.Clear();
        }
        public static void ClearImmediateScreen() => Console.Clear();
    }
}