﻿using Newtonsoft.Json;

namespace LibraryManagement.Utils
{
    public static class JSonUtils<T> where T : class
    {
        public static void WriteDataIntoJson(T entity, string path)
        {
            if (File.Exists(path))
                File.Delete(path);
            var myFile = File.Create(path);
            myFile.Close();
            string result = JsonConvert.SerializeObject(entity, Formatting.Indented);
            File.WriteAllText(path, result);
        }

        public static void WriteDataIntoJson(List<T> list, string path)
        {
            if (File.Exists(path))
                File.Delete(path);
            var myFile = File.Create(path);
            myFile.Close();
            string result = JsonConvert.SerializeObject(list, Formatting.Indented);
            File.WriteAllText(path, result);
        }

        public static List<T> ReadJsonIntoData(string path)
        {
            List<T> list = new List<T>();
            if (File.Exists(path))
            {
                var content = File.ReadAllText(path);
                list = JsonConvert.DeserializeObject<List<T>>(content)!;
            }
            return list;
        }

        public static T ReadJsonIntoSingleData(string path)
        {
            if (File.Exists(path))
            {
                var content = File.ReadAllText(path);
                var entity = JsonConvert.DeserializeObject<T>(content);
                return entity;
            }
            return default(T)!;
        }
    }
}
