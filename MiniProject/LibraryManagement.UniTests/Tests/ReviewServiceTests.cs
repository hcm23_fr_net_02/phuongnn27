﻿using LibraryManagement.AppStart;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryManagement.UniTests.Tests
{
    [TestClass]
    public class ReviewServiceTests
    {
        #region Fields and constructor
        private ServiceProvider? _serviceProvider;
        private IReviewService? _reviewService;

        [TestInitialize]
        public void SetUp()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _reviewService = _serviceProvider.GetService<IReviewService>()!;
            var _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            Data.Seed(ref _unitOfWork!);
        }
        #endregion

        #region Tests

        #region Review
        [TestMethod]
        public void ReviewService_IsNullEntity_ReturnsNotNull()
        {
            Assert.IsNotNull(_reviewService);
        }
        #endregion

        #region Add
        [TestMethod]
        public void ReviewService_CreateReview_ReturnsEqual()
        {
            //Arrange
            var customer = new Customer(1, "John", "Ho Chi Minh");
            var review = new Review(7, 1, "Review 1", "Good", 4, new DateTime(2022, 04, 01),
                new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" }), 1, customer);
            var book = new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" });

            //Act
            var actual = _reviewService!.CreateReview(review, book);

            //Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void ReviewService_CreateExistedReviewById_ReturnsLibraryException()
        {
            //Arrange
            var customer = new Customer(1, "John", "Ho Chi Minh");
            var review = new Review(1, 1, "Review 1", "Good", 4, new DateTime(2022, 04, 01),
                new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" }), 1, customer);
            var book = new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _reviewService!.CreateReview(review, book));
        }

        [TestMethod]
        public void ReviewService_CreateReviewByNotExistedBookId_ReturnsLibraryException()
        {
            //Arrange
            var customer = new Customer(1, "John", "Ho Chi Minh");
            var review = new Review(1, 11, "Review 1", "Good", 4, new DateTime(2022, 04, 01),
                new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" }), 1, customer);
            var book = new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _reviewService!.CreateReview(review, book));
        }

        [TestMethod]
        public void ReviewService_CreateReviewByNotExistedBook_ReturnsLibraryException()
        {
            //Arrange
            var customer = new Customer(1, "John", "Ho Chi Minh");
            var review = new Review(1, 1, "Review 1", "Good", 4, new DateTime(2022, 04, 01),
                new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" }), 1, customer);
            var book = new Book(11, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _reviewService!.CreateReview(review, book));
        }

        [TestMethod]
        public void ReviewService_CreateReviewByNotExistedCustomerId_ReturnsLibraryException()
        {
            //Arrange
            var customer = new Customer(1, "John", "Ho Chi Minh");
            var review = new Review(1, 1, "Review 1", "Good", 4, new DateTime(2022, 04, 01),
                new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" }), 11, customer);
            var book = new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _reviewService!.CreateReview(review, book));
        }

        [TestMethod]
        public void ReviewService_CreateReviewByNotExistedCustomer_ReturnsLibraryException()
        {
            //Arrange
            var customer = new Customer(11, "John", "Ho Chi Minh");
            var review = new Review(1, 1, "Review 1", "Good", 4, new DateTime(2022, 04, 01),
                new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" }), 1, customer);
            var book = new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() { "Technology", "Science" });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _reviewService!.CreateReview(review, book));
        }
        #endregion

        #endregion
    }
}
