﻿using LibraryManagement.AppStart;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using static LibraryManagement.Commons.LibraryEnum;

namespace LibraryManagement.UniTests.Tests
{
    [TestClass]
    public class LoanServiceTests
    {
        #region Fields and constructor
        private ServiceProvider? _serviceProvider;
        private ILoanService? _loanService;

        [TestInitialize]
        public void SetUp()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _loanService = _serviceProvider.GetService<ILoanService>()!;
            var _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            Data.Seed(ref _unitOfWork!);
        }
        #endregion

        #region Tests

        #region loan Service
        [TestMethod]
        public void LoanService_IsNullEntity_ReturnsNotNull()
        {
            Assert.IsNotNull(_loanService);
        }
        #endregion

        #region Check out Loan
        [TestMethod]
        public void LoanService_CheckOutExistedBorrowedLoan_ReturnsEqual()
        {
            //Arrange 
            var loan = new Loan(1, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(1, "John", "Ho Chi Minh"));

            //Act
            var actual = _loanService!.CheckOutLoan(loan);

            //Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void LoanService_CheckOutLoanNotBelongToCustomer_ReturnsLibraryException()
        {
            //Arrange
            var loan = new Loan(1, 2, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(2, "Jenny", "Ha Noi"));

            //Assert
            Assert.ThrowsException<LibraryException>(() => _loanService!.CheckOutLoan(loan));
        }

        [TestMethod]
        public void LoanService_CheckOutExistedLoanById_ReturnsLibraryException()
        {
            //Arrange
            var loan = new Loan(11, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(1, "John", "Ho Chi Minh"));

            //Assert
            Assert.ThrowsException<LibraryException>(() => _loanService!.CheckOutLoan(loan));
        }

        [TestMethod]
        public void LoanService_CheckOutLoanWithCheckedOutStatus_ReturnsLibraryException()
        {
            //Arrange
            var loan = new Loan(2, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(2, "Book 2", "Author 1", 2021, 1, "Description 2", new List<string>() { "Action" }),
                new Customer(1, "John", "Ho Chi Minh"),
                (int)LoanStatus.CheckedOut, new DateTime(2023, 10, 01));

            //Assert
            Assert.ThrowsException<LibraryException>(() => _loanService!.CheckOutLoan(loan));
        }

        [TestMethod]
        public void LoanService_CheckOutExistedLoanByCustomerId_ReturnsLibraryException()
        {
            //Arrange
            var loan = new Loan(1, 11, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(1, "John", "Ho Chi Minh"));

            //Assert
            Assert.ThrowsException<LibraryException>(() => _loanService!.CheckOutLoan(loan));
        }
        #endregion

        #region Add
        [TestMethod]
        public void LoanService_CreateLoan_ReturnsEqual()
        {
            //Arrange
            var expected = true;
            var loan = new Loan(11, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(1, "John", "Ho Chi Minh"));
            var customer = new Customer(1, "John", "Ho Chi Minh");

            //Act
            var actual = _loanService!.CreateLoan(loan, customer);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BookService_AddExistedLoanById_ReturnsLibraryException()
        {
            //Arrange
            var loan = new Loan(1, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(1, "John", "Ho Chi Minh"));
            var customer = new Customer(1, "John", "Ho Chi Minh");

            //Assert
            Assert.ThrowsException<LibraryException>(() => _loanService!.CreateLoan(loan, customer));
        }

        [TestMethod]
        public void BookService_AddLoanByNotExistedCustomer_ReturnsLibraryException()
        {
            //Arrange
            var loan = new Loan(1, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(1, "John", "Ho Chi Minh"));
            var customer = new Customer(11, "John", "Ho Chi Minh");

            //Assert
            Assert.ThrowsException<LibraryException>(() => _loanService!.CreateLoan(loan, customer));
        }
        #endregion

        #endregion
    }
}