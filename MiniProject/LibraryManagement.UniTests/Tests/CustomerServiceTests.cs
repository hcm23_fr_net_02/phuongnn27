using LibraryManagement.AppStart;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryManagement.UniTests.Tests
{
    [TestClass]
    public class CustomerServiceTests
    {
        #region Fields and constructor
        private ServiceProvider? _serviceProvider;
        private ICustomerService? _customerService;

        [TestInitialize]
        public void SetUp()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _customerService = _serviceProvider.GetService<ICustomerService>()!;
            var _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            Data.Seed(ref _unitOfWork!);
        }
        #endregion

        #region Tests
        #region Customer Service
        [TestMethod]
        public void CustomerService_IsNullEntity_ReturnsNotNull()
        {
            Assert.IsNotNull(_customerService);
        }
        #endregion

        #region Add
        [TestMethod]
        public void CustomerService_AddACustomer_ReturnsEqual()
        {
            //Arrange
            var expected = true;
            var customer = new Customer(11, "Stephen", "Nha Trang");

            //Act
            var actual = _customerService!.AddCustomers(customer);

            //Assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void CustomerService_AddExistedCustomer_ReturnsLibraryException()
        {
            //Arrange
            var customer = new Customer(4, "Stephen", "Nha Trang");

            //Assert
            Assert.ThrowsException<LibraryException>(() => _customerService!.AddCustomers(customer));

        }
        #endregion

        #region Delete
        [TestMethod]
        public void CustomerService_DeleteACustomer_ReturnsEqual()
        {
            //Arrange
            var expected = true;
            var customer = new Customer(5, "Stephen Smith", "Nha Trang");

            //Act
            var actual = _customerService!.DeleteCustomer(customer);

            //Assert
            Assert.AreEqual(expected, actual);

        }
        [TestMethod]
        public void CustomerService_DeleteNotExistedCustomer_ReturnsLibraryException()
        {
            //Arrange
            var loan = new Loan(1, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() { "Technology", "Science" }),
                new Customer(1, "John", "Ho Chi Minh"));
            var customer = new Customer(1, "John", "Ho Chi Minh", new List<Loan>() { loan });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _customerService!.DeleteCustomer(customer));

        }
        #endregion

        #endregion
    }
}