using LibraryManagement.AppStart;
using LibraryManagement.Commons;
using LibraryManagement.Entities;
using LibraryManagement.Exceptions;
using LibraryManagement.Repositories.Interfaces;
using LibraryManagement.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using static LibraryManagement.Commons.LibraryEnum;

namespace LibraryManagement.UniTests.Tests
{
    [TestClass]
    public class BookServiceTests
    {
        #region Fields and constructor
        private ServiceProvider? _serviceProvider;
        private IBookService? _bookService;

        [TestInitialize]
        public void SetUp()
        {
            _serviceProvider = new ServiceCollection().ConfigureDependencyInjection().BuildServiceProvider();
            _bookService = _serviceProvider.GetService<IBookService>()!;
            var _unitOfWork = _serviceProvider.GetService<IUnitOfWork>();
            Data.Seed(ref _unitOfWork!);
        }
        #endregion

        #region Tests

        #region Book Service
        [TestMethod]
        public void BookService_IsNullEntity_ReturnsNotNull()
        {
            Assert.IsNotNull(_bookService);
        }
        #endregion

        #region Delete
        [TestMethod]
        public void BookService_DeleteBookRelatingToOtherInfo_ReturnsFalse()
        {
            //Arrange 
            var expected = false;

            //Act
            var actual = _bookService!.DeleteBook(new Book(1));

            //Assert
            Assert.AreEqual(expected, actual);
        }
        #endregion

        #region Update
        [TestMethod]
        public void BookService_UpdateExistedBookDetail_ReturnsEqual()
        {
            //Arrange 
            var expected = new Book(4, "Book 4 Test", "Author 2", 2020, 30, "Description 4", new List<string>() { "Romantic" });

            //Act
            var actual = _bookService!.UpdateBook(new Book(4, "Book 4 Test", "Author 2", 2020, 30, "Description 4", new List<string>() { "Romantic" }));

            //Assert
            Assert.AreEqual(expected.Title, actual.Title);
        }

        [TestMethod]
        public void BookService_UpdateNotExistedBookDetail_ReturnsLibraryException()
        {
            //Arrange 
            var book = new Book(11, "Book 4 Test", "Author 2", 2020, 30, "Description 4", new List<string>() { "Romantic" });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _bookService!.UpdateBook(book));
        }
        #endregion

        #region Add
        [TestMethod]
        public void BookService_AddBookDetail_ReturnsEqual()
        {
            //Arrange 
            var expected = true;

            //Act
            var actual = _bookService!.AddBook(new Book(11, "Book 11", "Author 2", 2020, 30, "Description 4", new List<string>() { "Romantic" }));

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BookService_AddExistedBookDetail_ReturnsLibraryException()
        {
            //Arrange 
            var book = new Book(4, "Book 4 Test", "Author 2", 2020, 30, "Description 4", new List<string>() { "Romantic" });

            //Assert
            Assert.ThrowsException<LibraryException>(() => _bookService!.AddBook(book));
        }
        #endregion

        #region Filter
        [TestMethod]
        public void BookService_FilterBooksByTitleAscending_ReturnsList()
        {
            //Arrange
            var expectedFirstElementIdInList = 1;
            Sorting sorting = new Sorting(nameof(Book.Title),
                StatusExtension<BookSortOrder>.GetStatus((int)BookSortOrder.Ascending));

            //Act
            var list = _bookService!.FilterBooks(sorting);
            var actualFirstElementIdInList = list.First().Id;

            //Assert
            Assert.AreEqual(expectedFirstElementIdInList, actualFirstElementIdInList,
                $"Expected: {expectedFirstElementIdInList} - Actual: {actualFirstElementIdInList}");
        }
        [TestMethod]
        public void BookService_FilterBooksByAuthorDescending_ReturnsList()
        {
            //Arrange
            var expectedFirstElementIdInList = 5;
            Sorting sorting = new Sorting(nameof(Book.Author),
                StatusExtension<BookSortOrder>.GetStatus((int)BookSortOrder.Descending));

            //Act
            var list = _bookService!.FilterBooks(sorting);
            var actualFirstElementIdInList = list.First().Id;

            //Assert
            Assert.AreEqual(expectedFirstElementIdInList, actualFirstElementIdInList,
                $"Expected: {expectedFirstElementIdInList} - Actual: {actualFirstElementIdInList}");
        }

        [TestMethod]
        public void BookService_FilterBooksByGenresDescending_ReturnsList()
        {
            //Arrange
            var expectedFirstElementIdInList = 1;
            Sorting sorting = new Sorting(nameof(Book.Genres),
                StatusExtension<BookSortOrder>.GetStatus((int)BookSortOrder.Descending));

            //Act
            var list = _bookService!.FilterBooks(sorting);
            var actualFirstElementIdInList = list.First().Id;

            //Assert
            Assert.AreEqual(expectedFirstElementIdInList, actualFirstElementIdInList,
                $"Expected: {expectedFirstElementIdInList} - Actual: {actualFirstElementIdInList}");
        }
        #endregion

        #endregion
    }
}