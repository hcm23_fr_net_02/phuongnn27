﻿using LibraryManagement.Entities;
using LibraryManagement.Repositories.Interfaces;
using static LibraryManagement.Commons.LibraryEnum;

namespace LibraryManagement.UniTests
{
    public static class Data
    {
        public static void Seed(ref IUnitOfWork unitOfWork)
        {

            var loans = new List<Loan>()
            {
                new Loan(1, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() {"Technology", "Science"}),
                new Customer(1, "John", "Ho Chi Minh")),

                new Loan(2, 1, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(2, "Book 2", "Author 1", 2021, 1, "Description 2", new List<string>() {"Action"}),
                new Customer(1, "John", "Ho Chi Minh"),
                (int)LoanStatus.CheckedOut, new DateTime(2023,10,01)),

                new Loan(3, 2, new DateTime(2023, 08, 10), new DateTime(2023, 10, 10),
                new Book(3, "Book 3", "Author 2", 1, 2019, "Description 3", new List<string>() {"Business"}),
                new Customer(2, "Jenny", "Ha Noi")),

                new Loan(4, 2, new DateTime(2023, 08, 10), new DateTime(2023, 10, 10),
                new Book(4, "Book 4", "Author 2", 2020, 1, "Description 4", new List<string>() {"Romantic"}),
                new Customer(2, "Jenny", "Ha Noi")),

                new Loan(5, 3, new DateTime(2023, 09, 10), new DateTime(2023, 11, 10),
                new Book(1, "Book 1", "Author 1", 2023, 1, "Description 1", new List<string>() {"Technology"}),
                new Customer(3, "Maria", "Ho Chi Minh"))
            };

            var customers = new List<Customer>()
            {
                new Customer(1, "John", "Ho Chi Minh", loans.Take(2).ToList()),
                new Customer(2, "Jenny", "Ha Noi", loans.Skip(2).Take(2).ToList()),
                new Customer(3, "Maria", "Ho Chi Minh", new List<Loan>() {loans.Last()}),
                new Customer(4, "Stephen", "Nha Trang"),
                new Customer(5, "Stephen Smith", "Nha Trang"),
            };

            var reviews = new List<Review>()
            {
                new Review(1, 1, "Review 1", "Good", 4, new DateTime(2022, 04, 01),
                new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() {"Technology", "Science"}), 1, customers.First()),
                new Review(2, 2, "Review 2", "Ok", 3, new DateTime(2023, 03, 10),
                new Book(2, "Book 2", "Author 1", 2021, 30, "Description 2", new List<string>() {"Action"}), 2, customers[2]),
                new Review(3, 3, "Review 3", "Not good", 2, new DateTime(2023, 05, 10),
                new Book(3, "Book 3", "Author 2", 2019, 30, "Description 3", new List<string>() {"Business", "Economy"}), 3, customers[3]),
                new Review(4, 4, "Review 4", "Great", 5, new DateTime(2023, 06, 23),
                new Book(4, "Book 4", "Author 2", 2020, 30, "Description 4", new List<string>() {"Romantic"})),
                new Review(5, 5, "Review 5", "Highly recommend", 5, new DateTime(2023, 07, 01),
                new Book(5, "Book 5", "Author 3", 2020, 30, "Description 5", new List<string>() {"Comedy"})),
                new Review(6, 6, "Review 6", "Good", 3, new DateTime(2023, 08, 01),
                new Book(6, "Book 6", "Author 1", 2020, 30, "Description 6", new List<string>() {"Psychology"})),
            };

            var books = new List<Book>()
            {   new Book(1, "Book 1", "Author 1", 2023, 30, "Description 1", new List<string>() {"Technology", "Science"}, new List<Review>() {reviews.First()}),
                new Book(2, "Book 2", "Author 1", 2021, 30, "Description 2", new List<string>() {"Action"}, new List<Review>() {reviews[1]}),
                new Book(3, "Book 3", "Author 2", 2019, 30, "Description 3", new List<string>() {"Business"}, new List<Review>() {reviews[2]}),
                new Book(4, "Book 4", "Author 2", 2020, 30, "Description 4", new List<string>() {"Romantic"}),
                new Book(5, "Book 5", "Author 3", 2020, 30, "Description 5", new List<string>() {"Comedy"}),
                new Book(6, "Book 6", "Author 1", 2020, 30, "Description 6", new List<string>() {"Psychology"})
            };

            unitOfWork!.BookRepository.AddRange(books);
            unitOfWork!.LoanRepository.AddRange(loans);
            unitOfWork!.ReviewRepository.AddRange(reviews);
            unitOfWork!.CustomerRepository.AddRange(customers);
        }
    }
}
