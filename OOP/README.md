# OOP

## Phone Management
- This folder includes Mobile Phone, Store, Customer, and Purchase
- [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/OOP/PhoneManagement?ref_type=heads)

## Hotel Management
- This folder includes Mobile Booking, Customer, Hotel, Invoice, and Room
- [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/OOP/HotelManagement?ref_type=heads)

## Library Management
- This folder includes Mobile Book, Customer, Library, and Loan
- [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/OOP/LibraryManagement?ref_type=heads)