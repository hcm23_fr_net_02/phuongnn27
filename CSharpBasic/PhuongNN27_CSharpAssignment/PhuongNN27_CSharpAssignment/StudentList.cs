﻿using System.Collections.Generic;
using System.Security.Cryptography;

namespace PhuongNN27_CSharpAssignment
{
    public class StudentList
    {
        public List<Student> Students;

        public StudentList(List<Student> list = null)
        {
            if (list == null)
                Students = new List<Student>();
            else
                Students = list;
        }

        public bool IsEmpty(bool showError = true) {
            if (showError && !Students.Any())
            {
                Console.WriteLine("List is empty");
            }
            return Students.Any(); 
        }

        //Add
        public void AddStudent()
        {
            Students.Add(GetNewStudentInfo());
            Console.WriteLine("Add a new student successfully!");
        }

        //Remove

        public void RemoveStudent()
        {
            if (IsEmpty(true))
            {
                int Id = SearchById(false);
                var student = Students.SingleOrDefault(s => s.Id.Equals(Id));
                if (student != null)
                {
                    Students.Remove(student);
                    Console.WriteLine($"Delete student with id {Id} successfully!");
                } else
                {
                    Console.WriteLine($"Cannot find student with Id {Id}");
                }
            }
        }

        //Search
        public void Search()
        {
            if (IsEmpty(true))
            {
                bool flag = true;
                int choice = 0;
                while (flag)
                {
                    choice = Convert.ToInt32(ShowMessage("Search by Id (1) or by Name (2): ").Trim());
                    if (choice != 1 && choice != 2)
                        Console.WriteLine("Please type number 1 or 2");
                    else
                        flag = false;
                }
                switch (choice)
                {
                    //Id
                    case 1:
                        SearchById();
                        break;
                    //Name
                    case 2:
                        SearchByName();
                        break;
                }
            }
        }

        //Show
        public void Show()
        {
            if (IsEmpty(true))
            {
                string message = "********* Menu Xem danh sách sinh viên *********" +
                "\n1. Trang trước" +
                "\n2. Trang sau" +
                "\n3. Quay lại menu chính" +
                "\nLựa chọn của bạn: ";
                int choice = 0;
                int page = 0;
                var pagingModel = GetPagingByPage(page);
                while (choice != 3)
                {
                    try
                    {
                        choice = Convert.ToInt32(ShowMessage(message).Trim());
                        if (choice < 1 && choice > 3)
                            throw new Exception("Please type from 1 to 3");
                        switch(choice)
                        {
                            case 1:
                                if (!pagingModel.IsFirstPage)
                                {
                                    page -= 1;
                                    pagingModel = GetPagingByPage(page);
                                    PrintStudentsByPage(pagingModel);
                                }
                                else
                                    Console.WriteLine("Current page is the first page");
                                break;
                            case 2:
                                if (!pagingModel.IsLastPage)
                                {
                                    page += 1;
                                    pagingModel = GetPagingByPage(page);
                                    PrintStudentsByPage(pagingModel);
                                }
                                else
                                    Console.WriteLine("Current page is the first page");
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        if (!string.IsNullOrEmpty(e.Message))
                            Console.WriteLine(e.Message);
                        else
                            Console.WriteLine("Please type number");
                    }
                }
            }
        }

        #region Utils
        private string ShowMessage(string message)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write(message);
            return Console.ReadLine();
        }

        private Student GetNewStudentInfo()
        {
            int Id = !IsEmpty(false) ? 1 : Students.Count() + 1;
            string FullName = string.Empty;
            int Age = 0;
            float GPA = 0;
            bool flag = true;
            string message = string.Empty;

            //Full name
            while (flag)
            {
                message = string.Empty;
                FullName = ShowMessage("Full name: ").Trim();
                if (!CheckFullName(FullName))
                    message = "Please type valid full name";
                if (string.IsNullOrEmpty(FullName))
                    message = "Please type full name";
                if (string.IsNullOrEmpty(message))
                    flag = false;
                else
                    Console.WriteLine(message);                
            }
            flag = true;
            //Age
            while (flag)
            {
                message = string.Empty;
                string input = ShowMessage("Age: ").Trim();
                if (string.IsNullOrEmpty(input))
                    message = "Please type age";
                Age = Convert.ToInt32(input);
                if (Age <= 0)
                    message = "Please type age larger than 0";
                if (string.IsNullOrEmpty(message))
                    flag = false;
                else
                    Console.WriteLine(message);
            }

            //GPA
            flag = true;
            while (flag)
            {
                message = string.Empty;
                string input = ShowMessage("GPA (1 - 10): ").Trim();
                if (string.IsNullOrEmpty(input))
                    message = "Please type GPA";
                GPA = float.Parse(input);
                if (GPA < 0f || GPA > 10f)
                    message = "Please type GPA (1 - 10)";
                if (string.IsNullOrEmpty(message))
                    flag = false;
                else
                    Console.WriteLine(message);
            }

            return new Student(Id, FullName, Age, GPA);
        }

        private bool CheckFullName(string name)
        {
            var result = true;
            foreach (char c in name) 
            {
                if (char.IsDigit(c) || char.IsSymbol(c))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        private int SearchById(bool IsSearch = true)
        {
            var flag = true;
            int Id = 0;
            while (flag)
            {
                try
                {
                    Id = Convert.ToInt32(ShowMessage("Please type Id: ").Trim());
                    flag = false;
                } catch
                {
                    Console.WriteLine("Please type number");
                }
            }
            if (IsSearch)
            {
                var list = Students.Where(s => s.Id.Equals(Id)).ToList();
                string message = $"Id: {Id}";
                ShowSearchResult(list, message);
            }
            return Id;
        }

        private void SearchByName()
        {
            var flag = true;
            string FullName = string.Empty;
            while (flag)
            {
                try
                {
                    FullName = ShowMessage("Please type Id: ").Trim();
                    flag = false;
                }
                catch
                {
                    Console.WriteLine("Please type Id");
                }
            }

            var list = Students.Where(s => s.FullName.ToLower().Contains(FullName)).ToList();
            string message = $"Full name: {FullName}";
            ShowSearchResult(list, message);
        }

        private void ShowSearchResult(List<Student> list, string message)
        {
            if (list.Any())
            {
                Console.OutputEncoding = System.Text.Encoding.UTF8;
                Console.WriteLine($"\n Student with {message}" +
                    $"\n-------------------------------------------");
                list.ForEach(s => Console.WriteLine(s.ToString()));
                Console.WriteLine($"\n-------------------------------------------");
            }
            else
            {
                Console.WriteLine("Cannot find match students");
            }
        }

        public PagingModel<Student> GetPagingByPage(int page)
        {
            int size = 5;
            double totalPage = (double)((Students.Count * 1.0) / size);
            var list = this.Students.Skip(page * size).Take(size).ToList();
            int lastPage = page + 1;
            var result = new PagingModel<Student>(list, page, page == 0, lastPage == totalPage, size, totalPage);
            return result;
        }

        private void PrintStudentsByPage(PagingModel<Student> pagingModel)
        {
            if (pagingModel != null)
            {
                Console.OutputEncoding = System.Text.Encoding.UTF8;
                Console.WriteLine($"\n List students" +
                    $"\n-------------------------------------------");
                Console.WriteLine($"Page: {pagingModel.Page + 1} | {pagingModel.TotalPage} - Total: {pagingModel.Total}");
                pagingModel.Values.ForEach(e =>  Console.WriteLine(e.ToString()));
                Console.WriteLine($"\n-------------------------------------------");
            }
        }
        #endregion
    }
}
