﻿namespace PhuongNN27_CSharpAssignment
{
    public class Student
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public float GPA { get; set; }

        public Student(int Id, string FullName, int Age, float GPA)
        {
            this.Id = Id;
            this.FullName = FullName;
            this.GPA = GPA;
            this.Age = Age;
        }

        public override string ToString()
            => $"{Id} - {FullName} - {Age} - {GPA}";
    }
}
