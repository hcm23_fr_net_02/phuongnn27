﻿using PhuongNN27_CSharpAssignment;

Console.OutputEncoding = System.Text.Encoding.UTF8;

StudentList list = new StudentList();

int choice = 0;
string info = "********* Menu chính *********" +
                "\n1. Thêm sinh viên" +
                "\n2. Xem danh sách sinh viên" +
                "\n3. Tìm sinh viên" +
                "\n4. Xóa sinh viên" +
                "\n5. Thoát" +
                "\nLựa chọn của bạn: ";
do
{
    try
    {
        Console.Write(info);
        choice = Convert.ToInt32(Console.ReadLine().Trim());
        switch(choice)
        {
            case 1:
                list.AddStudent();
                break;
            case 2:
                list.Show();
                break;
            case 3:
                list.Search();
                break;
            case 4:
                list.RemoveStudent();
                break;
        }
        Console.Write("Please enter to clear console ");
        Console.ReadKey();
        Console.Clear();
    } catch
    {
        Console.WriteLine("Please type number");
    }
} while (choice != 5);
Console.WriteLine("Good bye!");