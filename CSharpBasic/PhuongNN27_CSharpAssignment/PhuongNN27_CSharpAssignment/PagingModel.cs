﻿namespace PhuongNN27_CSharpAssignment
{
    public class PagingModel<T>
    {
        public int Total { get; }
        public int Page { get; }
        public int Size { get; set; }
        public double TotalPage { get; set; }
        public List<T> Values { get; set; }
        public bool IsFirstPage { get; set; }
        public bool IsLastPage { get; set; }

        public PagingModel(List<T> values, int page, bool isFirstPage, bool isLastPage, int size, double totalPage)
        {
            Values = values;
            IsFirstPage = isFirstPage;
            IsLastPage = isLastPage;
            this.Size = size;
            Page = page;
            Total = values.Count();
            TotalPage = totalPage;

        }
    }
}
