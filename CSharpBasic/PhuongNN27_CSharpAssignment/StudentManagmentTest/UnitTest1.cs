using PhuongNN27_CSharpAssignment;

namespace StudentManagmentTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestFirstPageOfStudentPaging()
        {
            var list = new List<Student>() {
                new Student(1, "Ph??ng", 22, 7),
                new Student(2, "Lan", 18, 8)
            };
            StudentList studentList = new StudentList(list);
            var pageModel = studentList.GetPagingByPage(0);
            bool expectedOfFirstPage = true;
            Assert.AreEqual(expectedOfFirstPage, pageModel.IsFirstPage);
        }
        
        [TestMethod]
        public void TestLastPageOfStudentPaging()
        {
            var list = new List<Student>() {
                new Student(1, "Ph??ng", 22, 7),
                new Student(2, "Lan", 18, 8)
            };
            StudentList studentList = new StudentList(list);
            var pageModel = studentList.GetPagingByPage(0);
            bool expectedOfFirstPage = false;
            Assert.AreEqual(expectedOfFirstPage, pageModel.IsLastPage);
        }
    }
}