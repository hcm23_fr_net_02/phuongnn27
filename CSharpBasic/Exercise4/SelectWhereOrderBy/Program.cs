﻿using System.Text.Json;
using System.Text.RegularExpressions;

var list = new List<Employee>();
var path = "mock-data.json";

// đọc file mock-data.json trong project để lấy dữ liệu
list = await UploadFromJson(list);

// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

if (list.Any())
{
    var input = 0;
    int amount = list.Count;

    do
    {
        try
        {
            Console.Write("Please choose from 1 to 10 or choose 11 to quit: ");
            input = Convert.ToInt32(Console.ReadLine());

            if (input > 0 && input <= 10)
            {
                int number = GetNumberFromList();
                amount = number == 0 ? list.Count : number;
            }

            switch (input)
            {
                case 1:
                    //Lấy ra danh sách các FirstName của tất cả các nhân viên.

                    Console.WriteLine("1. Danh sách first name\n" +
                        "-----------------------------------------------");
                    list.Select(e => e.FirstName)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e));

                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 2:
                    //Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.

                    Console.WriteLine("2. Danh sách salary lớn hơn $50000\n" +
                        "-----------------------------------------------");
                    list.Where(e => e.Salary > 50000).Take(amount).ToList().ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 3:
                    //Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
                    Console.WriteLine("3. Danh sách nhân viên nam sắp theo thứ tự tên tăng dần lớn hơn $50000\n" +
                        "-----------------------------------------------");
                    list.Where(e => !string.IsNullOrEmpty(e.Gender) && e.Gender.ToLower().Equals("male"))
                        .OrderBy(e => e.FirstName)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 4:
                    //Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
                    Console.WriteLine("4. Danh sách nhân viên manager ở Indonesia\n" +
                        "-----------------------------------------------");
                    list.Where(e => 
                    !string.IsNullOrEmpty(e.country) &&
                    !string.IsNullOrEmpty(e.JobTitle) &&
                    e.country.ToLower().Equals("indonesia") &&
                    e.JobTitle.ToLower().Contains("manager"))
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 5:
                    //Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
                    Console.WriteLine("5. Danh sách nhân viên có mail sắp giảm dần theo họ\n" +
                        "-----------------------------------------------");
                    list.Where(e => !string.IsNullOrEmpty(e.Email.Trim()))
                        .OrderByDescending(e => e.LastNAme)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 6:
                    //Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.
                    var date = new DateTime(2022, 1, 1);
                    Console.WriteLine("6. Danh sách salary lớn hơn $60000 và StartDate trước ngày \"2022-01-01\"\n" +
                        "-----------------------------------------------");
                    list.Where(e => e.Salary > 60000 &&
                        e.ConvertDateTime().CompareTo(date) < 0)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 7:
                    //Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
                    Console.WriteLine("7. Danh sách nhân viên có PostalCode là null hoặc rỗng sắp tăng dần theo họ\n" +
                        "-----------------------------------------------");
                    list.Where(e => !string.IsNullOrEmpty(e.PostalCode))
                        .OrderByDescending(e => e.LastNAme)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 8:
                    //Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
                    Console.WriteLine("8. Danh sách nhân viên có FirstName và LastName được viết hoa sắp giảm dần theo Id\n" +
                        "-----------------------------------------------");
                    var regex = new Regex(@"(?=.*[A-Z])");
                    list.Where(e => !string.IsNullOrEmpty(e.FirstName) &&
                        !string.IsNullOrEmpty(e.LastNAme) &&
                        regex.IsMatch(e.FirstName) &&
                        regex.IsMatch(e.LastNAme))
                        .OrderByDescending(e => e.Id)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 9:
                    //Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
                    Console.WriteLine("9. Danh sách nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ sắp tăng dần theo salary\n" +
                        "-----------------------------------------------");
                    list.Where(e => e.Salary >= 50000 && e.Salary <= 700000)
                        .OrderByDescending(e => e.Salary)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 10:
                    //Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
                    Console.WriteLine("10. Danh sách nhân viên có FirstName chứa chữ \"a\" hoặc \"A\" sắp gỉam dần theo họ\n" +
                        "-----------------------------------------------");
                    list.Where(e => e.FirstName.Contains("a") || e.FirstName.Contains("A"))
                        .OrderByDescending(e => e.LastNAme)
                        .Take(amount).ToList()
                        .ForEach(e => Console.WriteLine(e.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
            }
        }
        catch
        {
            Console.WriteLine("Please type number");
        }

    } while (input != 11);
    Console.WriteLine("Good bye!");
}
async Task<List<Employee>> UploadFromJson(List<Employee> employees)
{
    var content = await File.ReadAllTextAsync(path);
    employees = JsonSerializer.Deserialize<List<Employee>>(content);
    return employees;
}

int GetNumberFromList()
{
    var result = 0;
    while(true)
    {
        try
        {
            Console.Write("Enter number or enter without number to take all: ");
            var input = Console.ReadLine().Trim();
            if (string.IsNullOrEmpty(input))
                break;
            else
            {
                result = Convert.ToInt32(input);
                break;
            }
        } catch
        {
            Console.WriteLine("Please type number");
        }
    }
    return result;
}

public class Employee
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastNAme { get; set; }
    public string Email { get; set; }
    public string Gender { get; set; }
    public string country { get; set; }
    public string PostalCode { get; set; }
    public string JobTitle { get; set; }
    public string StartDate { get; set; }
    public string EndDate { get; set; }
    public decimal Salary { get; set; }

    public override string ToString()
    {
        return this.Id + " - " + this.FirstName + " - " + this.LastNAme + " - " + this.Salary + " - " + this.Email;
    }

    public DateTime ConvertDateTime() => Convert.ToDateTime(StartDate);
}
