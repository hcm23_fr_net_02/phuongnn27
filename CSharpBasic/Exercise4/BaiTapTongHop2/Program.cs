﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;

var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);

if (products.Any() && categories.Any())
{
    var input = 0;
    var temp = categories.Join(products,
        category => category.Id,
        product => product.CategoryId,
        (category, product) => new
        {
            category = category,
            product = product,
        })
        .GroupBy(p => p.category)
        .Select(p => new
        {
            key = p.Key,
            value = p.Select(p => p.product).ToList()
        })
        .OrderBy(p => p.key.Name)
        .ToList();
    do
    {
        try
        {
            Console.Write("Please choose from 1 to 20 or choose 0 to quit: ");
            input = Convert.ToInt32(Console.ReadLine());

            switch (input)
            {
                case 1:
                    //Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.
                    Console.WriteLine("1. List of descending-based-price products whose price is larger than $100\n" +
                                            "-----------------------------------------------");
                    products.Where(p => p.Price > 100)
                        .OrderByDescending(p => p.Price).ToList()
                        .ForEach(p => Console.WriteLine(p.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 2:
                    //Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
                    temp.ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name} - Amount: {p.value.Count}");
                        Console.WriteLine($"--------------------------------------------------");
                    });
                    break;
                case 3:
                    //Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
                    temp.ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name} - Avarage: {p.value.Average(pr => pr.Price)}");
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;
                case 4://Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.
                    products
                        .OrderByDescending(p => p.Price)
                        .Take(10)
                        .ToList()
                        .ForEach(p => Console.WriteLine(p.ToString()));
                    break;
                case 5:
                    //Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.
                    temp.ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        p.value.GroupBy(v => v.Name).Select(v => new
                        {
                            Name = v.Key,
                            Average = v.Select(v1 => v1).Average(v2 => v2.Price)
                        }).OrderByDescending(v => v.Average).ToList()
                        .ForEach(value => Console.WriteLine($"Name: {value.Name} - Average: {value.Average}"));
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;
                case 6:
                    //Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.
                    temp.Where(p => p.value.Select(v => v.Price)
                    .Any(price => price < 50)).ToList()
                    .ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        p.value.Where(v => v.Price > 50).ToList()
                        .ForEach(value => Console.WriteLine($"{value.Name} - {value.Price}"));
                        Console.WriteLine("--------------------------------------------------");

                    });
                    break;
                case 7:
                    //Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
                    temp.ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name} - Sum price: {p.value.Sum(v => v.Price)}");
                        Console.WriteLine($"--------------------------------------------------");
                    });
                    break;
                case 8:
                    //Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.
                    temp.Where(p => p.value.Select(v => v.Name)
                    .Any(Name => Name.Contains("Apple"))).ToList()
                    .ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        p.value.Where(v => v.Name.Contains("Apple"))
                        .OrderBy(v => v.Name).ToList()
                        .ForEach(value => Console.WriteLine($"{value.Name} - {value.Price}"));
                        Console.WriteLine("--------------------------------------------------");

                    });
                    break;
                case 9:
                    //Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.
                    temp.Where(p => p.value.Sum(v => v.Price) > 1000).ToList()
                        .ForEach(p =>
                        {
                            Console.WriteLine($"Category: {p.key.Name} - Sum: {p.value.Sum(v => v.Price)}");
                        });
                    break;
                case 10:
                    //Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.
                    temp.Where(p => p.value.Select(v => v.Price)
                    .Any(price => price < 10)).ToList()
                    .ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        p.value.Where(v => v.Price < 10)
                        .OrderBy(v => v.Name).ToList()
                        .ForEach(value => Console.WriteLine($"{value.Name} - {value.Price}"));
                        Console.WriteLine("--------------------------------------------------");

                    });
                    break;
                case 11:
                    //Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
                    temp.ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        var price = p.value.Max(v => v.Price);
                        p.value.Where(v => v.Price.Equals(price)).ToList()
                        .ForEach(value => Console.WriteLine($"{value.Name} - {value.Price}"));
                        Console.WriteLine("--------------------------------------------------");

                    });
                    break;
                case 12:
                    //Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.
                    temp.ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        p.value
                        .GroupBy(v => v.Id)
                        .OrderByDescending(v => v.Select(v1 => v1.Price).Sum())
                        .ToList().ForEach(value =>
                        Console.WriteLine($"{value.First().Name} - {value.Select(v => v.Price).Sum()}"));
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;
                case 13:
                    //Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.
                    var AverageOfAllProducts = products.Average(p => p.Price);
                    Console.WriteLine($"Average price: {AverageOfAllProducts}");
                    temp.Where(p => p.value.Any(v => v.Price > AverageOfAllProducts)).ToList()
                        .ForEach(p =>
                        {
                            Console.WriteLine($"Category: {p.key.Name}");
                            Console.WriteLine("--------------------------------------------------");
                            p.value.ForEach(value =>
                            Console.WriteLine($"{value.Name} - {value.Price}"));
                            Console.WriteLine("--------------------------------------------------");
                        });
                    break;
                case 14:
                    //Tính tổng số tiền của tất cả các sản phẩm.
                    temp.ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.key.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        p.value.GroupBy(v => v.Id).Select(v => new
                        {
                            product = v.Select(v1 => v1).First(),
                            total = v.Select(v1 => v1).Sum(v2 => v2.Price)
                        }).ToList().ForEach(value =>
                        {
                            Console.WriteLine($"{value.product.Name} - {value.total}");
                        });
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;
                case 15:
                    // Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.
                    temp.Select(p => new
                    {
                        category = p.key,
                        price = p.value.Max(v => v.Price)
                    }).OrderByDescending(p => p.price).ToList()
                    .Join(temp,
                    temp1 => temp1.category.Id,
                    temp2 => temp2.key.Id,
                    (category, productTemp) => new
                    {
                        category = category.category,
                        productList = productTemp.value.Where(p => p.Price == category.price)
                    }).GroupBy(p => p.category).Select(p => new
                    {
                        category = p.Key,
                        productList = p.SelectMany(product => product.productList).ToList()
                    }).OrderByDescending(p => p.productList.Count()).ToList()
                    .ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.category.Name} - Amount: {p.productList.Count()}");
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;
                case 16:
                    //Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.
                    temp.Select(p => new
                    {
                        category = p.key,
                        product = p.value.MinBy(v => v.Price)
                    }).OrderByDescending(p => p.product.Price).ToList().ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.category.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        Console.WriteLine($"{p.product.Name} - {p.product.Price}");
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;
                case 17:
                    //Lấy danh sách các danh mục có ít sản phẩm nhất.
                    var AverageProductsInCategory = temp.Select(p => p.value.GroupBy(p => p.Id).Count()).Average();
                    temp.Where(p => p.value.GroupBy(p => p.Id).Count() < AverageProductsInCategory).ToList()
                        .ForEach(p => Console.WriteLine($"Category: {p.key.Name} - Count: {p.value.GroupBy(p => p.Id).Count()}"));
                    break;
                case 18:
                    //Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.
                    temp.Select(p => new
                    {
                        category = p.key,
                        product = p.value.MaxBy(v => v.Price)
                    }).OrderByDescending(p => p.product.Price).ToList().ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.category.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        Console.WriteLine($"{p.product.Name} - {p.product.Price}");
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;
                case 19:
                    //Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.
                    Console.WriteLine($"The total of products whose price is larger than $50: " +
                        $"{products.Where(p => p.Price > 50).Select(p => p.Price).Sum()}");
                    break;
                case 20:
                    //Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.
                    temp.Select(p => new
                    {
                        category = p.key,
                        product = p.value.MinBy(v => v.Price)
                    }).OrderBy(p => p.product.Price).ToList().ForEach(p =>
                    {
                        Console.WriteLine($"Category: {p.category.Name}");
                        Console.WriteLine("--------------------------------------------------");
                        Console.WriteLine($"{p.product.Name} - {p.product.Price}");
                        Console.WriteLine("--------------------------------------------------");
                    });
                    break;

            }
        }
        catch
        {
            Console.WriteLine("Please type number");
        }
    } while (input != 0);
    Console.WriteLine("Good bye!");
}

class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public decimal Price { get; set; }

    public override string ToString()
        => Id + " - " + Name + " - " + CategoryId + " - " + Price;
}

class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
}
