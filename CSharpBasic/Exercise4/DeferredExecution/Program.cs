﻿using System.Text.Json;

var list = new List<Orders>();
var path = "mock_data.json";

list = await UploadFromJson(list);

if (list.Any())
{
    var input = 0;
    int amount = list.Count;
    var options = new List<int>() { 1, 2, 3, 4, 11};

    do
    {
        try
        {
            Console.Write("Please choose from 1 to 9 or choose 10 to quit: ");
            input = Convert.ToInt32(Console.ReadLine());

            switch (input)
            {
                case 1:
                    //Tính tổng số tiền của tất cả các đơn đặt hàng

                    Console.WriteLine($"1. Sum of all orders' total amount: " +
                        $"{list.Sum(o => o.TotalAmount)}");
                    break;
                case 2:
                    //Lấy ra đơn hàng có giá trị (TotalAmount) cao nhất
                    Console.WriteLine("2. Max Total Amount: " +
                        $"{list.Max(o => o.TotalAmount)}");
                    break;
                case 3:
                    //Lấy ra đơn hàng có giá trị thấp nhất
                    Console.WriteLine("2. Min Total Amount: " +
                        $"{list.Min(o => o.TotalAmount)}");
                    break;
                case 4:
                    //Tính giá trị trung bình của các đơn
                    Console.WriteLine("2. Average of all orders: " +
                        $"{list.Average(o => o.TotalAmount)}");
                    break;
                //Elisha
                case 5:
                    //Tính tổng số đơn đặt hàng của một khách hàng cụ thể
                    string name = GetInputFromUser("Enter customer name: ");
                    if (string.IsNullOrEmpty(name))
                        Console.WriteLine("Cannot find customer");
                    else
                    {
                        var temp = list.Where(e => name.Equals(e.CustomerName));
                        if (temp.Any())
                        {
                            Console.WriteLine($"2. Count of {name}'s orders: {temp.Count()}");
                        } else
                            Console.WriteLine("Cannot find customer");
                    }
                    break;
                case 6:
                    //Tính tổng số tiền của tất cả các đơn đặt hàng của một khách hàng cụ thể
                    name = GetInputFromUser("Enter customer name: ");
                    if (string.IsNullOrEmpty(name))
                        Console.WriteLine("Cannot find customer");
                    else
                    {
                        var temp = list.Where(e => name.Equals(e.CustomerName));
                        if (temp.Any())
                        {
                            Console.WriteLine($"2. Sum of {name}'s total amount: " +
                                $"{temp.Sum(o => o.TotalAmount)}");
                        }
                        else
                            Console.WriteLine("Cannot find customer");
                    }
                    break;
                case 7:
                    //Lấy ra đơn hàng có giá trị cao nhất của một khách hàng cụ thể
                    name = GetInputFromUser("Enter customer name: ");
                    if (string.IsNullOrEmpty(name))
                        Console.WriteLine("Cannot find customer");
                    else
                    {
                        var temp = list.Where(e => name.Equals(e.CustomerName));
                        if (temp.Any())
                        {
                            var order = temp.Single(o =>
                            o.TotalAmount == temp.Max(o => o.TotalAmount));
                            Console.WriteLine($"2. Max of {name}'s total amount: " +
                                $"{order.ToString()}");
                        }
                        else
                            Console.WriteLine("Cannot find customer");
                    }
                    break;
                case 8:
                    //Lấy ra đơn hàng có giá trị thấp nhất cho một khách hàng cụ thể
                    name = GetInputFromUser("Enter customer name: ");
                    if (string.IsNullOrEmpty(name))
                        Console.WriteLine("Cannot find customer");
                    else
                    {
                        var temp = list.Where(e => name.Equals(e.CustomerName));
                        if (temp.Any())
                        {
                            var order = temp.Single(o =>
                            o.TotalAmount == temp.Min(o => o.TotalAmount));
                            Console.WriteLine($"2. Min of {name}'s total amount: " +
                                $"{order.ToString()}");
                        }
                        else
                            Console.WriteLine("Cannot find customer");
                    }
                    break;
                case 9:
                    //Tính giá trị trung bình các đơn đặt hàng của một khách hàng cụ thể.
                    name = GetInputFromUser("Enter customer name: ");
                    if (string.IsNullOrEmpty(name))
                        Console.WriteLine("Cannot find customer");
                    else
                    {
                        var temp = list.Where(e => name.Equals(e.CustomerName));
                        if (temp.Any())
                        {
                            Console.WriteLine($"2. Average of {name}'s total amount: " +
                                $"{temp.Average(o => o.TotalAmount)}");
                        }
                        else
                            Console.WriteLine("Cannot find customer");
                    }
                    break;
            }
        }
        catch
        {
            Console.WriteLine("Please type number");
        }

    } while (input != 10);
    Console.WriteLine("Good bye!");
}

string GetInputFromUser(string message)
{
    Console.Write(message);
    return Console.ReadLine().Trim();
}

async Task<List<Orders>> UploadFromJson(List<Orders> employees)
{
    var content = await File.ReadAllTextAsync(path);
    employees = JsonSerializer.Deserialize<List<Orders>>(content);
    return employees;
}
public class Orders
{
    public int Id { get; set; }
    public string CustomerName { get; set; }
    public decimal TotalAmount { get; set; }

    public override string ToString()
        => this.Id + " - " + this.CustomerName + " - " + this.TotalAmount;
}
