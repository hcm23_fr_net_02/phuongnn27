﻿// Hãy giả định chúng ta có một danh sách các sản phẩm của một cửa hàng với các thuộc tính sau: 
// tên sản phẩm, giá, và danh mục (category). 
// Hãy tạo một danh sách các sản phẩm và thực hiện các yêu cầu sau bằng cách sử dụng Linq:

// Tạo danh sách các sản phẩm:

var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Áo thun", Price = 250000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" }
};

if (products.Any())
{
    var input = 0;
    var list = new Dictionary<string, List<Product>>();
    do
    {
        try
        {
            Console.Write("Please choose from 1 to 6 or choose 0 to quit: ");
            input = Convert.ToInt32(Console.ReadLine());

            if (input >= 3 && input <= 6 && !list.Any())
            {
                list = products.GroupBy(p => p.Category)
                    .ToDictionary(p => p.Key, p => p.ToList());
            }

            switch (input)
            {
                case 1:
                    // Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:
                    Console.WriteLine("1. List of products whose price is larger than 500000 VND\n" +
                                        "-----------------------------------------------");
                    products.Where(p => p.Price > 500000).ToList()
                        .ForEach(p => Console.WriteLine(p.ToString()));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 2:
                    // Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:
                    Console.WriteLine("2. List of descending-based-price products' name whose price is smaller than 300000 VND\n" +
                                            "-----------------------------------------------");
                    products.Where(p => p.Price > 500000)
                        .OrderByDescending(p => p.Price)
                        .Select(p => p.Name).ToList()
                        .ForEach(p => Console.WriteLine(p));
                    Console.WriteLine("-----------------------------------------------");
                    break;
                case 3:
                    // Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
                    foreach (var item in list)
                    {
                        Console.WriteLine($"Category: {item.Key}");
                        Console.WriteLine("--------------------------------------------------");
                        item.Value.ForEach(item => Console.WriteLine($"{item.Name} - {item.Price}"));
                        Console.WriteLine("--------------------------------------------------");
                    }
                    break;
                case 4:
                    // Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
                    Console.WriteLine($"3. Sum of category Ao's Price: {list.SingleOrDefault(p => p.Key.Equals("Áo")).Value.Sum(p => p.Price)}");
                    break;
                case 5:
                    // Tìm sản phẩm có giá cao nhất:
                    Console.WriteLine($"4. Max price of product:");
                    var price = products.Max(p => p.Price);
                    list.SelectMany(p => p.Value)
                        .Where(p => p.Price.Equals(price))
                        .ToList()
                        .ForEach(p => Console.WriteLine(p.ToString()));
                    break;
                case 6:
                    // Tạo danh sách các sản phẩm với tên đã viết hoa:
                    Console.WriteLine($"5. List of product whose name is captialized:");
                    list.SelectMany(p => p.Value).ToList()
                        .ForEach(p => Console.WriteLine($"{p.Name.ToUpper()} - {p.Price} - {p.Category}"));
                    break;

            }
        }
        catch
        {
            Console.WriteLine("Please type number");
        }
    } while (input != 0);
    Console.WriteLine("Good bye!");
}

class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }

    public override string ToString()
        => this.Name + " - " + this.Price + " - " + this.Category;
}