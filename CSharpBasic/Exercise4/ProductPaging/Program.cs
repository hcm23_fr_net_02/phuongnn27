﻿using Newtonsoft.Json;

var productsJson = File.ReadAllText("products.json");
var list = JsonConvert.DeserializeObject<List<Product>>(productsJson);

if (list.Any())
{
    int size = 10;
    var input = 0;
    int page = 0;
    PagingModel<Product> model = Paging(list, page, size);

    string Info = "----------------------Paging--------------------" +
        "\n1. Set size for paging" +
        "\n2. Show current page" +
        "\n3. Previous page" +
        "\n4. Next page" +
        "\n0. Quit" +
        "\nYour choice: ";
    string Info_Previous = "----------------------Paging--------------------" +
        "\n1. Set size for paging" +
        "\n2. Show current page" +
        "\n3. Previous page" +
        "\n0. Quit" +
        "\nYour choice: ";
    string Info_Next = "----------------------Paging--------------------" +
        "\n1. Set size for paging" +
        "\n2. Show current page" +
        "\n4. Next page" +
        "\n0. Quit" +
        "\nYour choice: ";
    string message = "";

    do
    {
        try
        {
            if (model.IsFirstPage && !model.IsLastPage)
                message = Info_Next;
            else if (model.IsLastPage && !model.IsFirstPage)
                message = Info_Previous;
            else
                message = Info;
            Console.Write(message);
            input = Convert.ToInt32(Console.ReadLine());
            switch (input)
            {
                case 1:
                    size = GetInputFromUser("Please enter size of each page" +
                        "\n(enter for default size will be 10): ");
                    Console.WriteLine("Change size successfully!");
                    break;
                case 2:
                    model = Paging(list, page, size);
                    Console.WriteLine(model.ToString());
                    break;
                case 3:
                    if (!model.IsFirstPage)
                    {
                        page -= 1;
                        model = Paging(list, page, size);
                        Console.WriteLine(model.ToString());
                    } else
                        Console.WriteLine("Current page is first page");
                    break;
                case 4:
                    if (!model.IsLastPage)
                    {
                        page += 1;
                        model = Paging(list, page, size);
                        Console.WriteLine(model.ToString());
                    }  else
                    Console.WriteLine("Current page is last page");
                    break;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    } while (input != 0);
    Console.WriteLine("Good bye!");
}

int GetInputFromUser(string message)
{
    bool flag = true;
    int result = 10;
    while (flag)
    {
        try
        {
            Console.Write(message);
            string input = Console.ReadLine().Trim();
            if (string.IsNullOrEmpty(input))
            {
                flag = false;
                result = 10;
            }
            else
            {
                result = Convert.ToInt32(input);
                if (result <= 0)
                    throw new Exception();
                else
                    flag = false;
            }
        }
        catch
        {
            Console.WriteLine("Please enter number which is larger than 0");
        }
    }
    return result;
}

PagingModel<T> Paging<T>(List<T> list, int page, int size = 10)
{
    PagingModel<T> pagingModel = null;
    double TotalPage = Math.Ceiling(list.Count * 1.0 / size);
    if (page >= 0 && page <= TotalPage)
    {
        var tempList = list.Skip(page * size).Take(size).ToList();
        var lastPage = page + 1;
        pagingModel = new PagingModel<T>(tempList, page, size, Convert.ToInt32(TotalPage), page == 0, lastPage == TotalPage);
    }
    else
        throw new Exception("page is not the range of total page");
    return pagingModel;
}

class PagingModel<T>
{
    public int Total { get; }
    public int Size { get; }
    public List<T> List { get; set; }
    public int Page { get; set; }
    public int TotalPage { get; set; }
    public bool IsFirstPage { get; set; }
    public bool IsLastPage { get; set; }

    public PagingModel(List<T> List, int Page, int Size, int totalPage, bool IsFirstPage, bool IsLastPage)
    {
        this.List = List;
        this.Page = Page;
        this.Size = Size;
        this.IsFirstPage = IsFirstPage;
        this.IsLastPage = IsLastPage;
        Total = List.Count();
        TotalPage = totalPage;
    }

    public override string ToString()
    {
        var tempPage = Page + 1;
        string message = "";
        message += "--------------Page------------" +
            $"\nPage: {tempPage}/{TotalPage} | Total: {Total}\n";
        List.ForEach(item => message += $"{item.ToString()}\n ");
        message += "\n------------------------------";
        return message;
    }

}

class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CategoryId { get; set; }
    public decimal Price { get; set; }

    public override string ToString()
        => Id + " - " + Name + " - " + CategoryId + " - " + Price;
}