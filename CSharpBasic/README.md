# C# Basic
C# Basic folder includes following features:

## Exercise 1
1. Info
- DateTime
- Queue, Stack
- String
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise1?ref_type=heads)

## Exercise 2
1. Info
- Delegate
- Generic
- Stack, Queue
- Collection (List, Dictionary)
- JsonConvert (Practice)
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise2?ref_type=heads)

## Exercise 3
1. Info
- Extension
- HttpClient
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise3?ref_type=heads)

## Exercise 4
1. Info
- Paging
- LinQ
- Deferred Execution
2. [Link](https://gitlab.com/hcm23_fr_net_02/HCM23_FR_NET_02_ASSIGNMENT/-/tree/phuongnn27/CSharpBasic/Exercise4?ref_type=heads)

## Test
1. Info: Student Management (Console Application)
- Add a student
- Show student by page
- Remove a student by Id
- Search a student by Id or by name
2. Link