﻿namespace StackAndQueue
{
    public class Program
    {
        static void Main(string[] args)
        {
            Stack1<string> stack = new Stack1<string>(5);
            stack.Push("hello");
            stack.Push("world");
            stack.Push("1");
            stack.Show();

            stack.Pop();
            stack.Show();
        }
    }
    public class Stack1<T>
    {
        private T[] Items;
        private int Capacity;
        public int Count;

        public Stack1()
        {
            this.Capacity = 10;
            Items = new T[Capacity];
            Count = 0;
        }

        public Stack1(T[] Items)
        {
            this.Items = Items;
            this.Capacity = Items.Length;
            Count = 0;
        }

        public Stack1(int Capacity)
        {
            if (Capacity <= 0)
            {
                Console.WriteLine("Capacity of Stack must be greater than 0");
            }
            this.Capacity = Capacity;
            this.Items = new T[Capacity];
            Count = 0;

        }

        public void Clear()
        {
            this.Items = new T[Capacity];
            Count = 0;
        }

        public bool Contains(T item) => Items.Contains(item);

        public T Pop()
        {
            if (Count == 0)
                throw new NullReferenceException("Stack is empty");
            T result = Items[Count - 1];
            if (Capacity == 1)
                Items = new T[Capacity];
            else
            {
                T[] temp = new T[Capacity];
                int count = 0;
                foreach (T item in Items)
                {
                    if (item != null)
                    {
                        if (!item.Equals(result))
                        {
                            temp[count] = item;
                            count++;
                        }
                    }
                }
                Items = temp;
            }
            Count -= 1;

            return result;
        }

        public void Push(T item)
        {
            if (Count == Capacity)
                throw new ArgumentOutOfRangeException("Stack is full");
            if (Count == 0)
                Items[0] = item;
            else if (Count != 0 && Count < Capacity)
            {
                Items[Count] = item;
            }
            if (Count != Capacity) Count += 1;
        }

        public T Peek()
        {
            if (Count == 0)
                throw new NullReferenceException("Stack is empty");
            return Items[Count];
        }

        public void Show()
        {
            if (Count > 0)
            {
                T[] temp = Items.Reverse().ToArray();
                foreach (var item in temp)
                {
                    if (item != null)
                        Console.Write($"{item} ");
                }
                Console.WriteLine();
            }
            Console.WriteLine($"Count: {Count}");
        }
    }
}
