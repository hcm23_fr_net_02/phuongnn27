﻿namespace ToDoExercise
{
    public class ToDo
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public bool completed { get; set; }

        public ToDo(int id, string first_name, string last_name, bool completed)
        {
            this.id = id;
            this.first_name = first_name;
            this.last_name = last_name;
            this.completed = completed;
        }
    }
}
