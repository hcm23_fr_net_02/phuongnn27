﻿using System.Text.Json;

namespace ToDoExercise
{
    public class ToDoList
    {
        public List<ToDo> List { get; set; }
        private const string path = "list-item.json";
        private const string path_copy = @"F:\FPT Software\Gitlab\SQL_ASSIGNMENT\CSharpBasic\Exercise2\ToDoExercise\list-item.json";

        public ToDoList() 
        { 
            List = new List<ToDo>();
            UploadFromJson();
        }

        public void Show()
        {
            foreach (var item in List)
                Console.WriteLine($"{item.id} - {item.first_name} - {item.last_name} - Completed: {item.completed}");
        }

        public void Add()
        {
            this.List.Add(FillToDoInfo());
            Console.WriteLine("Added ToDo successfully");
        }

        public void Update()
        {
            if (CheckList(true))
            {
                Console.WriteLine("\n------------------- Update a ToDo -------------------");
                int id = GetToDoById();

                ToDo todo = List.SingleOrDefault(t => t.id.Equals(id));
                if (todo != null)
                {
                    int index = List.IndexOf(todo);
                    if (!todo.completed) todo.completed = true;
                    List[index] = todo;
                    Console.WriteLine($"Update todo by id {id}");
                } else
                    Console.WriteLine($"Cannot find todo by id {id}");
            }
        }

        public void Remove()
        {
            if (CheckList(true))
            {
                Console.WriteLine("\n------------------- Remove a ToDo -------------------");
                int id = GetToDoById();

                ToDo todo = List.SingleOrDefault(t => t.id.Equals(id));
                if (todo != null)
                {
                    if (List.Remove(todo))
                        Console.WriteLine($"Removed todo by id {id}");
                }
                else
                    Console.WriteLine($"Cannot find todo by id {id}");
            }
        }

        private int GetToDoById ()
        {
            bool flag = true;
            int id = 0;

            while (flag)
            {
                try
                {
                    string temp = GetInputFromUser("Id: ").Trim();
                    if (!string.IsNullOrEmpty(temp))
                    {
                        id = Convert.ToInt32(temp);
                        flag = false;
                    }
                    else
                        Console.WriteLine("Please enter first name");
                }
                catch
                {
                    Console.WriteLine("Please enter number");
                }
            }
            return id;
        }

        private ToDo FillToDoInfo()
        {
            Console.WriteLine("\n------------------- Add a ToDo -------------------");
            bool flag = true;
            int id = List.Any() ? List.Last().id + 1 : 1;
            string first_name = "";
            string last_name = "";
            bool completed = false;

            while (flag)
            {
                first_name = GetInputFromUser("First name: ").Trim();
                if (!string.IsNullOrEmpty(first_name))
                    flag = false;
                else
                    Console.WriteLine("Please enter first name");
            }

            flag = true;
            while (flag)
            {
                last_name = GetInputFromUser("Last name: ").Trim();
                if (!string.IsNullOrEmpty(last_name))
                    flag = false;
                else
                    Console.WriteLine("Please enter last name");
            }

            flag = true;
            while (flag)
            {
                string temp = GetInputFromUser("Completed (Y/N): ").Trim().ToUpper();
                if (!string.IsNullOrEmpty(temp))
                {
                    if (temp.Equals("Y"))
                    {
                        completed = true;
                        flag = false;
                    } else if (temp.Equals("N"))
                    {
                        completed = false;
                        flag = false;
                    } else
                        Console.WriteLine("Please enter as format");
                }
                else
                    Console.WriteLine("Please enter last name");
            }
            return new ToDo(id, first_name, last_name, completed);
        }

        private string GetInputFromUser(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        private bool CheckList(bool ShowMessage = false)
        {
            if (ShowMessage && !List.Any())
                Console.WriteLine("list is empty");
            return List.Any();
        }

        private void UploadFromJson()
        {
            var content = File.ReadAllText(path);
            List = JsonSerializer.Deserialize<List<ToDo>>(content);
        }

        public void WriteIntoFile()
        {
            if (File.Exists(path_copy))
            {
                string result = JsonSerializer.Serialize(List);
                File.WriteAllText(path_copy, result);
            }
        }
    }
}
