﻿using System.Text.Json;

namespace ToDoExercise
{
    public class Program
    {
        static readonly string Info = $"---------------- ToDo Management----------------\n"
            + "1. Show ToDo\n"
            + "2. Add a ToDo\n"
            + "3. Remove a ToDo\n"
            + "4. Complete a ToDo\n"
            + "5. Quit\n" +
            "Your choice: ";
        static void Main()
        {
            string input = "";
            ToDoList toDoList = new ToDoList();
            do
            {
                try
                {
                    Console.Write(Info);
                    input = Console.ReadLine().Trim();
                    int choice = Convert.ToInt32(input);

                    switch (choice)
                    {
                        case 1:
                            toDoList.Show();
                            break;
                        case 2:
                            toDoList.Add();
                            break;
                        case 3:
                            toDoList.Remove();
                            break;
                        case 4:
                            toDoList.Update();
                            break;
                        case 5:
                            toDoList.WriteIntoFile();
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Please type number");
                }

            } while (!input.Equals("5"));
            Console.WriteLine("Good bye!");
            Console.ReadKey();
        }
    }
}
