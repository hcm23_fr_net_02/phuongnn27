﻿namespace DelegateEx1
{
    public class Program
    {
        delegate int MathOperation(int a, int b);

        static int sum(int a, int b) => a + b;
        static int subtract(int a, int b) => a - b;
        static int multiple(int a, int b) => a * b;
        static int division(int a, int b) => b > 0 ? a / b : 0;

        static void Main(string[] args)
        {
            MathOperation mathOperation1 = new MathOperation(sum);
            var result = mathOperation1(2, 3);
            Console.WriteLine($"Sum: {result}");
            MathOperation mathOperation2 = new MathOperation(subtract);
            result = mathOperation2(2, 3);
            Console.WriteLine($"Subtract: {result}");
            MathOperation mathOperation3 = new MathOperation(multiple);
            result = mathOperation3(2, 3);
            Console.WriteLine($"Multiple: {result}");
            MathOperation mathOperation4 = new MathOperation(division);
            result = mathOperation4(2, 3);
            Console.WriteLine($"Division: {result}");
        }
    }
}


