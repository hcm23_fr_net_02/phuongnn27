﻿namespace StudentManagement
{
    internal class Program
    {
        static readonly string Info = $"---------------- Student Management----------------\n"
            + "1. Add a student\n"
            + "2. Remove a student by name\n"
            + "3. Show students\n"
            + "4. Find students by name\n"
            + "5. Quit\n" +
            "Your choice: ";
        static void Main()
        {
            string input = "";
            StudentManager studentManager = new StudentManager();
            do
            {
                try 
                {
                    Console.Write(Info);
                    input = Console.ReadLine().Trim();
                    int choice = Convert.ToInt32(input);

                    switch(choice) 
                    {
                        case 1:
                            studentManager.AddStudent();
                            break;
                        case 2:
                            studentManager.RemoveStudent();
                            break;
                        case 3:
                            studentManager.DisplayStudents();
                            break;
                        case 4:
                            studentManager.FindStudentByName();
                            break;
                    }
                } catch
                {
                    Console.WriteLine("Please type number");
                }
                
            } while (!input.Equals("5"));
            Console.WriteLine("Good bye!");
            Console.ReadKey();
        }
    }
}
