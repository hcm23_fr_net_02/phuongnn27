﻿namespace StudentManagement
{
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double GPA { get; set; }

        public Student(string Name, int Age, double GPA) 
        {
            this.Name = Name;
            this.Age = Age;
            this. GPA = GPA;
        }

        public Student (string Name) => this.Name = Name;
    }
}
