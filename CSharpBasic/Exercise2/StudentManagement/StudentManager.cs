﻿namespace StudentManagement
{
    public class StudentManager
    {

        public List<Student> Students;
        public StudentManager()
        {
            Students = new List<Student>();
        }
        
        public StudentManager(List<Student> students)
        {
            Students = students;
        }

        private bool CheckList(bool ShowMessage = false)
        {
            if (ShowMessage && !Students.Any())
                Console.WriteLine("list is empty");
            return Students.Any();
        }

        public void AddStudent()
        {
            this.Students.Add(FillStudentInfo());
            Console.WriteLine("Added student with name successfully");
        }

        public void RemoveStudent()
        {
            string name = GetInputFromUser("Please enter name: ");
            if (CheckList(true))
            {
                name = name.Trim();
                var TempList = GetStudentsByName(name);
                if (TempList.Any())
                {
                    Students = Students.Except(TempList).ToList();
                    Console.WriteLine($"Removed student with name {name} successfully");
                }
                else 
                    Console.WriteLine($"There is no student with name {name}");

            }
        }

        public void DisplayStudents()
        {
            if (CheckList(true))
            {
                Console.WriteLine("\n------------------- Show Students -------------------");
                Students.ForEach(Student => 
                Console.WriteLine($"Name: {Student.Name}, Age: {Student.Age}, GPA: {Student.GPA}"));
                Console.WriteLine("-------------------------------------------------------------------");
            }
        }

        public void FindStudentByName()
        {
            string name = GetInputFromUser("Please enter name: ");
            if (CheckList(true))
            {
                name = name.Trim();
                var TempList = GetStudentsByName(name);
                if (TempList.Any())
                {
                    Console.WriteLine($"\n------------------- Find Students By Name {name} -------------------");
                    foreach (var Student in TempList)
                        Console.WriteLine($"Name: {Student.Name}, Age: {Student.Age}, GPA: {Student.GPA}");
                    Console.WriteLine("-------------------------------------------------------------------");
                }
                else
                    Console.WriteLine($"There is no student with name {name}");

            }
        }

        private IEnumerable<Student> GetStudentsByName(String name) => 
            Students.Where(s => s.Name.ToLower().Contains(name.ToLower()));

        private Student FillStudentInfo()
        {
            Console.WriteLine("\n------------------- Add a student -------------------");
            bool flag = true;
            string name = "";
            int age = 0;
            double gpa = 0;

            while (flag)
            {
                name = GetInputFromUser("Name: ");
                if (CheckValidName(name))
                    flag = false;
                else
                    Console.WriteLine("Please enter valid name");
            }

            flag = true;
            do
            {
                try
                {
                    age = Convert.ToInt32(GetInputFromUser("Age: "));
                    if (age <= 0)
                        throw new Exception();
                    else
                        flag = false;
                } catch
                {
                    Console.WriteLine("Please enter valid age");
                }
            } while (flag);
            
            flag = true;
            do
            {
                try
                {
                    gpa = Convert.ToDouble(GetInputFromUser("GPA: "));
                    if (gpa < 0)
                        throw new Exception();
                    else
                        flag = false;
                } catch
                {
                    Console.WriteLine("Please enter valid GPA");
                }
            } while (flag);
            return new Student(name, age, gpa);
        }

        private bool CheckValidName(string name)
        {
            var result = true;
            foreach(var c in name)
            {
                if (Char.IsDigit(c))
                    result = false;
            }
            return result;
        }


        private string GetInputFromUser(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

    }
}
