using StackAndQueue;

namespace StackAndQueueUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Queue_Test()
        {
            //Enqueue
            int expected = 2;

            Queue1<string> queue = new Queue1<string>(5);
            queue.Enqueue("hello");
            queue.Enqueue("world");

            int actual = queue.Count;

            Assert.AreEqual(expected, actual, $"The result is not matched: Expect - {expected} | Actual: {actual}");

            //Dequeue
            expected = 1;
            queue.Dequeue();
            actual = queue.Count;
            Assert.AreEqual(expected, actual, $"The result is not matched: Expect - {expected} | Actual: {actual}");

        }

        [TestMethod]
        public void Queue_Test_Exception()
        {
            //Enqueue

            Queue1<string> queue = new Queue1<string>(1);
            queue.Enqueue("hello");
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => queue.Enqueue("world"));

            //Dequeue
            queue.Dequeue();
            Assert.ThrowsException<NullReferenceException>(() => queue.Dequeue());
        }


        [TestMethod]
        public void Stack_Test()
        {
            //Pop
            int expected = 3;
            Stack1<string> stack = new Stack1<string>(5);
            stack.Push("hello");
            stack.Push("world");
            stack.Push("1");

            int actual = stack.Count;

            Assert.AreEqual(expected, actual, $"The result is not matched: Expect - {expected} | Actual: {actual}");

            //Push
            expected = 2;
            stack.Pop();
            actual = stack.Count;
            Assert.AreEqual(expected, actual, $"The result is not matched: Expect - {expected} | Actual: {actual}");
        }

        [TestMethod]
        public void Stack_Test_Exception()
        {
            //Push

            Stack1<string> stack = new Stack1<string>(1);
            stack.Push("hello");
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => stack.Push("world"));

            //Pop
            stack.Pop();
            Assert.ThrowsException<NullReferenceException>(() => stack.Pop());
        }
    }
}