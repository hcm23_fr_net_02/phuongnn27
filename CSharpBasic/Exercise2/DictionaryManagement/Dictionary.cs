﻿using System.Xml.Linq;

namespace DictionaryManagement
{
    public class Dictionary
    {
        Dictionary<string, string> dict;

        public Dictionary()
        {
            dict = new Dictionary<string, string>();
        }

        public Dictionary(Dictionary<string, string> dict)
        {
            this.dict = dict;
        }

        public void Add()
        {
            Console.WriteLine("\n------------------- Add a word -------------------");
            bool flag = true;
            string en = "";
            string vn = "";

            while (flag)
            {
                en = GetInputFromUser("English: ").Trim();
                if (CheckValidName(en))
                {
                    if (CheckExistedName(en))
                        Console.WriteLine("Duplicated key");
                    else
                        flag = false;
                }
                else
                    Console.WriteLine("Please enter valid name");
            }

            flag = true;
            while (flag)
            {
                vn = GetInputFromUser("Vietnamese: ").Trim();
                if (CheckValidName(vn))
                {
                    if (CheckExistedName(vn, false))
                        Console.WriteLine("Duplicated value");
                    else
                        flag = false;
                }
                else
                    Console.WriteLine("Please enter valid name");
            }

            dict.Add(en, vn);
            Console.WriteLine($"Add {en} - {vn} successfully");
        }

        public void FindWord()
        {
            if (CheckList())
            {
                Console.WriteLine("\n------------------- Find a word -------------------");
                KeyValuePair<string, string> existedName = GetKeyByName();
                if (existedName.Value != null)
                {
                    Console.WriteLine($"Key: {existedName.Key} - Value: {existedName.Value}");
                    UpdateWord(existedName);
                }
                else
                    Console.WriteLine("Cannot find word!");
            }
                

        }

        public void UpdateWord(KeyValuePair<string, string> existedName)
        {
            string en = "";
            string vn = "";
            bool flag = true;

            Console.WriteLine($"English: {existedName.Key}\nEnter without words if you want to keep it");
            while (flag)
            {
                en = GetInputFromUser("English: ").Trim();
                if (string.IsNullOrEmpty(en))
                {
                    en = existedName.Key;
                    flag = false;
                } else
                {
                    if (CheckValidName(en))
                    {
                        if (CheckExistedName(en) && 
                            !existedName.Key.Trim().ToLower().Equals(en.ToLower().Trim()))
                            Console.WriteLine("Duplicated key");
                        else
                            flag = false;
                    }
                    else
                        Console.WriteLine("Please enter valid name");
                }
            }

            flag = true;
            Console.WriteLine($"Vietnames: {existedName.Value}\nEnter without words if you want to keep it");
            while (flag)
            {
                vn = GetInputFromUser("Vietnamese: ").Trim();

                if (string.IsNullOrEmpty(vn))
                {
                    vn = existedName.Value;
                    flag = false;
                }
                else
                {
                    if (CheckValidName(vn))
                    {
                        if (CheckExistedName(vn) &&
                            !existedName.Key.Trim().ToLower().Equals(en.ToLower().Trim()))
                            Console.WriteLine("Duplicated key");
                        else
                            flag = false;
                    }
                    else
                        Console.WriteLine("Please enter valid name");
                }
            }

            dict.Remove(en);
            dict.Add(en, vn);

            Console.WriteLine("Update successfully!");
        }

        public void RemoveWord()
        {
            if (CheckList())
            {
                Console.WriteLine("\n------------------- Remove a word -------------------");
                KeyValuePair<string, string> existedName = GetKeyByName();
                if (existedName.Value != null)
                {
                    if (dict.Remove(existedName.Key))
                        Console.WriteLine($"Removed {existedName.Key} - {existedName.Value} successfully!");
                }
                else
                    Console.WriteLine("Cannot find word!");
            }
        }

        public void Show()
        {
            string word = "";
            bool flag = true;
            
            if (CheckList())
            {
                while (flag)
                {
                    try
                    {
                        word = GetInputFromUser("\n1. Show all" +
                                "\n2. English only" +
                                "\n3. Vietnamese only" +
                                "\nYour choice: ").Trim();
                        int choice = Convert.ToInt32(word);

                        if (choice > 0 && choice < 4)
                        {
                            PrintList(choice);
                            flag = false;
                        }
                        else
                            Console.WriteLine("Please enter from 1 to 3!");
                    }
                    catch
                    {
                        Console.WriteLine("Please enter number!");
                    }
                }
            }
        }

        private void PrintList(int choice)
        {
            Console.WriteLine("\n------------------- Show words -------------------");
            switch (choice)
            {
                case 1:
                    foreach (var item in dict)
                        Console.WriteLine($"{item.Key} - {item.Value}");
                    break;
                case 2:
                    foreach (var item in dict.Keys)
                        Console.WriteLine(item);
                    break;
                case 3:
                    foreach (var item in dict.Values)
                        Console.WriteLine(item);
                    break;
            }
        }

        private KeyValuePair<string, string> GetKeyByName()
        {
            string word = GetInputFromUser("Enter word: ").Trim();
            KeyValuePair<string, string> existedName = new KeyValuePair<string, string>();
            if (CheckExistedName(word))
                existedName = dict.Single(kvp => kvp.Key == word);
            else if (CheckExistedName(word, false))
                existedName = dict.Single(kvp => kvp.Value == word);
            return existedName;
        }

        /// <summary>
        /// Check existed name. If it exists, returns true. Otherwise, returns false
        /// </summary>
        /// <param name="name"></param>
        /// <param name="ByKey"></param>
        /// <returns></returns>
        private bool CheckExistedName(string name, bool ByKey = true)
        {
            bool result = true;
            if (ByKey)
                result = dict.Keys.Contains(name);
            else
                result = dict.Values.Contains(name);
            return result;
        }

        private KeyValuePair<string, string> GetStudentsByKeyName(String name) 
            => dict.SingleOrDefault(w => w.Key.ToLower().Equals(name.ToLower().Trim()));

        private bool CheckValidName(string name)
        {
            var result = true;
            foreach (var c in name)
            {
                if (Char.IsDigit(c))
                    result = false;
            }
            return result;
        }

        private string GetInputFromUser(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        private bool CheckList(bool ShowMessage = true)
        {
            if (ShowMessage && !dict.Any())
                Console.WriteLine("list is empty");
            return dict.Any();
        }
    }
}
