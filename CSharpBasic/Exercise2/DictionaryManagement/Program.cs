﻿namespace DictionaryManagement
{
    internal class Program
    {
        static readonly string Info = $"---------------- Dictionary Management----------------\n"
            + "1. Add a word\n"
            + "2. Find and update a word\n"
            + "3. Remove a word\n"
            + "4. Show words\n"
            + "5. Quit\n" +
            "Your choice: ";
        static void Main()
        {
            string input = "";
            Dictionary dictionary = new Dictionary();
            do
            {
                try
                {
                    Console.Write(Info);
                    input = Console.ReadLine().Trim();
                    int choice = Convert.ToInt32(input);

                    switch (choice)
                    {
                        case 1:
                            dictionary.Add();
                            break;
                        case 2:
                            dictionary.FindWord();
                            break;
                        case 3:
                            dictionary.RemoveWord();
                            break;
                        case 4:
                            dictionary.Show();
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Please type number");
                }

            } while (!input.Equals("5"));
            Console.WriteLine("Good bye!");
            Console.ReadKey();
        }
    }
}