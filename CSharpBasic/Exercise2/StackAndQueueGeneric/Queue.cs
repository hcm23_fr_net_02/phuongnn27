﻿namespace StackAndQueueGeneric
{
    public class Queue1<T>
    {
        private T[] Items;
        private int Capacity;
        public int Count;
        public T LastItemIndex;

        public Queue1()
        {
            this.Capacity = 10;
            Items = new T[Capacity];
            Count = 0;
        }

        public Queue1(T[] Items)
        {
            this.Items = Items;
            this.Capacity = Items.Length;
            Count = 0;
        }

        public Queue1(int Capacity)
        {
            if (Capacity <= 0)
            {
                Console.WriteLine("Capacity of Queue must be greater than 0");
            }
            this.Capacity = Capacity;
            this.Items = new T[Capacity];
            Count = 0;

        }

        public void Clear()
        {
            this.Items = new T[Capacity];
            Count = 0;
        }

        public bool Contains(T item) => Items.Contains(item);

        public bool IsEmpty() => Count == 0;

        public T Dequeue()
        {
            if (Count == 0)
                throw new NullReferenceException("Queue is empty");
            T result = Items[0];
            if (Capacity == 1)
                Items = new T[Capacity];
            else
            {
                T[] temp = new T[Capacity];
                int count = 0;
                foreach (T item in Items)
                {
                    if (item != null)
                    {
                        if (!item.Equals(result))
                        {
                            temp[count] = item;
                            count++;
                        }
                    }
                }
                Items = temp;
            }
            Count -= 1;

            return result;
        }

        public void Enqueue(T item)
        {
            if (Count == Capacity)
                throw new ArgumentOutOfRangeException("Queue is full");
            if (Count == 0)
                Items[0] = item;
            else
                Items[Count] = item;
            Count += 1;
        }

        public T Peek()
        {
            if (Count == 0)
                throw new NullReferenceException("Queue is empty");
            return Items[0];
        }

        public void Show()
        {
            if (Count > 0)
            {
                foreach (var item in Items)
                {
                    if (item != null)
                        Console.Write($"{item} ");
                }
                Console.WriteLine();
            }
            Console.WriteLine($"Count: {Count}");
        }
    }
}
