﻿namespace StackAndQueueGeneric
{
    public class Program
    {
        static readonly string Stack_Info = $"----------------Stack----------------\n"
            + "1. Initialize a stack\n"
            + "2. Push\n"
            + "3. Pop\n"
            + "4. Peek\n"
            + "5. Check empty stack\n"
            + "6. Clear\n"
            + "7. Count\n"
            + "8. Show stack's elements\n"
            + "9. Get last item\n"
            + "10. Change to Queue\n"
            + "11. Quit\n" +
            "Your choice: ";
        static readonly string Queue_Info = $"----------------Queue----------------\n"
            + "1. Initialize a queue\n"
            + "2. Push\n"
            + "3. Pop\n"
            + "4. Peek\n"
            + "5. Check empty queue\n"
            + "6. Clear\n"
            + "7. Count\n"
            + "8. Show queue's elements\n"
            + "9. Get last item\n"
            + "10. Change to Stack\n"
            + "11. Quit\n"
            + "Your choice: ";
        static Queue1<string> queue = null;
        static Stack1<string> stack = null;
        static void Main(string[] args)
        {
            int choice = GetFirstOption();
            do {
                switch (choice)
                {
                    case 1:
                        choice = Operation(Stack_Info);
                        break;
                    case 2:
                        choice = Operation(Queue_Info, 2);
                        break;
                }
            } 
            while (choice != 0);
            Console.WriteLine("Good bye!");
            Console.ReadKey();
        }

        static int GetFirstOption()
        {
            string input = "";
            int result = 0;
            do
            {
                Console.Write("Stack And Queue Generic\n" +
                "Do you want to use stack (1), queue (2) or quit (q)? ");
                input = Console.ReadLine().Trim().ToLower();
                if (input.Length == 1)
                {
                    if (Char.IsNumber(input[0]) && 
                        (input.Equals("1") || input.Equals("2")))
                    {
                        result = Convert.ToInt32(input);
                        break;
                    }
                }
            } while (!input.Equals("q") && result == 0);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="IsStack">if stack, then 1. Otherwise, it is 2 for queue</param>
        /// <returns></returns>
        static int Operation(string info, int IsStack = 1)
        {
            Queue1<string> queue = null;
            string input = "";
            int result = 0;
            do
            {
                Console.Write(info);
                try
                {
                    input = Console.ReadLine().Trim().ToLower();
                    result = Convert.ToInt32(input);
                    if (result == 11)
                        result = 0;
                    else if (result == 10)
                    {
                        if (IsStack == 1)
                        {
                            if (stack != null) stack = null;
                            result = Operation(Queue_Info, 2);
                        } else
                        {
                            if (queue != null) queue = null;
                            result = Operation(Stack_Info, 1);
                        }
                    } else
                    {
                        if (IsStack == 1)
                        {
                            StackOperation(result);
                        } else
                        {
                            QueueOperation(result);
                        }
                        result = 0;
                    }

                } catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            } while (!input.Equals("11") && result == 0);
            return result;
        }

        static void StackOperation(int choice)
        {
            if (choice == 1)
            {
                bool flag = true;
                do
                {
                    try
                    {
                        Console.Write("How many elements: ");
                        int capacity = Convert.ToInt32(Console.ReadLine().Trim());
                        stack = new Stack1<string>(capacity);
                        flag = false;
                    } catch
                    {
                        Console.WriteLine("Please type number");
                    }
                } while (flag);
            } else
            {
                string message = "";
                if (!CheckNull(1))
                {
                    switch (choice)
                    {
                        //Push
                        case 2:
                            string element = GetElementForStackOrQueue();
                            stack.Push(element);
                            message = "Push element successfully";
                            break;
                        //Pop
                        case 3:
                            message = $"Pop element: {stack.Pop()}";
                            break;
                        //Peek
                        case 4:
                            message = $"Peek element: {stack.Peek()}";
                            break;
                        //Check empty
                        case 5:
                            message = stack.IsEmpty() ? "Stack is empty" : "Stack is not empty";
                            break;
                        //Clear
                        case 6:
                            stack.Clear();
                            message = "Stack is cleared!";
                            break;
                        //Count
                        case 7:
                            message = $"Count: {stack.Count}";
                            break;
                        //Show
                        case 8:
                            stack.Show();
                            break;
                        //Last
                        case 9:
                            message = $"Last item in stack: {stack.LastItemIndex}";
                            break;
                    }
                    if (!String.IsNullOrEmpty(message))
                        Console.WriteLine(message);
                }
            }
        }
        static void QueueOperation(int choice)
        {
            if (choice == 1)
            {
                bool flag = true;
                do
                {
                    try
                    {
                        Console.Write("How many elements: ");
                        int capacity = Convert.ToInt32(Console.ReadLine().Trim());
                        queue = new Queue1<string>(capacity);
                        flag = false;
                    } catch
                    {
                        Console.WriteLine("Please type number");
                    }
                } while (flag);
            } else
            {
                string message = "";
                if (!CheckNull(2))
                {
                    switch (choice)
                    {
                        //Enqueue
                        case 2:
                            string element = GetElementForStackOrQueue();
                            queue.Enqueue(element);
                            message = "Enqueue element successfully";
                            break;
                        //Dequeue
                        case 3:
                            message = $"Dequeue element: {queue.Dequeue()}";
                            break;
                        //Peek
                        case 4:
                            message = $"Peek element: {queue.Peek()}";
                            break;
                        //Check empty
                        case 5:
                            message = queue.IsEmpty() ? "Queue is empty" : "Queue is not empty";
                            break;
                        //Clear
                        case 6:
                            queue.Clear();
                            message = "Queue is cleared!";
                            break;
                        //Count
                        case 7:
                            message = $"Count: {queue.Count}";
                            break;
                        //Show
                        case 8:
                            queue.Show();
                            break;
                        //Last
                        case 9:
                            message = $"Last item in queue: {queue.LastItemIndex}";
                            break;
                    }
                    if (!String.IsNullOrEmpty(message))
                        Console.WriteLine(message);
                }
                
            }
        }

        static string GetElementForStackOrQueue()
        {
            Console.Write("Enter element: ");
            return Console.ReadLine();
        }

        /// <summary>
        /// Check null of stack or queue
        /// </summary>
        /// <param name="IsStack">if stack, then 1. Otherwise, it is 2 for queue</param>
        static bool CheckNull(int IsStack = 1)
        {
            bool result = false;
            if (IsStack == 1 && stack == null)
            {
                Console.WriteLine("Stack is not intialized yet!");
                result = true;
            }
            if (IsStack == 2 && queue == null)
            {
                Console.WriteLine("Queue is not intialized yet!");
                result = true;
            }
            return result; 
        }
    }
}
