﻿namespace Exercise1
{
    internal class DSAQueue
    {
        static void Main(string[] args)
        {
            int T = 0;
            Stack<int> stack = new Stack<int>();
            string input = "";
            try
            {
                do
                {
                    Console.WriteLine("Do you want to push (1), pop (2) or quit (q)?");
                    input = Console.ReadLine().Trim();


                    if (!string.IsNullOrEmpty(input) || input.Equals("1") || input.Equals("2"))
                    {
                        int option = int.Parse(input);
                        switch (option)
                        {
                            case 1:
                                Console.Write("Please enter number: ");
                                int number = Int32.Parse(Console.ReadLine().Trim());
                                stack.Push(number);
                                T = stack.Count > 1 ? T + 1 : 0;
                                break;
                            case 2:
                                if (stack.Count > 0)
                                {
                                    stack.Pop();
                                    T -= 1;
                                }
                                break;
                        }

                        if (stack.Count > 0)
                        {
                            foreach (int item in stack)
                                Console.Write($"{item} ");
                            Console.WriteLine($"\nT:{T}");
                        }
                    }
                }
                while (!input.Equals("q"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Thank you!");
            Console.ReadKey();
        }
    }
}
