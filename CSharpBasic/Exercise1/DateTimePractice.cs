﻿namespace Exercise1
{
    internal class DateTimePractice
    {
        static void Main(string[] args)
        {
            var today = DateTime.Today;
            Console.WriteLine($"Today: {today}");
            var DateTimeFormat = new DateTime(2023, 08, 31, 11, 59, 24, DateTimeKind.Utc);
            Console.WriteLine($"Date time format (UTC): {DateTimeFormat}");
            var TwoDaysAfter = today.AddDays(2);
            Console.WriteLine($"2 days after: {TwoDaysAfter}");
        }
    }
}
