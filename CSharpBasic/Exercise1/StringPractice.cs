﻿using System;

namespace Exercise1
{
    public class StringPractice
    {
        static void Main(string[] args)
        {
            var list = new List<Employee>()
            {
                new Employee(1, "Amy", "Han", CompanyRole.Manager),
                new Employee(2, "John", "Nguyen", CompanyRole.Employee),
                new Employee(3, "William", "Smith", CompanyRole.Employee),
            };

            list.ForEach(e => Console.WriteLine($"ID: {e.ID}\n" +
                $"Fullname: {string.Join(' ', new string[] { e.FirstName, e.LastName })}\n" +
                $"Role: {e.Role}"));
        }

        internal enum CompanyRole
        {
            Employee,
            Manager
        }

        internal class Employee
        {
            public int ID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public CompanyRole Role { get; set; }

            public Employee(int id, string firstName, string lastName, CompanyRole role)
            {
                ID = id;
                FirstName = firstName;
                LastName = lastName;
                Role = role;
            }
        }
    }
}