﻿namespace Exercise1
{
    internal class DSAStack
    {
        static void Main(string[] args)
        {
            int T = 0;
            Queue<int> queue = new Queue<int>();
            string input = "";
            try
            {
                do
                {
                    Console.WriteLine("Do you want to enqueue (1), dequeue (2) or quit (q)?");
                    input = Console.ReadLine().Trim();


                    if (!string.IsNullOrEmpty(input) || input.Equals("1") || input.Equals("2"))
                    {
                        int option = int.Parse(input);
                        switch (option)
                        {
                            case 1:
                                Console.Write("Please enter number: ");
                                int number = Int32.Parse(Console.ReadLine().Trim());
                                queue.Enqueue(number);
                                T = queue.Count > 1 ? T + 1 : 0;
                                break;
                            case 2:
                                if (queue.Count > 0)
                                {
                                    queue.Dequeue();
                                    T -= 1;
                                }
                                break;
                        }

                        if (queue.Count > 0)
                        {
                            foreach (int item in queue)
                                Console.Write($"{item} ");
                            Console.WriteLine($"\nT:{T}");
                        }
                    }
                }
                while (!input.Equals("q"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Thank you!");
            Console.ReadKey();
        }
    }
}
