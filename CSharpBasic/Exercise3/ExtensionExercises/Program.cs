﻿namespace ExtensionExercises
{
    internal class Program
    {
        static readonly string Info = $"---------------- Extension Exercise ----------------\n"
                + "1. Reverse words\n"
                + "2. Average of list\n"
                + "3. Remove duplicated words\n"
                + "4. Quit\n" +
                "Your choice: ";
        static void Main(string[] args)
        {
            int choice = 0;
            do
            {
                try
                {
                    Console.Write(Info);
                    choice = Convert.ToInt32(Console.ReadLine().Trim());
                    string input = "";
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Result: " +
                                $"{input.GetInputFromUser("Enter word(s) to reverse: ").DaoChuoi()}");
                            break;
                        case 2:
                            List<int> numbers = new List<int> { 1, 2, 3, 4, 5 };
                            Console.Write("List of numbers: ");
                            numbers.ForEach(x => Console.Write(x + " "));
                            Console.WriteLine($"\nResult: {numbers.GetAverageFromList()}");
                            break;
                        case 3:
                            input = "Hello word";
                            Console.WriteLine($"Word to remove duplicates: {input}");
                            Console.WriteLine($"Result: {input.RemoveDuplicates()}");
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Please type number");
                }

            } while (!choice.Equals(4));
            Console.WriteLine("Good bye!");
            Console.ReadKey();
        }
    }

    public static class ExtensionMethod
    {
        public static string DaoChuoi(this string name)
        {
            string result = null;
            if (!string.IsNullOrEmpty(name))
            {
                foreach (var item in name.Trim().Reverse())
                    result += item;
            }
            return result;
        }

        public static string RemoveDuplicates(this string name)
        {
            string result = null;
            if (!string.IsNullOrEmpty(name))
            {
                var list = new List<char>();
                foreach (var ch in name.Trim())
                {
                    if (!list.Contains(ch) || Char.IsWhiteSpace(ch))
                        list.Add(ch);
                }

                list.ForEach(ch => result += ch);
            }
            return result;
        }

        public static double GetAverageFromList(this List<int> list)
        {
            double result = 0;
            if (list.Any())
                result = list.Sum() / list.Count;
            return result;
        }

        public static string GetInputFromUser(this string name, string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }
    }

}