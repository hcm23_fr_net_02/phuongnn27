﻿var root_path = Environment.CurrentDirectory.Split(@"bin\Debug\net6.0")[0] + @"Images\";
if (Directory.Exists(root_path))
{
    var list = new Dictionary<string, string>()
    {
        {"https://t4.ftcdn.net/jpg/00/97/58/97/360_F_97589769_t45CqXyzjz0KXwoBZT9PRaWGHRk5hQqQ.jpg",
        root_path + "1.jpg"},
        {"https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg",
        root_path + "2.jpg"},
        {"https://thumbs.dreamstime.com/b/beautiful-happy-reddish-havanese-puppy-dog-sitting-frontal-looking-camera-isolated-white-background-46868560.jpg",
        root_path + "3.jpg"},
        {"https://cdn.pixabay.com/photo/2014/11/30/14/11/cat-551554_640.jpg",
        root_path + "4.jpg"},
        {"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUfeh2OibC1NImBHcsXkbzJ7VQQXcLeDIvq9eRJh4DzpnG6zFpc7PXmprHyvukuBgmTsM&usqp=CAU",
        root_path + "5.jpg"}
    };
    foreach (var item in list)
    {
        if (File.Exists(item.Value))
            File.Delete(item.Value);
        await item.Key.GetImage(item.Value);
    }
}

public static class ImageExtension
{
    public static async Task GetImage(this string url, string path)
    {
        using (HttpClient httpClient = new HttpClient())
        {
            try
            {
                HttpResponseMessage response = await httpClient.GetAsync(url);



                if (response.IsSuccessStatusCode)
                {
                    byte[] imageBytes = await response.Content.ReadAsByteArrayAsync();



                    // Save the image to a local file
                    System.IO.File.WriteAllBytes(path, imageBytes);



                    Console.WriteLine($"Image downloaded and saved");
                }
                else
                {
                    Console.WriteLine($"Failed to download the image. Status code: {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }
    }
}