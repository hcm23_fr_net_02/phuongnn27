﻿using ToDoHttpClientPractice;

string Info = $"---------------- ToDo Management----------------\n"
            + "1. Show ToDo\n"
            + "2. Add a ToDo\n"
            + "3. Remove a ToDo\n"
            + "4. Complete a ToDo\n"
            + "5. Increase priority\n"
            + "6. Quit\n" +
            "Your choice: ";
ToDoList toDoList = new ToDoList();

var imported = await toDoList.ImportFromUrl();
if (imported)
{
    await RunProgram(toDoList);
}

async Task RunProgram(ToDoList toDoList)
{
    string input = "";
    do
    {
        try
        {
            Console.Write(Info);
            input = Console.ReadLine().Trim();
            int choice = Convert.ToInt32(input);

            switch (choice)
            {
                case 1:
                    toDoList.Show();
                    break;
                case 2:
                    await toDoList.Add();
                    break;
                case 3:
                    await toDoList.Remove();
                    break;
                case 4:
                    await toDoList.UpdateCompletedStatus();
                    break;
                case 5:
                    await toDoList.UpdatePriority();
                    break;
            }
        }
        catch
        {
            Console.WriteLine("Please type number");
        }

    } while (!input.Equals("6"));
    Console.WriteLine("Good bye!");
    Console.ReadKey();
}

