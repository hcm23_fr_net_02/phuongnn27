﻿namespace ToDoHttpClientPractice
{
    public class ToDo
    {
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        public string name { get; set; }
        public string avatar { get; set; }
        public bool completed { get; set; }
        public int priority { get; set; }

        public ToDo(int id, DateTime createdAt, string name, string avatar, bool completed, int priority)
        {
            this.id = id;
            this.createdAt = createdAt;
            this.name = name;
            this.avatar = avatar;
            this.completed = completed;
            this.priority = priority;
        }
    }
}
