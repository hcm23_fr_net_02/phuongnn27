﻿using Newtonsoft.Json;
using System.Text;

namespace ToDoHttpClientPractice
{
    public class ToDoList
    {
        public List<ToDo> List { get; set; }
        private const string url = "https://651e627744a3a8aa47683cbe.mockapi.io/phuong521/api/v1/todo";

        public ToDoList()
        {
            List = new List<ToDo>();
        }

        public async void Show()
        {
            string word = "";
            bool flag = true;

            if (CheckList())
            {
                while (flag)
                {
                    try
                    {
                        word = GetInputFromUser("\n1. Show info based on created date" +
                                "\n2. Show info based on priority" +
                                "\nYour choice: ").Trim();
                        int choice = Convert.ToInt32(word);

                        if (choice > 0 && choice < 3)
                        {
                            PrintList(choice);
                            flag = false;
                        }
                        else
                            Console.WriteLine("Please enter from 1 to 3!");
                    }
                    catch
                    {
                        Console.WriteLine("Please enter number!");
                    }
                }
            }
        }

        public async Task Add()
        {
            var newToDo = FillToDoInfo();
            var result = await PostToUrl(newToDo);
            if (result)
            {
                this.List.Add(newToDo);
                Console.WriteLine("Added ToDo successfully");
            } else
                Console.WriteLine("Added ToDo unsuccessfully");
        }

        public async Task Remove()
        {
            if (CheckList(true))
            {
                Console.WriteLine("\n------------------- Remove a ToDo -------------------");
                int id = GetToDoById();

                ToDo todo = List.SingleOrDefault(t => t.id.Equals(id));
                if (todo != null)
                {
                    var result = await RemoveToDoToUrl(id);
                    if (result)
                    {
                        if (List.Remove(todo))
                            Console.WriteLine($"Removed todo by id {id}");
                    } else
                        Console.WriteLine($"Cannot remove todo by id {id}");
                }
                else
                    Console.WriteLine($"Cannot find todo by id {id}");
            }
        }

        public async Task UpdateCompletedStatus()
        {
            if (CheckList(true))
            {
                Console.WriteLine("\n------------------- Update a ToDo Completed Status -------------------");
                int id = GetToDoById();

                ToDo todo = List.SingleOrDefault(t => t.id.Equals(id));
                if (todo != null)
                {
                    int index = List.IndexOf(todo);
                    if (!todo.completed) todo.completed = true;
                    var result = await UpdateToDoToUrl(todo);
                    if (result)
                    {
                        List[index] = todo;
                        Console.WriteLine($"Update todo's completed status by id {id}");
                    }
                    else
                        Console.WriteLine($"Cannot update todo's completed status by id {id}");
                }
                else
                    Console.WriteLine($"Cannot find todo by id {id}");
            }
        }
        
        public async Task UpdatePriority()
        {
            if (CheckList(true))
            {
                Console.WriteLine("\n------------------- Update a ToDo Priority -------------------");
                int id = GetToDoById();

                ToDo todo = List.SingleOrDefault(t => t.id.Equals(id));
                if (todo != null)
                {
                    int index = List.IndexOf(todo);
                    todo.priority += 1;
                    var result = await UpdateToDoToUrl(todo);
                    if (result)
                    {
                        List[index] = todo;
                        Console.WriteLine($"Update todo's priority by id {id}");
                    }
                    else
                        Console.WriteLine($"Cannot update todo's priority by id {id}");
                }
                else
                    Console.WriteLine($"Cannot find todo by id {id}");
            }
        }

        private int GetToDoById()
        {
            bool flag = true;
            int id = 0;

            while (flag)
            {
                try
                {
                    string temp = GetInputFromUser("Id: ").Trim();
                    if (!string.IsNullOrEmpty(temp))
                    {
                        id = Convert.ToInt32(temp);
                        flag = false;
                    }
                    else
                        Console.WriteLine("Please enter first name");
                }
                catch
                {
                    Console.WriteLine("Please enter number");
                }
            }
            return id;
        }

        private ToDo FillToDoInfo()
        {
            Console.WriteLine("\n------------------- Add a ToDo -------------------");
            bool flag = true;
            int id = List.Any() ? List.Last().id + 1 : 1;
            string name = "";
            string avatar = "";
            int priority = 0;
            bool completed = false;

            //name
            while (flag)
            {
                name = GetInputFromUser("Name: ").Trim();
                if (!string.IsNullOrEmpty(name))
                    flag = false;
                else
                    Console.WriteLine("Please enter name");
            }

            flag = true;
            //avatar
            while (flag)
            {
                avatar = GetInputFromUser("Avatar: ").Trim();
                if (!string.IsNullOrEmpty(avatar))
                    flag = false;
                else
                    Console.WriteLine("Please enter avatar");
            }

            flag = true;
            //priority
            while (flag)
            {
                try
                {
                    priority = Convert.ToInt32(GetInputFromUser("Priority: "));
                    if (priority <= 0)
                        throw new Exception();
                    else
                        flag = false;
                }
                catch
                {
                    Console.WriteLine("Please enter valid priority");
                }
            }

            flag = true;
            while (flag)
            {
                string temp = GetInputFromUser("Completed (Y/N): ").Trim().ToUpper();
                if (!string.IsNullOrEmpty(temp))
                {
                    if (temp.Equals("Y"))
                    {
                        completed = true;
                        flag = false;
                    }
                    else if (temp.Equals("N"))
                    {
                        completed = false;
                        flag = false;
                    }
                    else
                        Console.WriteLine("Please enter as format");
                }
                else
                    Console.WriteLine("Please enter last name");
            }
            return new ToDo(id, DateTime.Now, name, avatar, completed, priority);
        }

        private void PrintList(int choice)
        {
            Console.WriteLine("\n------------------- Show words -------------------");
            switch (choice)
            {
                case 1:
                    foreach (var item in List.OrderByDescending(l => l.createdAt))
                        Console.WriteLine($"{item.id} - {item.name} - {item.createdAt} - Completed: {item.completed} - priority: {item.priority}");
                    break;
                case 2:
                    foreach (var item in List.OrderByDescending(l => l.priority))
                        Console.WriteLine($"{item.id} - {item.name} - {item.createdAt} - Completed: {item.completed} - priority: {item.priority}");
                    break;
            }
        }

        private bool CheckList(bool ShowMessage = true)
        {
            if (ShowMessage && !List.Any())
                Console.WriteLine("list is empty");
            return List.Any();
        }

        private string GetInputFromUser(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        public async Task<bool> ImportFromUrl()
        {
            var result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        List = JsonConvert.DeserializeObject<List<ToDo>>(content);
                        Console.WriteLine("Import successfully");
                        result = true;
                    }
                    else
                        Console.WriteLine("Import unsuccessfully");
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        private async Task<bool> PostToUrl(ToDo toDo)
        {
            var result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    var toDoJson = JsonConvert.SerializeObject(toDo);
                    var content = new StringContent(toDoJson, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(url, content);
                    if (response.IsSuccessStatusCode)
                        result = true;
                    else
                        Console.WriteLine("Cannot post todo by url");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        private async Task<bool> RemoveToDoToUrl(int id)
        {
            var result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.DeleteAsync($"{url}/{id}");
                    if (response.IsSuccessStatusCode)
                        result = true;
                    else
                        Console.WriteLine("Cannot delete todo by url");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        private async Task<bool> UpdateToDoToUrl(ToDo toDo)
        {
            var result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    var toDoJson = JsonConvert.SerializeObject(toDo);
                    var content = new StringContent(toDoJson, Encoding.UTF8, "application/json");
                    var response = await client.PutAsync($"{url}/{toDo.id}", content);
                    if (response.IsSuccessStatusCode)
                        result = true;
                    else
                        Console.WriteLine("Cannot update todo by url");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
    }
}
