namespace UnitTestPractice
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Test sum of 2 integer numbers
        /// </summary>
        [TestMethod]
        public void Test_SumOf2Numbers()
        {
            //Expected result
            int Expected = 3;

            //Actual result
            int Actual = 1 + 2;

            Assert.AreEqual(Expected, Actual, $"The sum is not correct Expected: {Expected} - Actual: {Actual}");
        }
        
        /// <summary>
        /// Test factorial
        /// </summary>
        [TestMethod]
        public void Test_Factorial()
        {
            //Expected result
            int Expected = 120;

            Factorial factorial = new Factorial();

            //Actual result
            int Actual = factorial.CalculateFactorial(5);

            Assert.AreEqual(Expected, Actual, $"The factorial is not correct Expected: {Expected} - Actual: {Actual}");
        }
    }

    public class Factorial
    {
        public int CalculateFactorial(int number)
        {
            if (number == 0) return 1;
            return number * CalculateFactorial(number - 1);
        }
    }
}